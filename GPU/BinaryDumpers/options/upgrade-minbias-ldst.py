from Configurables import LHCbApp
from PRConfig import TestFileDB
from Configurables import RecombineRawEvent

sample = TestFileDB.test_file_db['upgrade-magdown-sim09c-up02-reco-up01-minbias-ldst']
sample.run(configurable=LHCbApp(), withDB=True)

RecombineRawEvent().Version = 4.1
