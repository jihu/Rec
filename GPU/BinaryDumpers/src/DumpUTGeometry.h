#ifndef DUMPUTGEOMETRY_H
#define DUMPUTGEOMETRY_H 1

// Include files
#include <STDet/DeUTDetector.h>
#include "DumpGeometry.h"

/** @class DumpUTGeometry
 *  Class that dumps the UT geometry
 *
 *  @author Roel Aaij
 *  @date   2018-08-27
 */
class DumpUTGeometry final : public DumpGeometry<DeUTDetector> {
 public:
  DumpUTGeometry(std::string name, ISvcLocator* loc)
      : DumpGeometry<DeUTDetector>{std::move(name), loc, DeSTDetLocation::UT} {}

 protected:
  StatusCode dumpGeometry() const override;

 private:
  StatusCode dumpGeom() const;
  StatusCode dumpBoards() const;
};

#endif  // DUMPUTGEOMETRY_H
