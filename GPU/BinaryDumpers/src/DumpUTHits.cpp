#include <vector>

#include <boost/filesystem.hpp>

#include "DumpUTHits.h"
#include "Utils.h"

namespace {
using std::make_unique;
using std::vector;
using std::fstream;
using std::ofstream;
using std::ios;
using std::string;
using std::to_string;

namespace fs = boost::filesystem;
}

// Declaration of the Algorithm Factory
DECLARE_COMPONENT(DumpUTHits)

DumpUTHits::DumpUTHits(const string& name, ISvcLocator* pSvcLocator)
    : Consumer(name, pSvcLocator,
               {KeyValue{"ODINLocation", LHCb::ODINLocation::Default},
                KeyValue{"UTHitsLocation", UT::Info::HitLocation}}) {}

StatusCode DumpUTHits::initialize() {
  if (!DumpUtils::createDirectory(m_outputDirectory.value())) {
    error() << "Failed to create directory " << m_outputDirectory.value()
            << endmsg;
    return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}

void DumpUTHits::operator()(const LHCb::ODIN& odin,
                            const UT::HitHandler& hitHandler) const {
  string filename =
      (m_outputDirectory.value() + "/" + to_string(odin.runNumber()) + "_" +
       to_string(odin.eventNumber()) + ".bin");

  /*Write UT variables for GPU to binary file */
  ofstream outfile(filename, ios::out | ios::binary);

  // UT
  // Variables written to binary file as GPU input for VeloUT tracking

  const int n_layers_ut = 4;
  auto ut_cos = make_unique<vector<float>[]>(n_layers_ut);
  auto ut_yBegin = make_unique<vector<float>[]>(n_layers_ut);
  auto ut_yEnd = make_unique<vector<float>[]>(n_layers_ut);
  auto ut_dxDy = make_unique<vector<float>[]>(n_layers_ut);
  auto ut_zAtYEq0 = make_unique<vector<float>[]>(n_layers_ut);
  auto ut_xAtYEq0 = make_unique<vector<float>[]>(n_layers_ut);
  auto ut_weight = make_unique<vector<float>[]>(n_layers_ut);
  auto ut_highThreshold = make_unique<vector<int>[]>(n_layers_ut);
  auto ut_LHCbID = make_unique<vector<unsigned int>[]>(n_layers_ut);

  for (int iStation = 1; iStation < 3; ++iStation) {
    for (int iLayer = 1; iLayer < 3; ++iLayer) {
      for (int iRegion = 1; iRegion < 4; ++iRegion) {
        for (int iSector = 1; iSector < 99; ++iSector) {
          for (auto& hit :
               hitHandler.hits(iStation, iLayer, iRegion, iSector)) {
            /* For GPU input */
            ut_cos[hit.planeCode()].push_back(hit.cos());
            ut_yBegin[hit.planeCode()].push_back(hit.yBegin());
            ut_yEnd[hit.planeCode()].push_back(hit.yEnd());
            ut_dxDy[hit.planeCode()].push_back(hit.dxDy());
            ut_zAtYEq0[hit.planeCode()].push_back(hit.zAtYEq0());
            ut_xAtYEq0[hit.planeCode()].push_back(hit.xAtYEq0());
            ut_weight[hit.planeCode()].push_back(hit.weight());
            ut_highThreshold[hit.planeCode()].push_back(hit.highThreshold());
            ut_LHCbID[hit.planeCode()].push_back(hit.lhcbID().lhcbID());
          }
        }
      }
    }
  }

  // first the number of hits per layer in each half as header
  for (int iStation = 0; iStation < 2; ++iStation) {
    for (int iLayer = 0; iLayer < 2; ++iLayer) {
      int index = 2 * iStation + iLayer;
      uint32_t n_hits = (int)(ut_cos[index].size());
      outfile.write((char*)&n_hits, sizeof(uint32_t));
    }
  }

  // then the vectors containing the variables
  for (int i_layer = 0; i_layer < n_layers_ut; ++i_layer) {
    outfile.write((char*)ut_cos[i_layer].data(),
                  ut_cos[i_layer].size() * sizeof(float));
    outfile.write((char*)ut_yBegin[i_layer].data(),
                  ut_yBegin[i_layer].size() * sizeof(float));
    outfile.write((char*)ut_yEnd[i_layer].data(),
                  ut_yEnd[i_layer].size() * sizeof(float));
    outfile.write((char*)ut_dxDy[i_layer].data(),
                  ut_dxDy[i_layer].size() * sizeof(float));
    outfile.write((char*)ut_zAtYEq0[i_layer].data(),
                  ut_zAtYEq0[i_layer].size() * sizeof(float));
    outfile.write((char*)ut_xAtYEq0[i_layer].data(),
                  ut_xAtYEq0[i_layer].size() * sizeof(float));
    outfile.write((char*)ut_weight[i_layer].data(),
                  ut_weight[i_layer].size() * sizeof(float));
    outfile.write((char*)ut_highThreshold[i_layer].data(),
                  ut_highThreshold[i_layer].size() * sizeof(int));
    outfile.write((char*)ut_LHCbID[i_layer].data(),
                  ut_LHCbID[i_layer].size() * sizeof(unsigned int));
  }
  outfile.close();
}
