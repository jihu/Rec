#include <vector>

#include <boost/filesystem.hpp>

#include "DumpMuonCommonHits.h"
#include "Utils.h"

namespace {
using std::make_unique;
using std::vector;
using std::fstream;
using std::ofstream;
using std::ios;
using std::string;
using std::to_string;

namespace fs = boost::filesystem;
}

// Declaration of the Algorithm Factory
DECLARE_COMPONENT(DumpMuonCommonHits)

DumpMuonCommonHits::DumpMuonCommonHits(const string& name, ISvcLocator* pSvcLocator)
    : Consumer(name, pSvcLocator,
               {KeyValue{"ODINLocation", LHCb::ODINLocation::Default},
		   KeyValue{"MuonHitHandler", MuonHitHandlerLocation::Default}}) {}

StatusCode DumpMuonCommonHits::initialize() {
  if (!DumpUtils::createDirectory(m_outputDirectory.value())) {
    error() << "Failed to create directory " << m_outputDirectory.value()
            << endmsg;
    return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}

void DumpMuonCommonHits::operator()(const LHCb::ODIN& odin,
			      const MuonHitHandler& hitHandler) const {
  string filename =
      (m_outputDirectory.value() + "/" + to_string(odin.runNumber()) + "_" +
       to_string(odin.eventNumber()) + ".bin");

  /*Write muon common hit variables for GPU to binary file */
  ofstream outfile(filename, ios::out | ios::binary);

  const int n_stations = 4;
  
  auto tileID = make_unique<vector<int>[]>(n_stations);
  auto x = make_unique<vector<float>[]>(n_stations);
  auto dx = make_unique<vector<float>[]>(n_stations);
  auto y = make_unique<vector<float>[]>(n_stations);
  auto dy = make_unique<vector<float>[]>(n_stations);
  auto z = make_unique<vector<float>[]>(n_stations);
  auto dz = make_unique<vector<float>[]>(n_stations);
  auto uncrossed = make_unique<vector<int>[]>(n_stations);
  auto time = make_unique<vector<unsigned int>[]>(n_stations);
  auto deltaTime = make_unique<vector<int>[]>(n_stations);
  auto clusterSize = make_unique<vector<int>[]>(n_stations);

  // loop over the hits in every station
  for ( int i_station = 0; i_station < n_stations; ++i_station ) {
    const CommonMuonStation& station = hitHandler.station(i_station);
    for ( uint i_region = 0; i_region < station.nRegions(); ++i_region) {
      CommonMuonHitRange hitRange = station.hits(i_region);
      for ( const auto& hit : hitRange ) {
	tileID[i_station].push_back( int( hit.tile() ) );
	x[i_station].push_back( hit.x() );
	dx[i_station].push_back( hit.dx() );
	y[i_station].push_back( hit.y() );
	dy[i_station].push_back( hit.dy() );
	z[i_station].push_back( hit.z() );
	dz[i_station].push_back( hit.dz() );
	uncrossed[i_station].push_back( (int)(hit.uncrossed()) );
	time[i_station].push_back( hit.time() );
	deltaTime[i_station].push_back( hit.deltaTime() );
	clusterSize[i_station].push_back( hit.clusterSize() );
      }
    }
  }

  // first the number of hits in every station
  for ( int i_station = 0; i_station < n_stations; ++i_station ) {
    uint32_t n_hits = (int)(tileID[i_station].size());
    outfile.write((char*)&n_hits, sizeof(uint32_t));
  }
 
  // then the vectors containing the variables
  for ( int i_station = 0; i_station < n_stations; ++i_station ) {
    outfile.write((char*)tileID[i_station].data(),
		  tileID[i_station].size() * sizeof(int));
    outfile.write((char*)x[i_station].data(),
                  x[i_station].size() * sizeof(float));
    outfile.write((char*)dx[i_station].data(),
                  dx[i_station].size() * sizeof(float));
    outfile.write((char*)y[i_station].data(),
                  y[i_station].size() * sizeof(float));
    outfile.write((char*)dy[i_station].data(),
                  dy[i_station].size() * sizeof(float));
    outfile.write((char*)z[i_station].data(),
                  z[i_station].size() * sizeof(float));
    outfile.write((char*)dz[i_station].data(),
                  dz[i_station].size() * sizeof(float));
    outfile.write((char*)uncrossed[i_station].data(),
                  uncrossed[i_station].size() * sizeof(int));
    outfile.write((char*)time[i_station].data(),
                  time[i_station].size() * sizeof(unsigned int));
    outfile.write((char*)deltaTime[i_station].data(),
                  deltaTime[i_station].size() * sizeof(int));
    outfile.write((char*)clusterSize[i_station].data(),
                  clusterSize[i_station].size() * sizeof(int));
  }

  outfile.close();
}
