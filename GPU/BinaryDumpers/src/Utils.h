#ifndef DUMPUTILS_H
#define DUMPUTILS_H

#include <string>
#include <boost/filesystem.hpp>

namespace DumpUtils {

bool createDirectory(boost::filesystem::path dir);

}

#endif
