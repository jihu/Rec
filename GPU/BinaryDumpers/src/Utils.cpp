#include <boost/filesystem.hpp>
#include <string>

#include "Utils.h"

namespace {
namespace fs = boost::filesystem;
}

bool DumpUtils::createDirectory(fs::path directory) {
  if (!fs::exists(directory)) {
    boost::system::error_code ec;
    bool success = fs::create_directories(directory, ec);
    success &= !ec;
    return success;
  }
  return true;
}
