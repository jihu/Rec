#include <vector>

#include <boost/filesystem.hpp>

#include "Event/RawEvent.h"
#include "Event/VPLightCluster.h"

#include "DumpFTHits.h"
#include "Utils.h"

namespace {
using std::make_unique;
using std::vector;
using std::fstream;
using std::ofstream;
using std::ios;
using std::string;
using std::to_string;

namespace fs = boost::filesystem;
}

// Declaration of the Algorithm Factory
DECLARE_COMPONENT(DumpFTHits)

DumpFTHits::DumpFTHits(const string& name, ISvcLocator* pSvcLocator)
    : Consumer(name, pSvcLocator,
               {KeyValue{"ODINLocation", LHCb::ODINLocation::Default},
                KeyValue{"FTHitsLocation", PrFTInfo::FTHitsLocation}}) {}

StatusCode DumpFTHits::initialize() {
  if (!DumpUtils::createDirectory(m_outputDirectory.value())) {
    error() << "Failed to create directory " << m_outputDirectory.value()
            << endmsg;
    return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}

void DumpFTHits::operator()(const LHCb::ODIN& odin,
                            const PrFTHitHandler<PrHit>& hitHandler) const {
  string filename =
      (m_outputDirectory.value() + "/" + to_string(odin.runNumber()) + "_" +
       to_string(odin.eventNumber()) + ".bin");

  /*Write SciFi variables for GPU to binary file */
  ofstream outfile(filename, ios::out | ios::binary);

  // SciFi
  const int n_layers_scifi = 24;
  auto scifi_x = make_unique<vector<float>[]>(n_layers_scifi);
  auto scifi_z = make_unique<vector<float>[]>(n_layers_scifi);
  auto scifi_w = make_unique<vector<float>[]>(n_layers_scifi);
  auto scifi_dxdy = make_unique<vector<float>[]>(n_layers_scifi);
  auto scifi_dzdy = make_unique<vector<float>[]>(n_layers_scifi);
  auto scifi_YMin = make_unique<vector<float>[]>(n_layers_scifi);
  auto scifi_YMax = make_unique<vector<float>[]>(n_layers_scifi);
  auto scifi_LHCbID = make_unique<vector<unsigned int>[]>(n_layers_scifi);
  auto scifi_hitPlaneCode = make_unique<vector<int>[]>(n_layers_scifi);
  auto scifi_hitZone = make_unique<vector<int>[]>(n_layers_scifi);

  for (unsigned int zone = 0; PrFTInfo::nbZones() > zone; ++zone) {
    for (const auto& hit : hitHandler.hits(zone)) {
      // get the LHCbID from the PrHit
      LHCb::LHCbID lhcbid = hit.id();

      // Fill the info for the eventual binary
      int code = 2 * hit.planeCode() + hit.zone();
      scifi_x[code].push_back(hit.x());
      scifi_z[code].push_back(hit.z());
      scifi_w[code].push_back(hit.w());
      scifi_dxdy[code].push_back(hit.dxDy());
      scifi_dzdy[code].push_back(hit.dzDy());
      scifi_YMin[code].push_back(hit.yMin());
      scifi_YMax[code].push_back(hit.yMax());
      scifi_LHCbID[code].push_back(lhcbid.lhcbID());
      scifi_hitPlaneCode[code].push_back(hit.planeCode());
      scifi_hitZone[code].push_back(hit.zone());
    }
  }

  // first the number of hits per layer in each half as header
  for (int index = 0; index < n_layers_scifi; ++index) {
    uint32_t n_hits = (int)(scifi_x[index].size());
    outfile.write((char*)&n_hits, sizeof(uint32_t));
  }

  // then the vectors containing the variables
  for (int index = 0; index < n_layers_scifi; ++index) {
    outfile.write((char*)scifi_x[index].data(),
                  scifi_x[index].size() * sizeof(float));
    outfile.write((char*)scifi_z[index].data(),
                  scifi_z[index].size() * sizeof(float));
    outfile.write((char*)scifi_w[index].data(),
                  scifi_w[index].size() * sizeof(float));
    outfile.write((char*)scifi_dxdy[index].data(),
                  scifi_dxdy[index].size() * sizeof(float));
    outfile.write((char*)scifi_dzdy[index].data(),
                  scifi_dzdy[index].size() * sizeof(float));
    outfile.write((char*)scifi_YMin[index].data(),
                  scifi_YMin[index].size() * sizeof(float));
    outfile.write((char*)scifi_YMax[index].data(),
                  scifi_YMax[index].size() * sizeof(float));
    outfile.write((char*)scifi_LHCbID[index].data(),
                  scifi_LHCbID[index].size() * sizeof(unsigned int));
    outfile.write((char*)scifi_hitPlaneCode[index].data(),
                  scifi_hitPlaneCode[index].size() * sizeof(int));
    outfile.write((char*)scifi_hitZone[index].data(),
                  scifi_hitZone[index].size() * sizeof(int));
  }
  outfile.close();
}
