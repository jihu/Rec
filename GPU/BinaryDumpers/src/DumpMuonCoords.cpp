#include <vector>

#include <boost/filesystem.hpp>

#include "DumpMuonCoords.h"
#include "Utils.h"

namespace {
using std::make_unique;
using std::vector;
using std::fstream;
using std::ofstream;
using std::ios;
using std::string;
using std::to_string;

namespace fs = boost::filesystem;
}

// Declaration of the Algorithm Factory
DECLARE_COMPONENT(DumpMuonCoords)

DumpMuonCoords::DumpMuonCoords(const string& name, ISvcLocator* pSvcLocator)
    : Consumer(name, pSvcLocator,
               {KeyValue{"ODINLocation", LHCb::ODINLocation::Default},
		   KeyValue{"MuonCoordsLocation", LHCb::MuonCoordLocation::MuonCoords}}) {}

StatusCode DumpMuonCoords::initialize() {
  if (!DumpUtils::createDirectory(m_outputDirectory.value())) {
    error() << "Failed to create directory " << m_outputDirectory.value()
            << endmsg;
    return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}

void DumpMuonCoords::operator()(const LHCb::ODIN& odin,
			      const LHCb::MuonCoords& muonCoords) const {
  string filename =
      (m_outputDirectory.value() + "/" + to_string(odin.runNumber()) + "_" +
       to_string(odin.eventNumber()) + ".bin");

  /*Write muon coord variables for GPU to binary file */
  ofstream outfile(filename, ios::out | ios::binary);

  int n_stations_muon = 4;

  auto uncrossed = make_unique<vector<int>[]>(n_stations_muon);
  // if uncrossed is true: digitTile contains one MuonTileID
  // else if crossed pads were found: digitTile contains two MuonTileIDs
  auto digitTile = make_unique<vector<int>[]>(n_stations_muon);
  auto digitTDC1 = make_unique<vector<unsigned int>[]>(n_stations_muon);
  auto digitTDC2 = make_unique<vector<unsigned int>[]>(n_stations_muon);

  std::vector<std::vector<const LHCb::MuonCoord*>> sortedCoords(n_stations_muon);
  for (auto coord : muonCoords) {       
    sortedCoords[coord->key().station()].emplace_back(coord);    
  }

  for( int station = 0 ; station < n_stations_muon ; station++ ){     
    for (const auto& coord : sortedCoords[station]) {
      uncrossed[station].push_back( (int)(coord->uncrossed()) );
      for ( LHCb::MuonTileID tileID : coord->digitTile() ) {
	digitTile[station].push_back( int(tileID) );
      }
      digitTDC1[station].push_back( coord->digitTDC1() );
      digitTDC2[station].push_back( coord->digitTDC2() );
    }
  }
  // first the number of hits per station
  for (int station = 0; station < n_stations_muon; ++station) {
    uint32_t n_hits = (int)(uncrossed[station].size());
    outfile.write((char*)&n_hits, sizeof(uint32_t));
  }
 
  // then the vectors containing the variables
  for (int station = 0; station < n_stations_muon; ++station) {
    outfile.write((char*)uncrossed[station].data(),
                  uncrossed[station].size() * sizeof(int));
    outfile.write((char*)digitTile[station].data(),
		  digitTile[station].size() * sizeof(unsigned int));
    outfile.write((char*)digitTDC1[station].data(),
                  digitTDC1[station].size() * sizeof(unsigned int));
    outfile.write((char*)digitTDC2[station].data(),
                  digitTDC2[station].size() * sizeof(unsigned int));
  }  

  outfile.close();
}
