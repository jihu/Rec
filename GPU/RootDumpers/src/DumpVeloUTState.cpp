#include <string>

#include <Event/State.h>

#include "DumpVeloUTState.h"

namespace {
using std::string;
}

DECLARE_COMPONENT(DumpVeloUTState)

DumpVeloUTState::DumpVeloUTState(const std::string& name,
                                 ISvcLocator* pSvcLocator)
    : Consumer(name, pSvcLocator,
               {KeyValue{"UpstreamTrackLocation", "Rec/Track/Upstream"}}) {}

StatusCode DumpVeloUTState::initialize() {
  auto sc = Consumer::initialize();
  if (!sc.isSuccess()) return sc;
  if (sc) m_tupleTool.retrieve();
  return sc;
}

void DumpVeloUTState::operator()(
    const std::vector<LHCb::Track>& utTracks) const {
  auto tup = m_tupleTool->nTuple(string{"veloUT_tracks"});
  for (const auto& track : utTracks) {
    if (!track.hasStateAt(LHCb::State::Location::AtTT)) continue;
    const auto state = track.stateAt(LHCb::State::Location::AtTT);
    tup->column("qop", state->qOverP());
    tup->write();
  }
}
