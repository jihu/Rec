#ifndef DUMPVELOUTSTATE_H
#define DUMPVELOUTSTATE_H 1

#include <algorithm>
#include <string>
#include <vector>

#include <Event/Track.h>
#include <GaudiAlg/Consumer.h>
#include <GaudiAlg/ITupleTool.h>

class DumpVeloUTState : public Gaudi::Functional::Consumer<void(
                            const std::vector<LHCb::Track>&)> {
 public:
  DumpVeloUTState(const std::string& name, ISvcLocator* pSvcLocator);
  StatusCode initialize() override;

  void operator()(const std::vector<LHCb::Track>& utTracks) const override;

 private:
  ToolHandle<ITupleTool> m_tupleTool{"TupleTool", this};
};

#endif  // DUMPVELOUTSTATE_H
