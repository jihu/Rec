// Include files
#include "InCaloFutureAcceptanceAlg.h"

// ============================================================================
/** @class InEcalFutureAcceptance    Alg InEcalFutureAcceptance    Alg.cpp
 *  the preconfigured instance of InCaloFutureAcceptanceAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-17
 */
// ============================================================================

struct InEcalFutureAcceptanceAlg final : InCaloFutureAcceptanceAlg {
  /// Standard constructor
  InEcalFutureAcceptanceAlg(const std::string& name, ISvcLocator* pSvc)
      : InCaloFutureAcceptanceAlg(name, pSvc) {
    using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;
    Gaudi::Functional::updateHandleLocation(
        *this, "Output", CaloFutureIdLocation("InEcal", context()));

    _setProperty("Tool", "InEcalFutureAcceptance/InEcalFuture");
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Long, LHCb::Track::Types::Downstream,
                                     LHCb::Track::Types::Ttrack));
  }
};

// ============================================================================

DECLARE_COMPONENT( InEcalFutureAcceptanceAlg )

// ============================================================================
