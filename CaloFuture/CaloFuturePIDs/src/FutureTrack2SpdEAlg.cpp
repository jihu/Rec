// Include files
#include "CaloFutureTrack2IDAlg.h"

// ============================================================================
/** @class FutureTrack2SpdEAlg FutureTrack2SpdEAlg.cpp
 *  preconfigured instance of class  CaloFutureTrack2IDAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-15
 */
struct FutureTrack2SpdEAlg final : public CaloFutureTrack2IDAlg {
  FutureTrack2SpdEAlg(const std::string& name, ISvcLocator* pSvc)
      : CaloFutureTrack2IDAlg(name, pSvc) {
    using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;
    _setProperty("Output", CaloFutureIdLocation("SpdE", context()));
    _setProperty("Filter", CaloFutureIdLocation("InSpd", context()));
    _setProperty("Tool", "FutureSpdEnergyForTrack/SpdE");
  }
};

// ============================================================================

DECLARE_COMPONENT( FutureTrack2SpdEAlg )

// ============================================================================
