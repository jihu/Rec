// Include files
#include "InCaloFutureAcceptanceAlg.h"

// ============================================================================
/** @class InPrsFutureAcceptanceAlg InPrsFutureAcceptanceAlg.cpp
 *  the preconfigured instance of InCaloFutureAcceptanceAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-17
 */
// ============================================================================

struct InPrsFutureAcceptanceAlg final : InCaloFutureAcceptanceAlg {
  /// Standard constructor
  InPrsFutureAcceptanceAlg(const std::string& name, ISvcLocator* pSvc)
      : InCaloFutureAcceptanceAlg(name, pSvc) {
    using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;

    Gaudi::Functional::updateHandleLocation(*this, "Output",
                                            CaloFutureIdLocation("InPrs", context()));

    _setProperty("Tool", "InPrsFutureAcceptance/InPrs");
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Long, LHCb::Track::Types::Downstream,
                                     LHCb::Track::Types::Ttrack));
  }
};

// ============================================================================

DECLARE_COMPONENT( InPrsFutureAcceptanceAlg )

// ============================================================================
