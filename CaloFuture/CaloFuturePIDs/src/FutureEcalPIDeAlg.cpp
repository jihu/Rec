// Include files
#include "CaloFutureID2DLL.h"

// ============================================================================
/** @class FutureEcalPIDeAlg  FutureEcalPIDeAlg.cpp
 *  The preconfigured instance of class CaloFutureID2DLL
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-18
 */
// ============================================================================

class FutureEcalPIDeAlg final : public CaloFutureID2DLL {
 public:
  FutureEcalPIDeAlg(const std::string& name, ISvcLocator* pSvc)
      : CaloFutureID2DLL(name, pSvc) {
    using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;

    Gaudi::Functional::updateHandleLocation(
        *this, "Input", CaloFutureIdLocation("EcalChi2", context()));
    Gaudi::Functional::updateHandleLocation(
        *this, "Output", CaloFutureIdLocation("EcalPIDe", context()));

    _setProperty("nVlong"  , Gaudi::Utils::toString(2500));
    _setProperty("nVdown"  , Gaudi::Utils::toString(2500));
    _setProperty("nVTtrack", Gaudi::Utils::toString(2500));
    _setProperty("nMlong"  , Gaudi::Utils::toString(100 * Gaudi::Units::GeV));
    _setProperty("nMdown"  , Gaudi::Utils::toString(100 * Gaudi::Units::GeV));
    _setProperty("nMTtrack", Gaudi::Utils::toString(100 * Gaudi::Units::GeV));

    _setProperty("HistogramL"   , "DLL_Long");
    _setProperty("HistogramD"   , "DLL_Downstream");
    _setProperty("HistogramT"   , "DLL_Ttrack");
    _setProperty("ConditionName", "Conditions/ParticleID/Calo/EcalPIDe");

    _setProperty("HistogramL_THS", "CaloFuturePIDs/CALO/ECALPIDE/h3");
    _setProperty("HistogramD_THS", "CaloFuturePIDs/CALO/ECALPIDE/h5");
    _setProperty("HistogramT_THS", "CaloFuturePIDs/CALO/ECALPIDE/h6");

    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Long, LHCb::Track::Types::Ttrack,
                                     LHCb::Track::Types::Downstream));
  };
};

// ============================================================================

DECLARE_COMPONENT( FutureEcalPIDeAlg )

// ============================================================================
