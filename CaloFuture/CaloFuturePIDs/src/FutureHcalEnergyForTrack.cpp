// Include files
#include "CaloFutureEnergyForTrack.h"

// ============================================================================
/** @class FutureHcalEnergyForTrack
 *  The concrete preconfigured insatnce for CaloFutureEnergyForTrack tool
 *  along the track line
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================

class FutureHcalEnergyForTrack final : public CaloFutureEnergyForTrack {
 public:
  FutureHcalEnergyForTrack(const std::string& type, const std::string& name,
                     const IInterface* parent)
      : CaloFutureEnergyForTrack(type, name, parent) {
    setProperty("DataAddress", LHCb::CaloDigitLocation::Hcal).ignore();
    setProperty("CalorimeterFuture", DeCalorimeterLocation::Hcal).ignore();
    setProperty("MorePlanes", 5).ignore();
    setProperty("AddNeigbours", 0).ignore();
    setProperty("Tolerance", 15.0 * Gaudi::Units::mm).ignore();
  }
};

// ============================================================================

DECLARE_COMPONENT( FutureHcalEnergyForTrack )
