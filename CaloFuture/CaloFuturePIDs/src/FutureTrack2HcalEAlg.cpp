// Include files
#include "CaloFutureTrack2IDAlg.h"

// ============================================================================
/** @class FutureTrack2HcalEAlg FutureTrack2HcalEAlg.cpp
 *  preconfigured instance of class  CaloFutureTrack2IDAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-15
 */
struct FutureTrack2HcalEAlg final : public CaloFutureTrack2IDAlg {
  FutureTrack2HcalEAlg(const std::string& name, ISvcLocator* pSvc)
      : CaloFutureTrack2IDAlg(name, pSvc) {
    using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;
    _setProperty("Output", CaloFutureIdLocation("HcalE", context()));
    _setProperty("Filter", CaloFutureIdLocation("InHcal", context()));
    _setProperty("Tool", "FutureHcalEnergyForTrack/HcalEFuture");
  }
};

// ============================================================================

DECLARE_COMPONENT( FutureTrack2HcalEAlg )

// ============================================================================
