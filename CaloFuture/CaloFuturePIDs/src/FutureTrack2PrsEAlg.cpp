// Include files
#include "CaloFutureTrack2IDAlg.h"

// ============================================================================
/** @class FutureTrack2PrsEAlg FutureTrack2PrsEAlg.cpp
 *  preconfigured instance of class  CaloFutureTrack2IDAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-15
 */
struct FutureTrack2PrsEAlg final : public CaloFutureTrack2IDAlg {
  FutureTrack2PrsEAlg(const std::string& name, ISvcLocator* pSvc)
      : CaloFutureTrack2IDAlg(name, pSvc) {
    using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;
    _setProperty("Output", CaloFutureIdLocation("PrsE", context()));
    _setProperty("Filter", CaloFutureIdLocation("InPrs", context()));
    _setProperty("Tool", "FuturePrsEnergyForTrack/PrsE");
  }
};

// ============================================================================

DECLARE_COMPONENT( FutureTrack2PrsEAlg )

// ============================================================================
