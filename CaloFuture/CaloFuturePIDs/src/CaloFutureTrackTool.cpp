// Include files
#include "CaloFutureTrackTool.h"

// ============================================================================
/** @file
 *  Implementation file for class CaloFuture::CaloFutureTrackTool
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-05-28
 */
// ============================================================================
/// standard constructor
// ============================================================================
CaloFuture::CaloFutureTrackTool::CaloFutureTrackTool( const std::string& type   , // ?
                                    const std::string& name   ,
                                    const IInterface*  parent )
  : GaudiTool ( type , name , parent )
{
  _setProperty("CheckTracks", "false");
  declareProperty("Extrapolator", m_extrapolator);
}

//==============================================================================
/// initialize the tool
//==============================================================================

StatusCode CaloFuture::CaloFutureTrackTool::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) { return sc ; }
  //
  if ( propsPrint() || msgLevel ( MSG::DEBUG ) || m_use.check() )
  { info () << m_use << endmsg ; } ;
  //
  if ( !m_detectorName.empty() )
  { m_calo = getDet<DeCalorimeter> ( detectorName()  ); }
  else { Warning("empty detector name!"); }

  // Retrieve tools
  sc = m_extrapolator.retrieve();
  if (sc.isFailure()) {return sc;}
  sc = m_fastExtrapolator.retrieve();
  if (sc.isFailure()) {return sc;}

  return StatusCode::SUCCESS ;
}

//==============================================================================

void CaloFuture::CaloFutureTrackTool::_setProperty(const std::string &p,
                                       const std::string &v) {
  StatusCode sc = setProperty(p, v);
  if (!sc) {
    warning() << " setting Property " << p << " to " << v << " FAILED"
              << endmsg;
  }
}
