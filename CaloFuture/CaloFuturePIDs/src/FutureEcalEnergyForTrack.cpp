// Include files
#include "CaloFutureEnergyForTrack.h"

// ============================================================================
/** @class FutureEcalEnergyForTrack
 *  The concrete preconfigured insatnce for CaloFutureEnergyForTrack tool
 *  along the track line
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================

class FutureEcalEnergyForTrack final : public CaloFutureEnergyForTrack {
 public:
  FutureEcalEnergyForTrack(const std::string& type, const std::string& name,
                     const IInterface* parent)
      : CaloFutureEnergyForTrack(type, name, parent) {
    setProperty("DataAddress", LHCb::CaloDigitLocation::Ecal).ignore();
    setProperty("MorePlanes", 3).ignore();  /// 3 additional planes
    setProperty("AddNeigbours", 0).ignore();
    setProperty("Tolerance", 5 * Gaudi::Units::mm).ignore();
    setProperty("CalorimeterFuture", DeCalorimeterLocation::Ecal).ignore();
  }
};

// ============================================================================

DECLARE_COMPONENT( FutureEcalEnergyForTrack )
