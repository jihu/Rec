// Include files
#include "CaloFuturePhotonIdAlg.h"
#include "DetDesc/HistoParam.h"
#include <GaudiUtils/Aida2ROOT.h>
#include <math.h>

//------------------------------------------------------------------------------
// Implementation file for class : CaloFuturePhotonIdAlg
//
// 2010-02-27 : Olivier Deschamps
//------------------------------------------------------------------------------

DECLARE_COMPONENT( CaloFuturePhotonIdAlg )

//==============================================================================
// Standard constructor, initializes variables
//==============================================================================

CaloFuturePhotonIdAlg::CaloFuturePhotonIdAlg(const std::string &name,
                                 ISvcLocator *pSvcLocator)
    : MergingTransformer(name, pSvcLocator, {"Inputs", {}}, {"Output", {}}) {
  // TODO : split PID estimator (Prs-Spd-Ecal(Chi2, seed/cluster) + add
  // estimator ( E4/E9, Hcal/Ecal, shower-shape,  ...)
  // and let NeutralProtoParticle to combine ...
}

//==============================================================================
// Initialization
//==============================================================================
StatusCode CaloFuturePhotonIdAlg::initialize() {
  StatusCode sc = MergingTransformer::initialize();
  if (sc.isFailure())
    return sc;
  if (msgLevel(MSG::DEBUG))
    debug() << "==> Initialize" << endmsg;

  // Retrieve tools
  m_counterStat.retrieve();
  sc = m_estimator.retrieve();
  if (sc.isFailure())
    return sc;

  // Report
  if (m_dlnL) {
    info() << "PhotonId : Delta Log Likelihood calculation." << endmsg;
  } else {
    info() << "PhotonId : Likelihood estimator." << endmsg;
  }
  // Initialize histogram access
  if (m_useCondDB && !existDet<DataObject>(detSvc(), m_conditionName)) {
    warning() << "Initialise: Condition '" << m_conditionName
              << "' not found -- switch to reading the DLLs from THS!"
              << endmsg;
    m_useCondDB = false;
  }

  sc = m_useCondDB ? initializeWithCondDB() : initializeWithoutCondDB();

  // context-dependent inputs/output (if not defined via options)
  using LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation;
  using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;

  // Update inputs to this default if null
  if (getProperty("Inputs").toString() == "[  ]"){
    std::string loc;
    if (m_type == "MergedID") {
      loc = "MergedPi0s";
    } else if (m_type == "PhotonID") {
      loc = "Photons";
    } else if (m_type == "PhotonFromMergedID") {
      loc = "SplitPhotons";
    }
    std::vector<std::string> inputs = {CaloFutureHypoLocation(loc, context())};
    Gaudi::Functional::updateHandleLocations(*this, "Inputs", inputs);
  }
  // Update output to this default if null
  if (getProperty("Output").toString() == ""){
    std::string output = CaloFutureIdLocation(m_type, context());
    Gaudi::Functional::updateHandleLocation(*this, "Output", output);
  }


  // Warning : the algorithm settings overwrite the caloHypo2CaloFuture default
  // settings
  std::string seed = m_seed ? "true" : "false";
  std::string line = m_extrapol ? "true" : "false";
  std::string neig = m_neig ? "true" : "false";
  m_estimator->hypo2CaloFuture()->_setProperty("Seed", seed).ignore();
  m_estimator->hypo2CaloFuture()->_setProperty("PhotonLine", line).ignore();
  m_estimator->hypo2CaloFuture()->_setProperty("AddNeighbors", neig).ignore();
  m_estimator->_setProperty("SkipNeutralID", "true").ignore(); // avoid loop

  return sc;
}

//==============================================================================

Table CaloFuturePhotonIdAlg::operator()(const HyposList &hyposlist) const {

// StatusCode CaloFuturePhotonIdAlg::execute() {
  if (msgLevel(MSG::DEBUG))
    debug() << "==> Execute" << endmsg;

  // create the table
  auto table = Table(200);

  // fill the table
  for (const auto hypos : hyposlist) {
    // Skip if null
    if (!hypos) {
      if (m_counterStat->isQuiet())
        counter("Empty location ") += 1;
      continue;
    }
    // Retrieve location on TES
    const auto loc = hypos->registry()->identifier();
    if (m_counterStat->isVerbose())
      counter("#Hypos in " + loc) += hypos->size();

    // Loop over hypos
    for (const auto hypo : *hypos){
      if (hypo == nullptr) {
        if (m_counterStat->isQuiet())
          counter("hypo points to NULL in " + loc) += 1;
        continue;
      }
      const double l = m_isRunnable ? likelihood(hypo) : -999.;
      if (m_counterStat->isVerbose())
        counter("likelihood") += l;
      table.i_push(hypo, (float)(l));
    }
  }
  table.i_sort();

  // statistics
  if (m_counterStat->isQuiet()){
    const auto inputs = getProperty("Inputs").toString();
    const auto output = getProperty("Output").toString();
    counter(inputs + " ==> " + output) += table.i_relations().size();
  }

  return table;
}

//==============================================================================

double CaloFuturePhotonIdAlg::likelihood(const LHCb::CaloHypo *hypo) const {
  // Get the relevant information - basic checks
  if (0 == hypo) {
    Warning("CaloHypo points to NULL", StatusCode::SUCCESS).ignore();
    if (m_counterStat->isQuiet())
      counter("Null hypo") += 1;
    return -999.;
  }

  double energy, et, eSeed, ePrs;
  int nSpd;
  unsigned int area;

  double chi2 = -999.;

  // parameter evaluation
  if (evalParam(hypo, energy, et, eSeed, nSpd, ePrs, area).isFailure())
    return -999.;

  // get chi2
  chi2 = -999.;
  if (m_tracking)
    chi2 = m_estimator->data(hypo, CaloFutureDataType::ClusterMatch, +999.);
  // evaluate
  return evalLikelihood(energy, et, eSeed, nSpd, ePrs, chi2, area);
}

//==============================================================================

StatusCode CaloFuturePhotonIdAlg::evalParam(const LHCb::CaloHypo *hypo,
                                      double &energy, double &et, double &eSeed,
                                      int &nSpd, double &ePrs,
                                      unsigned int &area) const {
  using namespace CaloFutureDataType;
  energy = m_estimator->data(hypo, HypoE);
  et = m_estimator->data(hypo, HypoEt);
  eSeed = m_estimator->data(hypo, E1Hypo);
  nSpd = (int)m_estimator->data(hypo, ToSpdM);
  ePrs = m_estimator->data(hypo, ToPrsE);
  area = LHCb::CaloCellID(
             (LHCb::CaloCellID::ContentType)m_estimator->data(hypo, CellID))
             .area();
  if (!m_estimator->status())
    return StatusCode::FAILURE;
  return StatusCode::SUCCESS;
}

//==============================================================================

double CaloFuturePhotonIdAlg::evalLikelihood(double energy, double et, double eSeed,
                                       int nSpd, double ePrs, double chi2,
                                       unsigned int area) const {
  // Initialization
  double estimator;
  double signal, backgr;
  const double epsilon = 1.e-20;
  // SPD Hit

  signal = epsilon;
  backgr = epsilon;

  if (nSpd == 0) {
    signal = CaloFuturePhotonIdAlg::dLL(energy, ePrs, chi2, eSeed,
                                  CaloFuturePhotonIdAlg::SIGNAL, area);
    if (m_isRunnable) {
      backgr = CaloFuturePhotonIdAlg::dLL(energy, ePrs, chi2, eSeed,
                                    CaloFuturePhotonIdAlg::BACKGROUND, area);
    }
  } else {
    signal = CaloFuturePhotonIdAlg::dLL(energy, ePrs, chi2, eSeed,
                                  CaloFuturePhotonIdAlg::SIGNAL_SPD, area);
    if (m_isRunnable) {
      backgr = CaloFuturePhotonIdAlg::dLL(energy, ePrs, chi2, eSeed,
                                    CaloFuturePhotonIdAlg::BACKGROUND_SPD, area);
    }
  }

  if (m_dlnL) {
    if (m_isRunnable) {
      if (signal < epsilon)
        signal = epsilon;
      if (backgr < epsilon)
        backgr = epsilon;
      estimator = log(signal) - log(backgr);
    } else
      estimator = -999.;
  } else {
    if (m_isRunnable) {
      estimator = (signal + backgr > 0.) ? signal / (signal + backgr) : -1.;
    } else {
      estimator = -999.;
    }
  }

  if (msgLevel(MSG::DEBUG)) {
    debug() << "Photon Candidate :" << endmsg;
    debug() << " -E         =" << energy << endmsg;
    debug() << " -Et        =" << et << endmsg;
    debug() << " -#Spd hits =" << nSpd << endmsg;
    debug() << " -EPrs      =" << ePrs << endmsg;
    debug() << " -Chi2      =" << chi2 << endmsg;
    debug() << " -ESeed     =" << eSeed << endmsg;
    if (m_dlnL)
      debug() << "      => DlnL     = " << estimator << endmsg;
    else
      debug() << "      => estimator= " << estimator << endmsg;
  }

  return estimator;
}

//==============================================================================

StatusCode CaloFuturePhotonIdAlg::initializeWithCondDB() {
  info() << "init with CondDB, m_conditionName = " << m_conditionName.value() << endmsg;
  try {
    registerCondition(m_conditionName, m_cond, &CaloFuturePhotonIdAlg::i_updateDLL);
  } catch (GaudiException &e) {
    fatal() << e << endmsg;
    m_isRunnable = false;
    return StatusCode::FAILURE;
  }
  StatusCode sc = runUpdate(); // load the conditions
  return sc;
}

//==============================================================================

StatusCode CaloFuturePhotonIdAlg::initializeWithoutCondDB() {
  info() << "init w/o CondDB, pdf histos path is " << m_histo_path << endmsg;

  m_Sig_EPrs_10 = locateHistoOnDisk(m_title_Sig_EPrs_10);
  m_Sig_EPrs_11 = locateHistoOnDisk(m_title_Sig_EPrs_11);
  m_Sig_EPrs_12 = locateHistoOnDisk(m_title_Sig_EPrs_12);
  m_Sig_EPrs_15 = locateHistoOnDisk(m_title_Sig_EPrs_15);
  m_Sig_EPrs_16 = locateHistoOnDisk(m_title_Sig_EPrs_16);
  m_Sig_EPrs_17 = locateHistoOnDisk(m_title_Sig_EPrs_17);

  m_Sig_Chi2_20 = locateHistoOnDisk(m_title_Sig_Chi2_20);
  m_Sig_Chi2_21 = locateHistoOnDisk(m_title_Sig_Chi2_21);
  m_Sig_Chi2_22 = locateHistoOnDisk(m_title_Sig_Chi2_22);
  m_Sig_Chi2_25 = locateHistoOnDisk(m_title_Sig_Chi2_25);
  m_Sig_Chi2_26 = locateHistoOnDisk(m_title_Sig_Chi2_26);
  m_Sig_Chi2_27 = locateHistoOnDisk(m_title_Sig_Chi2_27);

  m_Sig_Seed_30 = locateHistoOnDisk(m_title_Sig_Seed_30);
  m_Sig_Seed_31 = locateHistoOnDisk(m_title_Sig_Seed_31);
  m_Sig_Seed_32 = locateHistoOnDisk(m_title_Sig_Seed_32);
  m_Sig_Seed_35 = locateHistoOnDisk(m_title_Sig_Seed_35);
  m_Sig_Seed_36 = locateHistoOnDisk(m_title_Sig_Seed_36);
  m_Sig_Seed_37 = locateHistoOnDisk(m_title_Sig_Seed_37);

  m_Bkg_EPrs_110 = locateHistoOnDisk(m_title_Bkg_EPrs_110);
  m_Bkg_EPrs_111 = locateHistoOnDisk(m_title_Bkg_EPrs_111);
  m_Bkg_EPrs_112 = locateHistoOnDisk(m_title_Bkg_EPrs_112);
  m_Bkg_EPrs_115 = locateHistoOnDisk(m_title_Bkg_EPrs_115);
  m_Bkg_EPrs_116 = locateHistoOnDisk(m_title_Bkg_EPrs_116);
  m_Bkg_EPrs_117 = locateHistoOnDisk(m_title_Bkg_EPrs_117);

  m_Bkg_Chi2_120 = locateHistoOnDisk(m_title_Bkg_Chi2_120);
  m_Bkg_Chi2_121 = locateHistoOnDisk(m_title_Bkg_Chi2_121);
  m_Bkg_Chi2_122 = locateHistoOnDisk(m_title_Bkg_Chi2_122);
  m_Bkg_Chi2_125 = locateHistoOnDisk(m_title_Bkg_Chi2_125);
  m_Bkg_Chi2_126 = locateHistoOnDisk(m_title_Bkg_Chi2_126);
  m_Bkg_Chi2_127 = locateHistoOnDisk(m_title_Bkg_Chi2_127);

  m_Bkg_Seed_130 = locateHistoOnDisk(m_title_Bkg_Seed_130);
  m_Bkg_Seed_131 = locateHistoOnDisk(m_title_Bkg_Seed_131);
  m_Bkg_Seed_132 = locateHistoOnDisk(m_title_Bkg_Seed_132);
  m_Bkg_Seed_135 = locateHistoOnDisk(m_title_Bkg_Seed_135);
  m_Bkg_Seed_136 = locateHistoOnDisk(m_title_Bkg_Seed_136);
  m_Bkg_Seed_137 = locateHistoOnDisk(m_title_Bkg_Seed_137);

  return StatusCode::SUCCESS;
}

//==============================================================================

StatusCode CaloFuturePhotonIdAlg::i_updateDLL() {
  if (msgLevel(MSG::DEBUG))
    debug() << "i_updateDLL() called" << endmsg;
  if (!m_cond)
    return StatusCode::FAILURE;

  try {
    m_Sig_EPrs_10 = locateHistoOnDB(m_title_Sig_EPrs_10);
    m_Sig_EPrs_11 = locateHistoOnDB(m_title_Sig_EPrs_11);
    m_Sig_EPrs_12 = locateHistoOnDB(m_title_Sig_EPrs_12);
    m_Sig_EPrs_15 = locateHistoOnDB(m_title_Sig_EPrs_15);
    m_Sig_EPrs_16 = locateHistoOnDB(m_title_Sig_EPrs_16);
    m_Sig_EPrs_17 = locateHistoOnDB(m_title_Sig_EPrs_17);

    m_Sig_Chi2_20 = locateHistoOnDB(m_title_Sig_Chi2_20);
    m_Sig_Chi2_21 = locateHistoOnDB(m_title_Sig_Chi2_21);
    m_Sig_Chi2_22 = locateHistoOnDB(m_title_Sig_Chi2_22);
    m_Sig_Chi2_25 = locateHistoOnDB(m_title_Sig_Chi2_25);
    m_Sig_Chi2_26 = locateHistoOnDB(m_title_Sig_Chi2_26);
    m_Sig_Chi2_27 = locateHistoOnDB(m_title_Sig_Chi2_27);

    m_Sig_Seed_30 = locateHistoOnDB(m_title_Sig_Seed_30);
    m_Sig_Seed_31 = locateHistoOnDB(m_title_Sig_Seed_31);
    m_Sig_Seed_32 = locateHistoOnDB(m_title_Sig_Seed_32);
    m_Sig_Seed_35 = locateHistoOnDB(m_title_Sig_Seed_35);
    m_Sig_Seed_36 = locateHistoOnDB(m_title_Sig_Seed_36);
    m_Sig_Seed_37 = locateHistoOnDB(m_title_Sig_Seed_37);

    m_Bkg_EPrs_110 = locateHistoOnDB(m_title_Bkg_EPrs_110);
    m_Bkg_EPrs_111 = locateHistoOnDB(m_title_Bkg_EPrs_111);
    m_Bkg_EPrs_112 = locateHistoOnDB(m_title_Bkg_EPrs_112);
    m_Bkg_EPrs_115 = locateHistoOnDB(m_title_Bkg_EPrs_115);
    m_Bkg_EPrs_116 = locateHistoOnDB(m_title_Bkg_EPrs_116);
    m_Bkg_EPrs_117 = locateHistoOnDB(m_title_Bkg_EPrs_117);

    m_Bkg_Chi2_120 = locateHistoOnDB(m_title_Bkg_Chi2_120);
    m_Bkg_Chi2_121 = locateHistoOnDB(m_title_Bkg_Chi2_121);
    m_Bkg_Chi2_122 = locateHistoOnDB(m_title_Bkg_Chi2_122);
    m_Bkg_Chi2_125 = locateHistoOnDB(m_title_Bkg_Chi2_125);
    m_Bkg_Chi2_126 = locateHistoOnDB(m_title_Bkg_Chi2_126);
    m_Bkg_Chi2_127 = locateHistoOnDB(m_title_Bkg_Chi2_127);

    m_Bkg_Seed_130 = locateHistoOnDB(m_title_Bkg_Seed_130);
    m_Bkg_Seed_131 = locateHistoOnDB(m_title_Bkg_Seed_131);
    m_Bkg_Seed_132 = locateHistoOnDB(m_title_Bkg_Seed_132);
    m_Bkg_Seed_135 = locateHistoOnDB(m_title_Bkg_Seed_135);
    m_Bkg_Seed_136 = locateHistoOnDB(m_title_Bkg_Seed_136);
    m_Bkg_Seed_137 = locateHistoOnDB(m_title_Bkg_Seed_137);
  } catch (GaudiException &exc) {
    fatal() << "DLL update failed! msg ='" << exc << "'" << endmsg;
    return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}

//==============================================================================

TH2D *CaloFuturePhotonIdAlg::locateHistoOnDisk(std::string histoname) const {
  TH2D *histo = nullptr;
  if (!histoname.empty()) {
    AIDA::IHistogram2D *aida =
        get<AIDA::IHistogram2D>(histoSvc(), m_histo_path + histoname);
    if (aida == nullptr) {
      warning() << "Could not find AIDA::IHistogram2D* "
                << m_histo_path + histoname << "." << endmsg;
      m_isRunnable = false;
      return nullptr;
    }
    histo = Gaudi::Utils::Aida2ROOT::aida2root(aida);
  }
  return histo;
}

//==============================================================================

TH2D *CaloFuturePhotonIdAlg::locateHistoOnDB(std::string histoname) const {
  TH2D *histo = nullptr;
  if (!histoname.empty()) {
    histo = reinterpret_cast<TH2D *>(
        &m_cond->param<DetDesc::Params::Histo2D>(histoname));
  }
  return histo;
}
