// Include files
#include "InCaloFutureAcceptance.h"

// ============================================================================
/** @class InEcalFutureAcceptance    
 *  The precofigured instance of InCaloFutureAcceptance Tool
 *  @author Victor EGORYCHEV Victor.Egorychev@cern.ch
 *  @author Vanya  BELYAEV    ibelyaev@phsycis.syr.edu
 *  @date   2006-05-28
 */
// ============================================================================

struct InEcalFutureAcceptance     final : InCaloFutureAcceptance {
  /// standard constructor
  InEcalFutureAcceptance(const std::string& type, const std::string& name,
                   const IInterface* parent)
      : InCaloFutureAcceptance(type, name, parent) {
    _setProperty("CalorimeterFuture", DeCalorimeterLocation::Ecal);
    _setProperty("UseFiducial", "true");
    _setProperty("Tolerance", "5");  /// 5 * Gaudi::Units::mm
  };

  /// C++11 non-copyable idiom
  InEcalFutureAcceptance() = delete;
  InEcalFutureAcceptance(const InEcalFutureAcceptance&) = delete;
  InEcalFutureAcceptance& operator=(const InEcalFutureAcceptance&) = delete;
};

// ============================================================================

DECLARE_COMPONENT( InEcalFutureAcceptance )

// ============================================================================
