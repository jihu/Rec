// include files
#include <type_traits>
#include "Relations/Relation1D.h"
#include "InCaloFutureAcceptanceAlg.h"

// ============================================================================
/** @file
 *  Implementation file for class InCaloFutureAcceptanceAlg
 *  @Date 2006-06-17
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================

DECLARE_COMPONENT( InCaloFutureAcceptanceAlg )

// ============================================================================
/// Standard protected constructor
// ============================================================================

InCaloFutureAcceptanceAlg::InCaloFutureAcceptanceAlg(const std::string& name,
                                         ISvcLocator* pSvc)
    : Transformer(name, pSvc, KeyValue{"Inputs", ""}, KeyValue{"Output", ""}) {
  // context-dependent default track container
  // (Context only available after baseclass is contructed)
  Gaudi::Functional::updateHandleLocation(*this, "Inputs", LHCb::CaloFutureAlgUtils::TrackLocations(context()).front());
}

// ============================================================================
// algorithm execution
// ============================================================================

Table InCaloFutureAcceptanceAlg::operator()(const LHCb::Tracks& tracks) const {
  // a trivial check
  Assert(m_tool, "InAcceptance-tool  is invalid!");

  if (0 == tracks.size()) {
    if (msgLevel(MSG::DEBUG))
      debug() << "No tracks retrieved from container" << endmsg;
  }

  Table table(100);

  size_t nAccept = 0;
  // loop over all tracks in the container
  for (const auto& track : tracks) {
    if (!use(track)) {
      continue;
    }  // CONTINUE
    const bool result = m_tool->inAcceptance(track);
    // fill the relation table
    table.i_push(track, result);  // ATTENTION: i-push is used
    if (result) {
      ++nAccept;
    }
  }
  // MANDATORY: i_sort after i_push
  table.i_sort();

  // a bit of statistics
  m_nTracks += tracks.size();
  m_nAccept += nAccept;
  m_nLinks  += table.i_relations().size();

  return table;
}
