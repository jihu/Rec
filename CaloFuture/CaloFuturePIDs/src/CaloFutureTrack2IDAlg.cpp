// Include files
#include "CaloFutureTrack2IDAlg.h"
#include <type_traits>

// ============================================================================

DECLARE_COMPONENT( CaloFutureTrack2IDAlg )

// ============================================================================
/// Standard protected constructor
// ============================================================================

CaloFutureTrack2IDAlg::CaloFutureTrack2IDAlg(const std::string &name, ISvcLocator *pSvc)
    : Transformer(name, pSvc, {KeyValue{"Inputs",""},KeyValue{"Filter",""}}, KeyValue{"Output",""}) {
  _setProperty("StatPrint", "false");
  // track types:
  _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                   LHCb::Track::Types::Long, 
                                   LHCb::Track::Types::Downstream,
                                   LHCb::Track::Types::Ttrack));

  // context-dependent default track container
  Gaudi::Functional::updateHandleLocation(*this, "Inputs", LHCb::CaloFutureAlgUtils::TrackLocations(context()).front());
}

// ============================================================================
/// standard algorithm execution
// ============================================================================
Table CaloFutureTrack2IDAlg::operator()(const LHCb::Tracks& tracks, const Filter& filter) const {
  static_assert(std::is_base_of<LHCb::CaloFuture2Track::ITrEvalTable, Table>::value, "Table must inherit from ITrEvalTable");

  Table table(100);

  // loop over all tracks
  for (const auto& track : tracks) {
    // use the track ?
    if (!use(track)) {
      continue;
    } // CONTINUE
    // use filter ?
    const auto r = filter.relations(track);
    // no positive information? skip!
    if (r.empty() || !r.front().to()) {
      continue;
    } // CONTINUE
    double value = 0;
    StatusCode sc = m_tool->process(track, value);
    if (sc.isFailure()) {
      Warning(" Failure from the tool, skip the track!", sc).ignore();
      continue;
    }
    // make a relations (fast, efficient, call for i_sort is mandatory!)
    table.i_push(track, value); // NB! i_push
  }
  // MANDATORY after i_push! // NB! i_sort
  table.i_sort();

  m_nTracks += tracks.size();
  auto links = table.i_relations();
  m_nLinks  += links.size();
  //for( const auto& link : links) m_nEnergy += link.to() / Gaudi::Units::GeV;
  auto c = m_nEnergy.buffer();
  for(const auto& link : links) c += link.to() / Gaudi::Units::GeV;

  return table;
}
