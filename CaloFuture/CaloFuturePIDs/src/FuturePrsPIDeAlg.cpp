// Include files
#include "CaloFutureID2DLL.h"

// ============================================================================
/** @class FuturePrsPIDeAlg  FuturePrsPIDeAlg.cpp
 *  The preconfigured instance of class CaloFutureID2DLL
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-18
 */
// ============================================================================

class FuturePrsPIDeAlg final : public CaloFutureID2DLL {
 public:
  FuturePrsPIDeAlg(const std::string& name, ISvcLocator* pSvc)
      : CaloFutureID2DLL(name, pSvc) {
    using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;

    Gaudi::Functional::updateHandleLocation(
        *this, "Input", CaloFutureIdLocation("PrsE", context()));
    Gaudi::Functional::updateHandleLocation(
        *this, "Output", CaloFutureIdLocation("PrsPIDe", context()));

    _setProperty("nVlong"  , Gaudi::Utils::toString(100 * Gaudi::Units::MeV));
    _setProperty("nVdown"  , Gaudi::Utils::toString(100 * Gaudi::Units::MeV));
    _setProperty("nVTtrack", Gaudi::Utils::toString(100 * Gaudi::Units::MeV));
    _setProperty("nMlong"  , Gaudi::Utils::toString(100 * Gaudi::Units::GeV));
    _setProperty("nMdown"  , Gaudi::Utils::toString(100 * Gaudi::Units::GeV));
    _setProperty("nMTtrack", Gaudi::Utils::toString(100 * Gaudi::Units::GeV));

    _setProperty("HistogramL"   , "DLL_Long");
    _setProperty("HistogramD"   , "DLL_Downstream");
    _setProperty("HistogramT"   , "DLL_Ttrack");
    _setProperty("ConditionName", "Conditions/ParticleID/Calo/PrsPIDe");

    _setProperty("HistogramL_THS", "CaloFuturePIDs/CALO/PRSPIDE/h3");
    _setProperty("HistogramD_THS", "CaloFuturePIDs/CALO/PRSPIDE/h5");
    _setProperty("HistogramT_THS", "CaloFuturePIDs/CALO/PRSPIDE/h6");

    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Long, LHCb::Track::Types::Ttrack,
                                     LHCb::Track::Types::Downstream));
  };
};

// ============================================================================

DECLARE_COMPONENT( FuturePrsPIDeAlg )

// ============================================================================
