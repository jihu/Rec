// Include files
#include "CaloFutureEnergyForTrack.h"

// ============================================================================
/** @class FuturePrsEnergyForTrack
 *  The concrete preconfigured insatnce for CaloFutureEnergyForTrack tool
 *  along the track line
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================

class FuturePrsEnergyForTrack final : public CaloFutureEnergyForTrack {
 public:
  FuturePrsEnergyForTrack(const std::string& type, const std::string& name,
                    const IInterface* parent)
      : CaloFutureEnergyForTrack(type, name, parent) {
    _setProperty("DataAddress", LHCb::CaloDigitLocation::Prs);
    _setProperty("Tolerance", "2");  /// 2 * Gaudi::Units::mm
    _setProperty("CalorimeterFuture", DeCalorimeterLocation::Prs);
  };
};

// ============================================================================

DECLARE_COMPONENT( FuturePrsEnergyForTrack )
