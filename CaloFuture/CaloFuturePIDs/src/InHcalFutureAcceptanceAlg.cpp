// Include files
#include "InCaloFutureAcceptanceAlg.h"

// ============================================================================
/** @class InHcalFutureAcceptanceAlg InHcalFutureAcceptanceAlg.cpp
 *  the preconfigured instance of InCaloFutureAcceptanceAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-17
 */
// ============================================================================

struct InHcalFutureAcceptanceAlg final : InCaloFutureAcceptanceAlg {
  /// Standard constructor
  InHcalFutureAcceptanceAlg(const std::string& name, ISvcLocator* pSvc)
      : InCaloFutureAcceptanceAlg(name, pSvc) {
    using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;

    Gaudi::Functional::updateHandleLocation(
        *this, "Output", CaloFutureIdLocation("InHcal", context()));

    _setProperty("Tool", "InHcalFutureAcceptance/InHcalFuture");
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Long, LHCb::Track::Types::Downstream,
                                     LHCb::Track::Types::Ttrack));
  }
};

// ============================================================================

DECLARE_COMPONENT( InHcalFutureAcceptanceAlg )

// ============================================================================
