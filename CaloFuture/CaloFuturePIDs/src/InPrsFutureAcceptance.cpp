// Include files
#include "InCaloFutureAcceptance.h"

// ============================================================================
/** @class InPrsFutureAcceptance     
 *  The precofigured instance of InCaloFutureAcceptance Tool
 *  @author Victor EGORYCHEV Victor.Egorychev@cern.ch
 *  @author Vanya  BELYAEV    ibelyaev@phsycis.syr.edu
 *  @date   2006-05-28
 */
// ============================================================================

struct InPrsFutureAcceptance final : InCaloFutureAcceptance {
  /// standard constructor
  InPrsFutureAcceptance(const std::string& type, const std::string& name,
                  const IInterface* parent)
      : InCaloFutureAcceptance(type, name, parent) {
    _setProperty("CalorimeterFuture", DeCalorimeterLocation::Prs);
    _setProperty("UseFiducial", "false");
    _setProperty("Tolerance", "10");  /// 10 * Gaudi::Units::mm
  };

  /// C++11 non-copyable idiom
  InPrsFutureAcceptance() = delete;
  InPrsFutureAcceptance(const InPrsFutureAcceptance&) = delete;
  InPrsFutureAcceptance& operator=(const InPrsFutureAcceptance&) = delete;
};

// ============================================================================

DECLARE_COMPONENT( InPrsFutureAcceptance )

// ============================================================================
