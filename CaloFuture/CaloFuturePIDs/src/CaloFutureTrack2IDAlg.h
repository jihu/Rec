#ifndef CALOFUTUREPIDS_CALOFUTURETRACK2IDALG_H
#define CALOFUTUREPIDS_CALOFUTURETRACK2IDALG_H 1

// Include files
#include "CaloFutureTrackAlg.h"
#include "CaloFutureInterfaces/ICaloFutureTrackIdEval.h"
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "Event/Track.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/Counters.h"
#include "Relations/Relation1D.h"
#include "ToVector.h"

// ============================================================================
/** @class CaloFutureTrack2IDAlg CaloFutureTrack2IDAlg.h
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-15
 */

using Table  = LHCb::Relation1D<LHCb::Track,float>;
using Filter = LHCb::Relation1D<LHCb::Track,bool>;

class CaloFutureTrack2IDAlg : public Gaudi::Functional::Transformer<Table(const LHCb::Tracks&,const Filter&),Gaudi::Functional::Traits::BaseClass_t<CaloFutureTrackAlg> > {
  static_assert(std::is_base_of<LHCb::CaloFuture2Track::ITrAccTable,Filter>::value,"Filter must inherit from ITrAccTable");
  public:
    CaloFutureTrack2IDAlg(const std::string& name, ISvcLocator* pSvc);
    Table operator()(const LHCb::Tracks&,const Filter&) const override;

  private:
    // tool to be used for evaluation
    ToolHandle<ICaloFutureTrackIdEval> m_tool{this, "Tool", "<NOT DEFINED>"};

    mutable Gaudi::Accumulators::StatCounter<> m_nTracks{this, "#total tracks"};
    mutable Gaudi::Accumulators::StatCounter<> m_nLinks {this, "#links in table"};
    mutable Gaudi::Accumulators::StatCounter<double> m_nEnergy{this, "#total energy"};
};

// ============================================================================
#endif  // CALOFUTURETRACK2IDALG_H
// ============================================================================
