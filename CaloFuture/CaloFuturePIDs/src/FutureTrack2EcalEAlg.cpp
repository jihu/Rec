// Include files
#include "CaloFutureTrack2IDAlg.h"

// ============================================================================
/** @class FutureTrack2EcalEAlg FutureTrack2EcalEAlg.cpp
 *  preconfigured instance of class  CaloFutureTrack2IDAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-15
 */
struct FutureTrack2EcalEAlg final : public CaloFutureTrack2IDAlg {
  FutureTrack2EcalEAlg(const std::string& name, ISvcLocator* pSvc)
      : CaloFutureTrack2IDAlg(name, pSvc) {
    using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;
    _setProperty("Output", CaloFutureIdLocation("EcalE", context()));
    _setProperty("Filter", CaloFutureIdLocation("InEcal", context()));
    _setProperty("Tool", "FutureEcalEnergyForTrack/EcalEFuture");
  }
};

// ============================================================================

DECLARE_COMPONENT( FutureTrack2EcalEAlg )

// ============================================================================
