// Include files
#include "InCaloFutureAcceptanceAlg.h"

// ============================================================================
/** @class InSpdFutureAcceptanceAlg InSpdFutureAcceptanceAlg.cpp
 *  the preconfigured instance of InCaloFutureAcceptanceAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-17
 */
// ============================================================================

struct InSpdFutureAcceptanceAlg final : InCaloFutureAcceptanceAlg {
  /// Standard constructor
  InSpdFutureAcceptanceAlg(const std::string& name, ISvcLocator* pSvc)
      : InCaloFutureAcceptanceAlg(name, pSvc) {
    using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;

    Gaudi::Functional::updateHandleLocation(*this, "Output",
                                            CaloFutureIdLocation("InSpd", context()));

    _setProperty("Tool", "InSpdFutureAcceptance/InSpd");
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Long, LHCb::Track::Types::Downstream,
                                     LHCb::Track::Types::Ttrack));
  }
};

// ============================================================================

DECLARE_COMPONENT( InSpdFutureAcceptanceAlg )

// ============================================================================
