// Include files
#include "CaloFutureTrackMatchAlg.h"


// ============================================================================
/** @class FuturePhotonMatchAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-16
 */
// ============================================================================
using TABLE = LHCb::RelationWeighted2D<LHCb::CaloCluster, LHCb::Track, float>;
using CALOFUTURETYPES = LHCb::CaloClusters;

struct FuturePhotonMatchAlg final: CaloFutureTrackMatchAlg<TABLE,CALOFUTURETYPES> {
  static_assert(std::is_base_of<LHCb::CaloFuture2Track::IClusTrTable2D, TABLE>::value,
                "TABLE must inherit from IClusTrTable2D");
  FuturePhotonMatchAlg(const std::string& name, ISvcLocator* pSvc)
      : CaloFutureTrackMatchAlg<TABLE,CALOFUTURETYPES>(name, pSvc) {
    Gaudi::Functional::updateHandleLocation(*this, "Calos",  LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation("Ecal",context()));
    Gaudi::Functional::updateHandleLocation(*this, "Output", LHCb::CaloFutureAlgUtils::CaloFutureIdLocation("ClusterMatch",context()));
    Gaudi::Functional::updateHandleLocation(*this, "Filter", LHCb::CaloFutureAlgUtils::CaloFutureIdLocation("InEcal",context()));

    _setProperty("Tool", "CaloFuturePhotonMatch/FuturePhotonMatch");
    _setProperty("Threshold", "1000");
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Long, LHCb::Track::Types::Downstream,
                                     LHCb::Track::Types::Ttrack));
    _setProperty("TableSize", "5000");
  }

};

// ============================================================================

DECLARE_COMPONENT( FuturePhotonMatchAlg )

