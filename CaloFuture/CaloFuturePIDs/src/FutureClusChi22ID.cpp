// Include files
#include "CaloFutureChi22ID.h"
#include "ToVector.h"

// ============================================================================
using TABLEI = LHCb::RelationWeighted2D<LHCb::CaloCluster, LHCb::Track, float>;
using TABLEO = LHCb::Relation1D<LHCb::Track, float>;

struct FutureClusChi22ID final: public CaloFutureChi22ID<TABLEI,TABLEO> {
  static_assert(
      std::is_base_of<LHCb::CaloFuture2Track::IClusTrTable2D, TABLEI>::value,
      "TABLEI must inherit from IClusTrTable2D");

  FutureClusChi22ID(const std::string& name, ISvcLocator* pSvc)
      : CaloFutureChi22ID<TABLEI,TABLEO>(name, pSvc) {
    using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;
    Gaudi::Functional::updateHandleLocation(*this,  "Input", CaloFutureIdLocation("ClusterMatch", context()));
    Gaudi::Functional::updateHandleLocation(*this, "Output", CaloFutureIdLocation("ClusChi2", context()));
    // @todo it must be in agrement with "Threshold" for PhotonMatchAlg
    _setProperty("CutOff", "1000");  //
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Long, LHCb::Track::Types::Ttrack,
                                     LHCb::Track::Types::Downstream));
  };
};
// ============================================================================

DECLARE_COMPONENT( FutureClusChi22ID )
