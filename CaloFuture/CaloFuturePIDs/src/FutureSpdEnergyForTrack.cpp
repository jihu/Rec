// Include files
#include "CaloFutureEnergyForTrack.h"

// ============================================================================
/** @class FutureSpdEnergyForTrack
 *  The concrete preconfigured insatnce for CaloFutureEnergyForTrack tool
 *  along the track line
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================

class FutureSpdEnergyForTrack final : public CaloFutureEnergyForTrack {
 public:
  FutureSpdEnergyForTrack(const std::string& type, const std::string& name,
                    const IInterface* parent)
      : CaloFutureEnergyForTrack(type, name, parent) {
    _setProperty("DataAddress", LHCb::CaloDigitLocation::Spd);
    _setProperty("Tolerance", "2");  /// 2 * Gaudi::Units::mm
    _setProperty("CalorimeterFuture", DeCalorimeterLocation::Spd);
  };
};

// ============================================================================

DECLARE_COMPONENT( FutureSpdEnergyForTrack )
