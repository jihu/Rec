// Include files
#include "InCaloFutureAcceptance.h"

// ============================================================================
/** @class InHcalFutureAcceptance    
 *  The precofigured instance of InCaloFutureAcceptance Tool
 *  @author Victor EGORYCHEV Victor.Egorychev@cern.ch
 *  @author Vanya  BELYAEV    ibelyaev@phsycis.syr.edu
 *  @date   2006-05-28
 */
// ============================================================================

struct InHcalFutureAcceptance     final : InCaloFutureAcceptance {
  /// standard constructor
  InHcalFutureAcceptance(const std::string& type, const std::string& name,
                   const IInterface* parent)
      : InCaloFutureAcceptance(type, name, parent) {
    _setProperty("CalorimeterFuture", DeCalorimeterLocation::Hcal);
    _setProperty("UseFiducial", "true");
    _setProperty("Tolerance", "10");  /// 10 * Gaudi::Units::mm
  };

  /// C++11 non-copyable idiom
  InHcalFutureAcceptance() = delete;
  InHcalFutureAcceptance(const InHcalFutureAcceptance&) = delete;
  InHcalFutureAcceptance& operator=(const InHcalFutureAcceptance&) = delete;
};

// ============================================================================

DECLARE_COMPONENT( InHcalFutureAcceptance     )

// ============================================================================
