// Include files
#include "CaloFutureChi22ID.h"
#include "ToVector.h"

// ============================================================================
/** @class BremChi22IDFuture BremChi22IDFuture.cpp
 *  The preconfigured instance of class CaloFutureChi22ID
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================
using TABLEI = LHCb::RelationWeighted2D<LHCb::CaloHypo, LHCb::Track, float>;
using TABLEO = LHCb::Relation1D<LHCb::Track, float>;

struct BremChi22IDFuture final : public CaloFutureChi22ID<TABLEI,TABLEO> {
  static_assert(
      std::is_base_of<LHCb::CaloFuture2Track::IHypoTrTable2D, TABLEI>::value,
      "TABLEI must inherit from IHypoTrTable2D");
  BremChi22IDFuture(const std::string& name, ISvcLocator* pSvc)
    : CaloFutureChi22ID<TABLEI,TABLEO>(name, pSvc) {
    using LHCb::CaloFutureAlgUtils::CaloFutureIdLocation;
    Gaudi::Functional::updateHandleLocation(*this,  "Input", CaloFutureIdLocation("BremMatch", context()));
    Gaudi::Functional::updateHandleLocation(*this, "Output", CaloFutureIdLocation("BremChi2", context()));
    // @todo it must be in agrement with "Threshold" for BremMatchAlgFuture
    _setProperty("CutOff", "10000");
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                   LHCb::Track::Types::Velo, LHCb::Track::Types::Long,
                                   LHCb::Track::Types::Upstream));
  };
};

// ============================================================================

DECLARE_COMPONENT( BremChi22IDFuture )
