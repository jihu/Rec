// Include files
#include "InCaloFutureAcceptance.h"

// ============================================================================
/** @class InSpdFutureAcceptance     
 *  The precofigured instance of InCaloFutureAcceptance Tool
 *  @author Victor EGORYCHEV Victor.Egorychev@cern.ch
 *  @author Vanya  BELYAEV    ibelyaev@phsycis.syr.edu
 *  @date   2006-05-28
 */
// ============================================================================

struct InSpdFutureAcceptance final : InCaloFutureAcceptance {
  /// standard constructor
  InSpdFutureAcceptance(const std::string& type, const std::string& name,
                  const IInterface* parent)
      : InCaloFutureAcceptance(type, name, parent) {
    _setProperty("CalorimeterFuture", DeCalorimeterLocation::Spd);
    _setProperty("UseFiducial", "false");
    _setProperty("Tolerance", "1");  /// 1 * Gaudi::Units::mm
  };

  /// C++11 non-copyable idiom
  InSpdFutureAcceptance() = delete;
  InSpdFutureAcceptance(const InSpdFutureAcceptance&) = delete;
  InSpdFutureAcceptance& operator=(const InSpdFutureAcceptance&) = delete;
};

// ============================================================================

DECLARE_COMPONENT( InSpdFutureAcceptance      )

// ============================================================================
