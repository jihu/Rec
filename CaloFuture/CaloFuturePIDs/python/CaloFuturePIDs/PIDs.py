#!/usr/bin/env python
# =============================================================================
# $Id: PIDs.py,v 1.4 2010/03/08 01:31:33 odescham Exp $
# =============================================================================
## The major building blocks of CaloFuturerimeter PID
#  @author Vanya BELYAEV Ivan.Belyaev@nikhe.nl
#  @date 2008-07-17
# =============================================================================
"""
The major building blocks of CaloFuturerimeter PID
"""
# =============================================================================
__author__  = "Vanya BELYAEV Ivan.Belyaev@nikhef.nl"
__version__ = "CVS tag $Name: v5r6 $, version $Revision: 1.4 $"
# =============================================================================
from Gaudi.Configuration  import *
    
from Configurables        import    GaudiSequencer

from CaloKernel.ConfUtils import ( hltContext          ,
                                   getAlgo             ,
                                   setTheProperty      ,
                                   addAlgs
                                   )



# =============================================================================
## define "PhotonID" algorithm
def PhotonID ( context , enableRecoOnDemand, useTracks = True ) :
    """
    define 'PhotonID' algorithm
    """
    
    
    from Configurables import   CaloFuturePhotonIdAlg
    
    ## photonID
    pid = getAlgo ( CaloFuturePhotonIdAlg,
                    "PhotonID", 
                    context,
                    "Rec/Calo/PhotonID",
                    enableRecoOnDemand
                    )
    
    pid.Type = 'PhotonID'
    pid.Tracking = useTracks 
    return pid

# =============================================================================
## define "MergedID" algorithm
def MergedID ( context , enableRecoOnDemand, useTracks = True ) :
    """
    define 'MergedID' algorithm
    """
    
    from Configurables import   CaloFuturePhotonIdAlg
    
    ## photonID
    mid = getAlgo ( CaloFuturePhotonIdAlg,
                    "MergedID", 
                    context,
                    "Rec/Calo/MergedID",
                    enableRecoOnDemand
                    )
    
    mid.Type = 'MergedID'    
    mid.Tracking = useTracks
    return mid

# =============================================================================
## define "MergedID" algorithm
def PhotonFromMergedID ( context , enableRecoOnDemand, useTracks = True ) :
    """
    define 'PhotonFromMergedID' algorithm
    """
    
    from Configurables import   CaloFuturePhotonIdAlg
    
    ## photonID
    pmid = getAlgo ( CaloFuturePhotonIdAlg    ,
                     "PhotonFromMergedID", 
                     context,
                     "Rec/Calo/PhotonFromMergedID",
                     enableRecoOnDemand
                     )
    
    pmid.Type = 'PhotonFromMergedID'    
    pmid.Tracking = useTracks
    return pmid

# =============================================================================
## define "inEcalAcceptance" algorithm
def inEcalAcc ( context , enableRecoOnDemand, trackLocation = "", matchTrTypes=[]) :
    """
    
    define 'inEcalFutureAcceptance' algorithm
    
    """

    from Configurables import  ( InEcalFutureAcceptance        , 
                                 InEcalFutureAcceptanceAlg ) 
    
    ## check if the track is in Ecal acceptance 
    inEcal = getAlgo ( InEcalFutureAcceptanceAlg  ,
                       'InECALFuture'             ,
                       context              ,
                       "Rec/Calo/InAccEcal" ,
                       enableRecoOnDemand   )

    if trackLocation :
        inEcal.Inputs   = trackLocation
 
    if matchTrTypes != [] :
        inEcal.AcceptedType=matchTrTypes
        
    return inEcal

## ============================================================================
## define the minimal track match sequnce for photon recontruction
def trackMatch ( context , enableRecoOnDemand, trackLocation = "" , matchTrTypes=[], fastReco = False , clusterLocation = "" ) :
    """

    Define the minimal track match sequnce for photon reconstruction

    """

    from Configurables import FuturePhotonMatchAlg      
    
    seq = getAlgo ( GaudiSequencer , 'CaloFutureTrackMatch' , context )


    ## perform the actual track <-> cluster match 
    clmatch = getAlgo  (  FuturePhotonMatchAlg          ,
                          'FutureClusterMatch'          ,
                          context                 ,
                          'Rec/Calo/ClusterMatch' ,
                          enableRecoOnDemand      )

    if clusterLocation != '' :
        clmatch.Calos = clusterLocation 
    

#    log.info(" ========= trackLocation = " + trackLocation ) 

    ## check if the track is in Ecal acceptance 
    inEcal = inEcalAcc ( context , enableRecoOnDemand , trackLocation,matchTrTypes) 
    
    seq.Members = [ inEcal , clmatch ] 

    setTheProperty ( seq , 'Context' , context )

    if matchTrTypes != [] :
        clmatch.AcceptedType=matchTrTypes


    if trackLocation :
        clmatch.Tracks = trackLocation
        
    log.debug ( "Configure Cluster Track Match : '%s' for  '%s'" %(seq.name(), context) )


    return seq


# =================================================================================
## define various CaloFuture PIDs evaluation
def caloPIDs ( context , enableRecoOnDemand , list , trackLocation = ''   , matchTrTypes=[] , caloTrTypes=[] , bremTrTypes=[],
               skipNeutrals = False , skipCharged = False , fastPID = False , clusterLocation = '', name = '', noSpdPrs=False) :
    """
    Define various CaloFuture PIDs evaluation
    """


    from Configurables import ( InSpdFutureAcceptance     ,
                                InSpdFutureAcceptanceAlg  ,
                                InPrsFutureAcceptance     ,
                                InPrsFutureAcceptanceAlg  ,
                                InEcalFutureAcceptance    ,
                                InEcalFutureAcceptanceAlg ,
                                InBremFutureAcceptance    ,
                                FutureInBremFutureAcceptanceAlg , 
                                InHcalFutureAcceptance    ,
                                InHcalFutureAcceptanceAlg ) 
    
    from Configurables import ( FutureElectronMatchAlg    ,
                                BremMatchAlgFuture        )    
    
    from Configurables import ( FutureTrack2SpdEAlg       ,
                                FutureTrack2PrsEAlg       ,
                                FutureTrack2EcalEAlg      ,
                                FutureTrack2HcalEAlg      )
    
    
    from Configurables import ( FutureEcalChi22ID         ,
                                BremChi22IDFuture         ,
                                FutureClusChi22ID         ,
                                FuturePrsPIDeAlg          ,
                                FutureEcalPIDeAlg         ,
                                BremPIDeAlgFuture         ,
                                FutureHcalPIDeAlg         ,
                                FutureEcalPIDmuAlg        ,
                                FutureHcalPIDmuAlg        ,
                                CaloFuturePhotonIdAlg
                                )

    ##  SANITY CHECK FOR TRACK TYPES (caloTrTypes must be included  in matchTrTypes)
    defMatchTrTypes = [3,5,6]  # Long / Downstream / TTracks
    if matchTrTypes != [] :
        defMatchTrTypes = matchTrTypes
    for item in caloTrTypes :
        if item not in defMatchTrTypes :
            raise AttributeError, 'TrackTypes for ClusterMatching must include CaloFuturePID TrackTypes'
        
    if matchTrTypes != [] :
        log.info(" ! Will use track types = % s for matching" % matchTrTypes)
    if caloTrTypes != [] :
        log.info(" ! Will use track types = % s for caloPID" % caloTrTypes)
    if bremTrTypes != [] :
        log.info(" ! Will use track types = % s for bremPID" % bremTrTypes)
                 


    ## global PID sequence
    _name = 'CaloFuturePIDs'
    if name != context or context.upper() == 'OFFLINE': _name = _name+name
    seq = getAlgo ( GaudiSequencer, _name, context )
    seq.Members = []

    ## add Charged
    if not skipCharged :
        _name = 'ChargedPIDs'
        if name != context or context.upper() == 'OFFLINE': _name = _name+name

        charged = getAlgo( GaudiSequencer , _name , context )

        # inAcceptance
        inAcc = getAlgo ( GaudiSequencer , 'InCaloFutureAcceptance' , context )

        inECAL = inEcalAcc ( context , enableRecoOnDemand , trackLocation , matchTrTypes )         
        inHCAL = getAlgo ( InHcalFutureAcceptanceAlg  ,
                           'InHCALFuture'             , 
                           context              ,
                           "Rec/Calo/InAccHcal" ,
                           enableRecoOnDemand   )
        inBREM = getAlgo ( FutureInBremFutureAcceptanceAlg  ,
                           'InBREMFuture'             , 
                           context              ,
                           "Rec/Calo/InAccBrem" ,
                           enableRecoOnDemand   )
        inAcc.Members = [ inECAL , inHCAL , inBREM ]

        if not noSpdPrs :
            inSPD = getAlgo ( InSpdFutureAcceptanceAlg    ,
                              'InSPD'               , 
                              context               ,
                              "Rec/Calo/InAccSpd"   ,
                              enableRecoOnDemand    )
            inPRS = getAlgo ( InPrsFutureAcceptanceAlg    ,
                              'InPRS'               , 
                              context               ,
                              "Rec/Calo/InAccPrs"   ,
                              enableRecoOnDemand    )             
            inAcc.Members += [ inSPD  , inPRS  ]
        else :
            log.info('remove Spd/Prs from acceptance')


            

        # matching
        match    = getAlgo ( GaudiSequencer  ,
                             'CaloFutureMatch'     ,
                             context         )
        cluster  = trackMatch ( context , enableRecoOnDemand , trackLocation, matchTrTypes, fastPID,clusterLocation)

        electron = getAlgo ( FutureElectronMatchAlg         ,
                             "ElectronMatchFuture"          ,
                             context                  ,
                             'Rec/Calo/ElectronMatch' ,
                             enableRecoOnDemand       ) 
        brem     = getAlgo ( BremMatchAlgFuture             ,
                             "BremMatchFuture"              ,
                             context                  ,
                             'Rec/Calo/BremMatch'     ,
                             enableRecoOnDemand       )

        match.Members = [cluster  , electron, brem ]

        # energy
        energy = getAlgo ( GaudiSequencer ,
                           'CaloFutureEnergy'   ,
                           context        )
        ecalE  = getAlgo ( FutureTrack2EcalEAlg     ,
                           'EcalEFuture'            , 
                           context            ,
                           'Rec/Calo/EcalE'   ,
                           enableRecoOnDemand )
        hcalE  = getAlgo ( FutureTrack2HcalEAlg     ,
                           'HcalEFuture'            , 
                           context            ,
                           'Rec/Calo/HcalE'   ,
                           enableRecoOnDemand )
   

        energy.Members = [ ecalE , hcalE ]
        if not noSpdPrs :
            spdE   = getAlgo ( FutureTrack2SpdEAlg      ,
                               'SpdEFuture'             , 
                               context            ,
                               'Rec/Calo/SpdE'    ,
                               enableRecoOnDemand )
            prsE   = getAlgo ( FutureTrack2PrsEAlg      ,
                               'PrsEFuture'             , 
                               context            ,
                               'Rec/Calo/PrsE'    ,
                           enableRecoOnDemand )

            energy.Members += [ spdE , prsE ]
        else :
            log.info('remove Spd/Prs from energy estimators')


        # Chi2's
        chi2  = getAlgo ( GaudiSequencer , 'CaloFutureChi2', context        )
        eChi2 = getAlgo ( FutureEcalChi22ID, 'FutureEcalChi22ID' , context, 'Rec/Calo/EcalChi2', enableRecoOnDemand   ) 
        bChi2 = getAlgo ( BremChi22IDFuture, 'BremChi22IDFuture' , context, 'Rec/Calo/BremChi2', enableRecoOnDemand   ) 
        cChi2 = getAlgo ( FutureClusChi22ID, 'FutureClusChi22ID' , context, 'Rec/Calo/ClusChi2', enableRecoOnDemand   )
        chi2.Members = [ eChi2, bChi2 , cChi2 ]


        # DLL
        dlle  = getAlgo ( GaudiSequencer ,
                          'CaloFutureDLLeFuture'     ,
                          context        )
        dllmu  = getAlgo ( GaudiSequencer ,
                          'CaloFutureDLLmuFuture'     ,
                          context        )
        ecale=getAlgo ( FutureEcalPIDeAlg          ,
                        'EcalPIDeFuture'           ,
                        context              ,
                        'Rec/Calo/EcalPIDe'  ,
                        enableRecoOnDemand   )
        breme=getAlgo ( BremPIDeAlgFuture          ,
                        'BremPIDeFuture'           ,
                        context              ,
                        'Rec/Calo/BremPIDe'  ,
                        enableRecoOnDemand   ) 
        hcale=getAlgo ( FutureHcalPIDeAlg          ,
                        'HcalPIDeFuture'           ,
                        context              ,
                        'Rec/Calo/HcalPIDe'  ,
                        enableRecoOnDemand   ) 
        ecalmu=getAlgo ( FutureEcalPIDmuAlg         ,
                         'EcalPIDmuFuture'          ,
                         context              ,
                         'Rec/Calo/EcalPIDmu' ,
                         enableRecoOnDemand   ) 
        hcalmu=getAlgo ( FutureHcalPIDmuAlg         ,
                         'HcalPIDmuFuture'          ,
                         context              ,
                         'Rec/Calo/HcalPIDmu' ,
                         enableRecoOnDemand   )

        
        dllmu.Members = [ ecalmu, hcalmu ]
        dlle.Members  = [ ecale , breme , hcale ]
        if not noSpdPrs :
            prse=getAlgo ( FuturePrsPIDeAlg           ,
                           'PrsPIDe'            ,
                           context              ,
                           'Rec/Calo/PrsPIDe'   ,
                           enableRecoOnDemand   ) 

            dlle.Members  += [ prse ]
        else :
            log.info('remove Spd/Prs from dlle')



        # alternative sequence (per caloPID technique)
        ecalT  = getAlgo ( GaudiSequencer ,'EcalPIDFuture'     , context        )
        ecalT.Members = [ inECAL,  cluster, electron, ecalE, eChi2, cChi2, ecale, ecalmu ]

        hcalT  = getAlgo ( GaudiSequencer ,'HcalPIDFuture'     , context        )
        hcalT.Members = [ inHCAL,  hcalE, hcale, hcalmu ]


        if not noSpdPrs :
            prsT  = getAlgo ( GaudiSequencer ,'PrsPIDFuture'     , context        )
            prsT.Members = [ inPRS,  prsE, prse ]
 
            spdT  = getAlgo ( GaudiSequencer ,'SpdPIDFuture'     , context        )
            spdT.Members = [ inSPD,  spdE ]
 
        bremT  = getAlgo ( GaudiSequencer ,'BremPIDFuture'     , context        )
        bremT.Members = [ inBREM,  brem, bChi2, breme ]


        # === Redefine accepted track types ===
        # matchTrTypes propagated to the relevant modules (inEcalAcc & clusterMatch)
        if caloTrTypes != [] :
            electron.AcceptedType  = caloTrTypes
            cChi2.AcceptedType     = caloTrTypes
            eChi2.AcceptedType     = caloTrTypes
            ecalE.AcceptedType     = caloTrTypes
            ecale.AcceptedType     = caloTrTypes
            ecalmu.AcceptedType    = caloTrTypes
            inHCAL.AcceptedType    = caloTrTypes        
            hcalE.AcceptedType     = caloTrTypes
            hcale.AcceptedType     = caloTrTypes
            hcalmu.AcceptedType    = caloTrTypes
            if not noSpdPrs :
                inSPD.AcceptedType = caloTrTypes
                spdE.AcceptedType  = caloTrTypes
                inPRS.AcceptedType = caloTrTypes          
                prsE.AcceptedType  = caloTrTypes
                prse.AcceptedType  = caloTrTypes
        if bremTrTypes != [] :
            inBREM.AcceptedType = bremTrTypes            
            brem.AcceptedType   = bremTrTypes
            bChi2.AcceptedType  = bremTrTypes
            breme.AcceptedType  = bremTrTypes

        # === Override CaloFutureAlgUtils default track location ===
        if  trackLocation :
            electron.Tracks = trackLocation
            brem.Tracks     = trackLocation
            inBREM.Inputs   = trackLocation
            inHCAL.Inputs   = trackLocation
            eChi2.Tracks    = trackLocation
            bChi2.Tracks    = trackLocation
            cChi2.Tracks    = trackLocation
            ecalE.Inputs    = trackLocation
            hcalE.Inputs    = trackLocation            
            if not noSpdPrs :
                inPRS.Inputs    = trackLocation
                inSPD.Inputs    = trackLocation
                spdE.Inputs     = trackLocation
                prsE.Inputs     = trackLocation

        charged.Members = []
        # updatXe global sequence with charged
        if 'InAcceptance' in list : charged.Members += [ inAcc  ]
        if 'Match' in list : charged.Members        += [ match  ]
        if 'Energy' in list : charged.Members       += [ energy ]
        if 'Chi2' in list : charged.Members         += [ chi2   ]
        if 'DLL' in list or 'DLLe' in list : charged.Members          += [ dlle   ]
        if 'DLL' in list or 'DLLmu' in list : charged.Members         += [ dllmu  ]

        # alternative full sequence per technique
        if 'EcalPID' in list :  charged.Members        += [ ecalT  ]
        if 'BremPID' in list :  charged.Members        += [ bremT  ]
        if 'HcalPID' in list :  charged.Members        += [ hcalT  ]
        if 'PrsPID' in list and not noSpdPrs :  charged.Members         += [ prsT   ]
        if 'SpdPID' in list and not noSpdPrs :  charged.Members         += [ spdT   ]
        if charged.Members : addAlgs( seq , charged )
        

    ## Add Neutrals (! No neutralID so far for the noSpdPrs configuration )
    if not skipNeutrals  and not noSpdPrs:
        _name = 'NeutralPIDs'
        if name != context or context.upper() == 'OFFLINE': _name = _name+name
        neutrals = getAlgo( GaudiSequencer , _name , context )
        photonID = PhotonID( context , enableRecoOnDemand )
        mergedID = MergedID( context , enableRecoOnDemand )
        photonFromMergedID = PhotonFromMergedID( context , enableRecoOnDemand )
        # update global sequence with neutrals
        neutrals.Members = []
        if 'PhotonID' in list or 'NeutralPID' in list            : neutrals.Members += [ photonID  ]
        if 'MergedID' in list  or 'NeutralPID' in list           : neutrals.Members += [ mergedID  ]
        if 'PhotonFromMergedID' in list  or 'NeutralPID' in list : neutrals.Members += [ photonFromMergedID ]
        if neutrals.Members : addAlgs( seq , neutrals )



    setTheProperty ( seq , 'Context' , context )
    
    log.debug ( 'Configure CaloFuture PIDs  Reco : %s    for context : %s' %( seq.name() , context) )
        
    return seq



def referencePIDs( dataType='' ) :

    """
    Define various reference Histograms on THS
    """
    hsvc = HistogramSvc ( 'HistogramDataSvc' )
    inputs = hsvc.Input 
     
    # photon PDF default
    pfound  = False   
    for line in inputs : 
        if 0 == line.find ( 'CaloFutureNeutralPIDs') : pfound = True 

    if pfound : 
        log.info ("CaloFuturePIDsConf: LUN 'CaloFutureNeutralPIDs' has been defined already")  
    else: 
        hsvc.Input += [ "CaloFutureNeutralPIDs DATAFILE='$PARAMFILESROOT/data/PhotonPdf.root' TYP='ROOT'" ] 


    # charged PDF default 
    found  = False   
    for line in inputs : 
        if 0 == line.find ( 'CaloFuturePIDs') : found = True 
        
    if found : 
        log.info ("CaloFuturePIDsConf: LUN 'CaloFuturePIDs' has been defined already") 
    elif 'DC06' == dataType : 
        hsvc.Input += [ "CaloFuturePIDs DATAFILE='$PARAMFILESROOT/data/CaloPIDs_DC06_v2.root' TYP='ROOT'" ]
    else: 
        hsvc.Input += [ "CaloFuturePIDs DATAFILE='$PARAMFILESROOT/data/CaloPIDs_DC09_v1.root' TYP='ROOT'" ] 

    return

    
# =============================================================================
if '__main__' == __name__ :
    print __doc__
    
