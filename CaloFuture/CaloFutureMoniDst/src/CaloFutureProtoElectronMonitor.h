#ifndef CALOFUTUREPROTOELECTRONMONITOR_H
#define CALOFUTUREPROTOELECTRONMONITOR_H 1

// Includes
#include "GaudiAlg/Consumer.h"
#include "Event/ProtoParticle.h"
#include "CaloFutureUtils/ICaloFutureElectron.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "CaloFutureMoniAlg.h"

namespace {
  using Input = LHCb::ProtoParticles;
}

/** @class CaloFutureProtoElectronMonitor CaloFutureProtoElectronMonitor.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2009-12-11
 */
class CaloFutureProtoElectronMonitor final
: public Gaudi::Functional::Consumer<void(const Input&),
    Gaudi::Functional::Traits::BaseClass_t<CaloFutureMoniAlg>>
{
public:
  /// Standard constructor
  CaloFutureProtoElectronMonitor( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;
  void operator()(const Input&) const override;

  /// C++11 non-copyable idiom
  CaloFutureProtoElectronMonitor( const CaloFutureProtoElectronMonitor& ) = delete;
  CaloFutureProtoElectronMonitor &operator=( const CaloFutureProtoElectronMonitor& ) = delete;

private:
  ToolHandle<ICaloFutureElectron>      m_caloElectron { "CaloFutureElectron", this };
  ToolHandle<ITrackExtrapolator> m_extrapolator { "TrackRungeKuttaExtrapolator/Extrapolator", this };

  bool valid_track(const LHCb::ProtoParticle* proto) const;

  Gaudi::Property<float> m_eOpMin
    {this, "HistoEoPMin", 0.};

  Gaudi::Property<float> m_eOpMax
    {this, "HistoEoPMax", 3.};

  Gaudi::Property<int> m_eOpBin
    {this, "HistoEoPBin", 100};

  Gaudi::Property<float> m_prsCut
    {this, "PrsCut", 50.* Gaudi::Units::MeV};

  Gaudi::Property<bool> m_pairing
    {this, "ElectronPairing", false};

  Gaudi::Property<std::vector<int>> m_tracks
    {this, "TrackTypes", 
    {LHCb::Track::Types::Long, LHCb::Track::Types::Downstream}};

};
#endif // CALOFUTUREPROTOELECTRONMONITOR_H
