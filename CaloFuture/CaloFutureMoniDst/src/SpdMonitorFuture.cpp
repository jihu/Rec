// Includes
#include "GaudiAlg/Consumer.h"
#include "Event/CaloDigit.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureMoniAlg.h"

// using namespace LHCb;
// using namespace Gaudi::Units;

//==============================================================================
/** @file
 *
 *  Implementation file for class SpdMonitorFuture
 *
 *  SPD Monitoring
 *
 *  @author Albert Puig apuignav@cern.ch
 *  @date   2007-15-07
 */
//==============================================================================

using Input = LHCb::CaloDigits;

class SpdMonitorFuture final
: public Gaudi::Functional::Consumer<void(const Input&),
    Gaudi::Functional::Traits::BaseClass_t<CaloFutureMoniAlg>>
{
public:
  StatusCode initialize() override;
  StatusCode finalize() override;
  void operator()(const Input&) const override;

  SpdMonitorFuture( const std::string& name, ISvcLocator* pSvc );

private:
  DeCalorimeter *m_detSpd = nullptr;
  CaloVector<int> m_neighN;
  mutable unsigned int m_nEvents = 0;
};

//==============================================================================

DECLARE_COMPONENT( SpdMonitorFuture )

//==============================================================================

SpdMonitorFuture::SpdMonitorFuture( const std::string &name, ISvcLocator *pSvcLocator )
: Consumer( name, pSvcLocator, {
    KeyValue{ "Input", LHCb::CaloDigitLocation::Spd },
}){
  m_split=true;  // Area splitting is the default !
}

//==============================================================================

StatusCode SpdMonitorFuture::initialize() {
  StatusCode sc = Consumer::initialize();
  if (sc.isFailure()) return sc;
  // Loads the detectors
  m_detSpd = getDet<DeCalorimeter>( DeCalorimeterLocation::Spd );
  // Histograms
  bookCaloFuture2D( "1", "Spd occupancy", "Spd" );
  bookCaloFuture2D( "2", "Spd neighbor occupancy", "Spd" );

  // Initialize neighbor matrix
  for(unsigned int cellIt = 0; cellIt != m_detSpd->numberOfCells(); cellIt++){
    const auto& cell  = m_detSpd->cellIdByIndex(cellIt);
    const auto& neigh = m_detSpd->zsupNeighborCells(cell);
    m_neighN.addEntry(neigh.size(), cell);
  }
  return StatusCode::SUCCESS;
}

//==============================================================================

StatusCode SpdMonitorFuture::finalize() {
  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug() << "==> Finalize" << endmsg;
  info() << "Number of Events Analyzed : " << m_nEvents << endmsg;
  return Consumer::finalize() ;
}

//==============================================================================

void SpdMonitorFuture::operator()(const Input& digitsSpd) const {

  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug() << "==> Execute " << endmsg;

  // Fill histos
  for(const auto& digit: digitsSpd){
    if (digit==nullptr) continue;
    const auto cell = digit->cellID();
    // histo1
    if(doHisto("1")) fillCaloFuture2D( "1", cell, 1.0 );
    // histo2
    if( doHisto("2") ){
      const auto neighs = m_detSpd->zsupNeighborCells(cell);
      for(const auto neighCell: neighs){
        const auto value = (float) 1./float(m_neighN[cell]);
        fillCaloFuture2D( "2", neighCell , value );
      }
    }
  }
  m_nEvents++;
  return;
}
