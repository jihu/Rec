#ifndef CALOFUTUREELECTRONNTP_H 
#define CALOFUTUREELECTRONNTP_H 1

// Include files
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/IEventTimeDecoder.h"
#include "Event/ODIN.h" 
#include "Event/ProtoParticle.h"
#include "Event/RecVertex.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/ICaloFutureHypo2CaloFuture.h"
#include "CaloFutureInterfaces/IFutureCounterLevel.h"
#include "CaloFutureUtils/ICaloFutureElectron.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

// List of Consumers dependencies
namespace {
  using ODIN = LHCb::ODIN;
  using Protos = LHCb::ProtoParticles;
  using Vertices = LHCb::RecVertices;
}

//==============================================================================

/** @class CaloFutureElectronNtp CaloFutureElectronNtp.h
 *  
 *
 *  @author Olivier Deschamps
 *  @date   2009-12-11
 */
class CaloFutureElectronNtp final
: public Gaudi::Functional::Consumer<void(const ODIN&, const Protos&, const Vertices&),
    Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>>
{
public: 
  CaloFutureElectronNtp( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;
  void operator()(const ODIN&, const Protos&, const Vertices&) const override;

  /// C++11 non-copyable idiom
  CaloFutureElectronNtp() = delete;
  CaloFutureElectronNtp( const CaloFutureElectronNtp& ) = delete;
  CaloFutureElectronNtp &operator=( const CaloFutureElectronNtp& ) = delete;

private:
  DeCalorimeter* m_calo = nullptr;
  std::string m_vertLoc;

  ToolHandle<IFutureCounterLevel>      m_counterStat  { "FutureCounterLevel" };
  ToolHandle<ICaloFutureElectron>      m_caloElectron { "CaloFutureElectron"                            , this };
  ToolHandle<IEventTimeDecoder>  m_odin         { "OdinTimeDecoder/OdinDecoder"             , this };
  ToolHandle<ICaloFutureHypo2CaloFuture>     m_toSpd        { "CaloFutureHypo2CaloFuture/CaloFutureHypo2Spd"              , this };
  ToolHandle<ICaloFutureHypo2CaloFuture>     m_toPrs        { "CaloFutureHypo2CaloFuture/CaloFutureHypo2Prs"              , this };
  ToolHandle<ITrackExtrapolator> m_extrapolator { "TrackRungeKuttaExtrapolator/Extrapolator", this };

  Gaudi::Property<std::pair<double, double>> m_e   { this, "EFilter"  , { 0.  , 99999999}};
  Gaudi::Property<std::pair<double, double>> m_et  { this, "EtFilter" , { 200., 999999.}};
  Gaudi::Property<std::pair<double, double>> m_prs { this, "PrsFilter", { 50. , 9999999.}};
  Gaudi::Property<std::pair<double, double>> m_eop { this, "EoPFilter", { 0.  , 2.5}};
  
  Gaudi::Property<bool> m_pairing {this, "ElectronPairing", true};
  Gaudi::Property<bool> m_histo   {this, "Histo", true};
  Gaudi::Property<bool> m_tuple   {this, "Tuple", true};
  Gaudi::Property<bool> m_trend   {this, "Trend", false};
  Gaudi::Property<bool> m_usePV3D {this, "UsePV3D", false};
  Gaudi::Property<bool> m_splitFEBs{this, "splitFEBs", false};
  Gaudi::Property<bool> m_splitE  {this, "splitE", false};

  Gaudi::Property<std::vector<int>> m_tracks 
    {this, "TrackTypes", {LHCb::Track::Types::Long, LHCb::Track::Types::Downstream}};

  bool set_and_validate( const LHCb::ProtoParticle* proto, bool count=false ) const;
  double invar_mass_squared( const LHCb::Track* t1, const LHCb::Track* t2 ) const;
  void fillH(double eOp,Gaudi::LorentzVector t, LHCb::CaloCellID id,std::string hat="") const;

};

#endif // CALOFUTUREELECTRONNTP_H
