// $Id: Track2CaloFuture.h,v 1.1.1.1 2008-05-08 09:09:02 cattanem Exp $
#ifndef TRACK2CALOFUTURE_H
#define TRACK2CALOFUTURE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
//from LHCb
#include "CaloFutureInterfaces/ITrack2CaloFuture.h"
#include "Event/Track.h"

// Forward declarations
struct ITrackExtrapolator;
namespace LHCb
{
}

/** @class Track2CaloFuture Track2CaloFuture.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2006-11-30
 */
class Track2CaloFuture : public GaudiTool, virtual public ITrack2CaloFuture {
public:
  /// Standard constructor
  Track2CaloFuture( const std::string& type,
               const std::string& name,
               const IInterface* parent);

  StatusCode initialize() override;

  bool match(const  LHCb::Track* track,
                     std::string det = DeCalorimeterLocation::Ecal,
                     CaloPlane::Plane plane = CaloPlane::ShowerMax,
                     double delta = 0.,
                     const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion()
                     ) override;

  LHCb::State caloState() override {return m_state;};
  LHCb::CaloCellID caloCellID() override {return m_cell;};
  bool isValid() override {return m_valid;};

  // Closest State
  LHCb::State closestState(LHCb::CaloHypo*    hypo   ,const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion()) override;
  LHCb::State closestState(LHCb::CaloCluster* cluster,const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion()) override;
  LHCb::State closestState(LHCb::CaloPosition calopos,const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion()) override;
  LHCb::State closestState(LHCb::CaloCellID   cellID ,const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion()) override;
  const LHCb::Track* track() override {return m_track;};



protected:
  LHCb::State closestState(double x, double y,const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion());
  LHCb::State caloState(CaloPlane::Plane plane= CaloPlane::ShowerMax ,
                        double delta =0 ,
                        const LHCb::Tr::PID pid = LHCb::Tr::PID::Pion() );
  bool setting (const  LHCb::Track* track);
  bool                    m_status = false;
  const LHCb::Track*      m_track = nullptr;
  LHCb::State             m_state;
  DeCalorimeter*          m_calo = nullptr;
  std::string             m_det;
private:
  ITrackExtrapolator*  m_extrapolator = nullptr;
  Gaudi::Property<std::string> m_extrapolatorType {this, "ExtrapolatorType", "TrackRungeKuttaExtrapolator"};
  Gaudi::Property<float> m_tolerance {this, "Tolerance", 0.01};
  LHCb::CaloCellID        m_cell;
  bool                    m_valid = false;
};
#endif // TRACK2CALOFUTURE_H
