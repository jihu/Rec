#ifndef FUTURECOUNTERLEVEL_H 
#define FUTURECOUNTERLEVEL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "CaloFutureInterfaces/IFutureCounterLevel.h"            // Interface


/** @class FutureCounterLevel FutureCounterLevel.h
 *  
 *
 *  @author Olivier Deschamps
 *  @date   2016-08-13
 */
class FutureCounterLevel final : public extends<GaudiTool, IFutureCounterLevel> {
public: 
  /// Standard constructor
  FutureCounterLevel( const std::string& type, 
                const std::string& name,
                const IInterface* parent);

  bool isQuiet()      const override {return m_isQuiet;     };
  bool isVerbose()    const override {return m_isVerbose;   };
  bool isLevel(int l) const override {return m_clevel >= l; };
  int  level()        const override {return m_clevel     ; };
    
private:

  Gaudi::Property<int> m_clevel {this, "SetLevel", 1, "quiet mode is the default"};
  bool m_isQuiet = true;
  bool m_isVerbose = false;
};
#endif // FUTURECOUNTERLEVEL_H
