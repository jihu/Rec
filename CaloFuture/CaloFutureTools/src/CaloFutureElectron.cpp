// Include files 

// LHCb
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "Event/Particle.h"

// local
#include "CaloFutureElectron.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloFutureElectron
//
// 2006-11-30 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( CaloFutureElectron )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloFutureElectron::CaloFutureElectron( const std::string& type,
                            const std::string& name,
                            const IInterface* parent )
: base_class ( type, name , parent )
{
  declareInterface<ICaloFutureElectron>(this);
}

//=============================================================================
bool CaloFutureElectron::set(const LHCb::Particle* particle, std::string det,CaloPlane::Plane plane , double delta){
  m_status = setting(particle) && caloSetting();
  if( m_status )return Part2CaloFuture::match(particle , det, plane, delta );
  return m_status;
}
bool CaloFutureElectron::set(const LHCb::ProtoParticle* proto, std::string det,CaloPlane::Plane plane , double delta){
  m_status = setting(proto) && caloSetting();
  if( m_status )return Part2CaloFuture::match(proto , det, plane, delta );
  return m_status;
}
//=============================================================================
LHCb::CaloHypo* CaloFutureElectron::electron(){
  return m_electron;
}
//=============================================================================
LHCb::CaloHypo* CaloFutureElectron::bremstrahlung(){
  return m_bremstrahlung;
}
//=============================================================================
LHCb::CaloMomentum CaloFutureElectron::bremCaloFutureMomentum(){
  if(!m_status || NULL == m_bremstrahlung )return LHCb::CaloMomentum();
  Gaudi::XYZPoint point;
  Gaudi::SymMatrix3x3 matrix;
  m_track->position(point,matrix);  
  LHCb::CaloMomentum bremPhoton( m_bremstrahlung ,point, matrix);
  return bremPhoton;
}

//=============================================================================
double CaloFutureElectron::ecalE(){
  if( !m_status )return 0.;
  return (double) m_electron->e() ;
}
//=============================================================================
double CaloFutureElectron::eOverP(){
  if( !m_status )return 0.;
  return (double) m_electron->e()/m_track->p();
}
//=============================================================================
LHCb::State CaloFutureElectron::closestState(std::string toWhat ){
  LHCb::State state; // empty state
  if( !m_status ) return state;
  // get hypo position
  double x = 0. ;
  double y = 0. ;
  if( "hypo" == toWhat ){
    x = m_calopos->parameters()(LHCb::CaloPosition::Index::X);
    y = m_calopos->parameters()(LHCb::CaloPosition::Index::Y);
  }else if("cluster" == toWhat ){
    x = m_calopos->center()(LHCb::CaloPosition::Index::X);
    y = m_calopos->center()(LHCb::CaloPosition::Index::Y);
  }else{
    return state;
  }
  // default to electron
  LHCb::Tr::PID pid = ( m_particle ?  LHCb::Tr::PID(m_particle->particleID().abspid()) : LHCb::Tr::PID::Electron() );
  return Part2CaloFuture::closestState(x,y, pid);
}
//=============================================================================
double CaloFutureElectron::caloTrajectoryZ(CaloPlane::Plane refPlane , std::string toWhat ){
  LHCb::State theState = closestState( toWhat );
  LHCb::State refState = caloState( refPlane );
  return m_zOffset + theState.z() - refState.z();
}
//=============================================================================
double CaloFutureElectron::caloTrajectoryL(CaloPlane::Plane refPlane , std::string toWhat ){
  LHCb::State theState = closestState( toWhat );
  LHCb::State refState = caloState( refPlane );
  Gaudi::XYZVector depth = theState.position()-refState.position();
  ROOT::Math::Plane3D plane = m_calo->plane( refPlane );
  double dist = plane.Distance( theState.position() );//signed distance to refPlane
  return depth.R() * dist/fabs(dist);
}

//=============================================================================
bool CaloFutureElectron::caloSetting(){
  // CaloFuture setting
  m_electron = NULL;
  m_bremstrahlung = NULL;
  m_calopos = NULL;

  SmartRefVector<LHCb::CaloHypo> hypos = m_proto->calo();
  if(0 == hypos.size())return false;

  for(SmartRefVector<LHCb::CaloHypo>::const_iterator ihypo =  hypos.begin(); ihypo != hypos.end() ; ++ihypo){
    const LHCb::CaloHypo* hypo =  *ihypo;
    if(NULL == hypo)continue;
    if( LHCb::CaloHypo::Hypothesis::EmCharged == hypo->hypothesis() ) m_electron      = (LHCb::CaloHypo*)  hypo;
    if( LHCb::CaloHypo::Hypothesis::Photon ==   hypo->hypothesis() )  m_bremstrahlung = (LHCb::CaloHypo*) hypo;
  }
  if( NULL == m_electron )return false; // Electron hypo is mandatory - brem. not
  m_calopos = m_electron->position();
  if( NULL == m_calopos) return false;
  return true;  
}

