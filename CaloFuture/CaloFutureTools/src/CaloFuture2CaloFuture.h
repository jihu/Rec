#ifndef CALOFUTURE2CALOFUTURE_H
#define CALOFUTURE2CALOFUTURE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "CaloFutureInterfaces/ICaloFuture2CaloFuture.h"            // Interface
#include "CaloDet/DeCalorimeter.h"
#include  "CaloFutureInterfaces/ICaloFutureGetterTool.h"
/** @class CaloFuture2CaloFuture CaloFuture2CaloFuture.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2007-05-29
 */

class CaloFuture2CaloFuture : public extends<GaudiTool, ICaloFuture2CaloFuture> {
public:
  /// Standard constructor
  CaloFuture2CaloFuture( const std::string& type,
             const std::string& name,
             const IInterface* parent);

  StatusCode initialize() override;
  // setting
  void setCalos ( const std::string& fromCaloFuture ,
                  const std::string& toCaloFuture   ) override;
  // CaloCellIDs
  const std::vector<LHCb::CaloCellID>&
  cellIDs ( const LHCb::CaloCluster& fromCluster ,
            const std::string&       toCaloFuture      ) override;
  const std::vector<LHCb::CaloCellID>&
  cellIDs ( const LHCb::CaloCellID&  fromId     ,
            const std::string&       toCaloFuture, bool init=true) override;
  const std::vector<LHCb::CaloCellID>& cellIDs() override {return m_cells;};
  // Digits
  const std::vector<LHCb::CaloDigit*>& digits
  ( const LHCb::CaloCellID& fromId     ,
    const std::string&      toCaloFuture     ) override;
  const std::vector<LHCb::CaloDigit*>& digits
  ( const LHCb::CaloCluster& fromCluster ,
    const std::string&       toCaloFuture    ) override;
  const std::vector<LHCb::CaloDigit*>& digits() override {return m_digits;};
  // Energy
  double energy ( const LHCb::CaloCellID&  fromId ,
                  const std::string&       toCaloFuture ) override;
  double energy ( const LHCb::CaloCluster& fromCluster ,
                  const std::string&       toCaloFuture ) override;
  double energy () override {return m_energy;};
  // multiplicity
  int multiplicity( const LHCb::CaloCellID&  fromId ,
                    const std::string&       toCaloFuture ) override;
  int multiplicity( const LHCb::CaloCluster& fromCluster ,
                    const std::string&       toCaloFuture ) override;
  int multiplicity() override {return m_count;};
  // Additional
  bool isLocalMax ( const LHCb::CaloDigit& digit) override;

protected:
  //
  void reset();
  const std::vector<LHCb::CaloCellID>& addCell
  ( const LHCb::CaloCellID& id, const std::string& toCaloFuture);
  // CaloFuture Maps
  std::map<std::string,DeCalorimeter*> m_det;
  std::map<std::string,std::string> m_loc;
  std::map<std::string,double>m_refSize;
  std::map<std::string,Gaudi::Plane3D> m_plane;
  //
  Gaudi::Property<std::string> m_fromCaloFuture {this, "FromCaloFuture", "??"};
  Gaudi::Property<std::string> m_toCaloFuture {this, "ToCaloFuture", "??"};
  std::vector<LHCb::CaloCellID> m_cells;
  std::vector<LHCb::CaloDigit*>  m_digits;
  double m_energy = 0.;
  int m_count = 0;
  DeCalorimeter* m_fromDet = nullptr;
  DeCalorimeter* m_toDet = nullptr;
  double m_fromSize = 0.;
  double m_toSize = 0.;
  std::string m_toLoc;
  Gaudi::Plane3D m_toPlane;
  LHCb::CaloDigits* m_digs = nullptr;
  ICaloFutureGetterTool* m_getter = nullptr;
  bool m_ok = true;
private:
  Gaudi::Property<bool> m_geo {this, "IdealGeometry", true};
  Gaudi::Property<std::string> m_getterName {this, "GetterName", "CaloFutureGetter"};

};
#endif // CALOFUTURE2CALOFUTURE_H
