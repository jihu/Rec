#ifndef CALOFUTUREHYPOESTIMATOR_H
#define CALOFUTUREHYPOESTIMATOR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/Incident.h"
#include "Event/CaloDataFunctor.h"
#include "CaloFutureUtils/ClusterFunctors.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "Relations/Relation2D.h"
#include "Relations/IRelationWeighted.h"
#include "Relations/IRelationWeighted2D.h"
#include "Event/Track.h"
#include "CaloFutureInterfaces/ICaloFutureHypoEstimator.h"            // Interface
#include "CaloFutureInterfaces/IFutureCounterLevel.h"
#include "CaloFutureInterfaces/IFutureGammaPi0SeparationTool.h"
#include "CaloFutureInterfaces/IFutureNeutralIDTool.h"
#include "CaloFutureUtils/ICaloFutureElectron.h"
#include "CaloFutureInterfaces/ICaloFutureRelationsGetter.h"
#include "CaloDet/DeCalorimeter.h"

/** @class CaloFutureHypoEstimator CaloFutureHypoEstimator.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2010-08-18
 */

class CaloFutureHypoEstimator : public GaudiTool, virtual public ICaloFutureHypoEstimator, virtual public IIncidentListener {
public:
  /// Standard constructor
  CaloFutureHypoEstimator( const std::string& type,
                     const std::string& name,
                     const IInterface* parent);

  StatusCode initialize() override;
  StatusCode finalize() override;

  double data(const LHCb::CaloCluster* cluster ,CaloFutureDataType::DataType type, double def = CaloFutureDataType::Default) override;
  double data(const LHCb::CaloHypo* hypo ,CaloFutureDataType::DataType type, double def = CaloFutureDataType::Default) override;

  void handle(const Incident&  ) override {
    if( UNLIKELY( msgLevel(MSG::DEBUG) ) )
      debug() << "IIncident Svc reset" << endmsg;
    clean();
  }
  ICaloFutureHypo2CaloFuture* hypo2CaloFuture() override {return m_toCaloFuture;};
  const LHCb::Track* toTrack(CaloFutureMatchType::MatchType match) override {
    caloMatchType::iterator it = m_track.find( match );
    if( it == m_track.end() )return NULL;
    return (*it).second;
  }
  const LHCb::CaloCluster* toCluster(CaloFutureClusterType::ClusterType clus=CaloFutureClusterType::SplitOrMain) override {
    caloClusterType::iterator it = m_clusters.find( clus );
    if( it == m_clusters.end() )return NULL;
    return (*it).second;
  }


  StatusCode  _setProperty(const std::string& p,const std::string& v) override {return  setProperty(p,v);};
  bool status() override {return m_status;}


private:
  IFutureCounterLevel* counterStat = nullptr;
  bool estimator(const LHCb::CaloCluster* cluster,const LHCb::CaloHypo* fromHypo=NULL);
  bool estimator(const LHCb::CaloHypo* hypo);
  void clean();
  
  Gaudi::Property<bool> m_extrapol {this, "Extrapolation", true};
  Gaudi::Property<bool> m_seed {this, "AddSeed", false};
  Gaudi::Property<bool> m_neig {this, "AddNeighbors", false};
  
  ICaloFutureHypo2CaloFuture* m_toCaloFuture = nullptr;
  caloDataType m_data;
  caloMatchType m_track;
  caloClusterType m_clusters;
  LHCb::CaloHypo* m_hypo = nullptr;
  LHCb::CaloCluster* m_cluster = nullptr;
  
  Gaudi::Property<std::map<std::string,std::string>> m_pidLoc {this, "NeutralIDLocations"};
  Gaudi::Property<std::string> m_cmLoc {this, "ClusterMatchLocation"};
  Gaudi::Property<std::string> m_emLoc {this, "ElectronMatchLocation"};
  Gaudi::Property<std::string> m_bmLoc {this, "BremMatchLocation"};
  Gaudi::Property<bool> m_skipC {this, "SkipChargedID", false};
  Gaudi::Property<bool> m_skipN {this, "SkipNeutralID", false};
  Gaudi::Property<bool> m_skipCl {this, "SkipClusterMatch", false};

  std::map<std::string,LHCb::CaloFuture2Track::IHypoEvalTable*> m_idTable;
  bool m_status = true;
  ICaloFutureElectron * m_electron = nullptr;
  IFutureGammaPi0SeparationTool* m_GammaPi0 = nullptr;
  IFutureGammaPi0SeparationTool* m_GammaPi0XGB = nullptr;
  IFutureNeutralIDTool* m_neutralID = nullptr;
  ICaloFutureRelationsGetter*    m_tables = nullptr;
  DeCalorimeter* m_ecal = nullptr;
};
#endif // CALOFUTUREHYPOESTIMATOR_H
