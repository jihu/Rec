#ifndef CHECKCALOFUTUREHYPOREF_H
#define CHECKCALOFUTUREHYPOREF_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "CaloFutureInterfaces/IFutureCounterLevel.h"

/** @class CheckCaloFutureHypoRef CheckCaloFutureHypoRef.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2012-05-14
 */
class CheckCaloFutureHypoRef : public GaudiAlgorithm {
public:
  /// Standard constructor
  CheckCaloFutureHypoRef( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override;    ///< Algorithm execution

private:
  Gaudi::Property<std::vector<std::string>> m_inputs {this, "CaloHypos"};
  IFutureCounterLevel* counterStat = nullptr;
};
#endif // CHECKCALOFUTUREHYPOREF_H
