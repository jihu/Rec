#ifndef CALOFUTUREELECTRON_H
#define CALOFUTUREELECTRON_H 1

// Include files
#include "Part2CaloFuture.h"

//from LHCb
#include "CaloFutureUtils/ICaloFutureElectron.h"
#include "CaloFutureUtils/CaloMomentum.h"

// Forward declarations
namespace LHCb
{
  class ProtoParticle;
}


/** @class CaloFutureElectron CaloFutureElectron.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2006-11-30
 */
class CaloFutureElectron : public extends<Part2CaloFuture, ICaloFutureElectron> {
public:
  /// Standard constructor
  CaloFutureElectron( const std::string& type,
              const std::string& name,
              const IInterface* parent);

  bool  set(const  LHCb::Particle* particle,
            std::string det = DeCalorimeterLocation::Ecal,
            CaloPlane::Plane plane = CaloPlane::ShowerMax,
            double delta =0 ) override;
  bool  set(const  LHCb::ProtoParticle* proto,
            std::string det = DeCalorimeterLocation::Ecal,
            CaloPlane::Plane plane = CaloPlane::ShowerMax,
            double delta =0 ) override;

  LHCb::CaloHypo*    electron() override;
  LHCb::CaloHypo*    bremstrahlung() override;
  LHCb::CaloMomentum bremCaloFutureMomentum() override;
  double ecalE() override;
  double eOverP() override;
  using ICaloFutureElectron::closestState;
  LHCb::State closestState(std::string toWhat = "hypo") override;
  double caloTrajectoryZ(CaloPlane::Plane refPlane = CaloPlane::ShowerMax ,std::string toWhat = "hypo") override;
  double caloTrajectoryL(CaloPlane::Plane refPlane = CaloPlane::ShowerMax ,std::string toWhat = "hypo") override;



protected:
  bool caloSetting ();
private:
  LHCb::CaloHypo*            m_electron = nullptr;
  LHCb::CaloHypo*            m_bremstrahlung = nullptr;
  const LHCb::CaloPosition*  m_calopos = nullptr;
  Gaudi::Property<float> m_zOffset
    {this, "zOffset", 0, "Should be 0.0 if ShowerMax plane is correctly defined in condDB"};
};
#endif // CALOFUTUREELECTRON_H
