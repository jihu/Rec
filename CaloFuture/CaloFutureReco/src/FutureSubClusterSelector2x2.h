#ifndef CALOFUTURERECO_SUBCLUSTERSELECTOR2x2_H
#define CALOFUTURERECO_SUBCLUSTERSELECTOR2x2_H 1
// Include files
#include "FutureSubClusterSelectorBase.h"
#include "CaloFutureUtils/CellMatrix2x2.h"


class FutureSubClusterSelector2x2: public FutureSubClusterSelectorBase{
public:

  StatusCode initialize() override;
  StatusCode tag        ( LHCb::CaloCluster* cluster ) const override;
  StatusCode untag      ( LHCb::CaloCluster* cluster ) const override;

protected:
  StatusCode choice2x2 (LHCb::CaloCluster* cluster,
                        CellMatrix2x2::SubMatrixType &type,
                        double &energy) const ;

public:
  FutureSubClusterSelector2x2( const std::string& type   ,
                         const std::string& name   ,
                         const IInterface*  parent );

private:
  const DeCalorimeter* m_det = NULL;
  CellMatrix2x2        m_matrix;
};

#endif // SUBCLUSTERSELECTOR2X2_H
