// $Id: CaloFutureGetterInit.h,v 1.1 2009-04-17 11:44:53 odescham Exp $
#ifndef CALOFUTUREGETTERINIT_H 
#define CALOFUTUREGETTERINIT_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
//from LHCb
#include "CaloFutureInterfaces/ICaloFutureGetterTool.h"

/** @class CaloFutureGetterInit CaloFutureGetterInit.h
 *  
 *
 *  @author Olivier Deschamps
 *  @date   2009-04-17
 */
class CaloFutureGetterInit : public GaudiAlgorithm {
public: 
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;    ///< Algorithm finalization

private:
  ICaloFutureGetterTool* m_getter = nullptr;
  Gaudi::Property<std::string> m_name {this, "ToolName", "CaloFutureGetter"};
};
#endif // CALOFUTUREGETTERINIT_H
