#ifndef CALOFUTURERECO_CALOFUTURESELECTORNOT_H
#define CALOFUTURERECO_CALOFUTURESELECTORNOT_H 1
// Include files
// from STL
#include <string>
// from GaudiAlg
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Counters.h"
// From CaloFutureInterfaces
#include "CaloFutureInterfaces/ICaloFutureClusterSelector.h"
/** @class CaloFutureSelectorNOT CaloFutureSelectorNOT.h
 *
 *  Helper concrete tool for selection of calocluster objects
 *  This selector selects the cluster if
 *  none  of its daughter selector select it!
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   27/04/2002
 */
class CaloFutureSelectorNOT :
  public virtual ICaloFutureClusterSelector,
  public GaudiTool
{
public:
  using Names = std::vector<std::string>;
  using Selectors= std::vector<ICaloFutureClusterSelector*>;
  using IncCounter =  Gaudi::Accumulators::Counter<>;

  /** "select"/"preselect" method
   *  @see ICaloFutureClusterSelector
   *  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select
  (const LHCb::CaloCluster* cluster) const override;

  /** "select"/"preselect" method (functor interface)
   *  @see ICaloFutureClusterSelector
   *  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator ()
  ( const LHCb::CaloCluster* cluster ) const  override;

  StatusCode initialize() override;
  StatusCode finalize() override;

  /** StNOTard constructor
   *  @param type   tool type
   *  @param name   tool name
   *  @param parent tool parent
   */
  CaloFutureSelectorNOT
  (const std::string& type,
   const std::string& name,
   const IInterface* parent);

private:
  Gaudi::Property<Names> m_selectorsTypeNames {this, "SelectorTools"};
  Selectors m_selectors;
  mutable IncCounter m_counter{this, "selected clusters"};
};
// ============================================================================
#endif // CALOFUTURERECO_CALOFUTURESELECTORNOT_H
