// Include files 

// local
#include "CaloFutureGetterInit.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloFutureGetterInit
//
// 2009-04-17 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CaloFutureGetterInit )

//=============================================================================
// Initialization
//=============================================================================
StatusCode CaloFutureGetterInit::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( UNLIKELY(msgLevel(MSG::DEBUG) ) ) debug() << "==> Initialize" << endmsg;


  m_getter = tool<ICaloFutureGetterTool>("CaloFutureGetterTool", m_name );

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode CaloFutureGetterInit::execute() {

  if ( UNLIKELY(msgLevel(MSG::DEBUG) ) ) debug() << "==> Execute" << endmsg;

  m_getter->update();

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode CaloFutureGetterInit::finalize() {

  if ( UNLIKELY(msgLevel(MSG::DEBUG) ) ) debug() << "==> Finalize" << endmsg;

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}

//=============================================================================
