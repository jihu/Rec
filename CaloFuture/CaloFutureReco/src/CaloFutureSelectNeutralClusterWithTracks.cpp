// ============================================================================
//CaloFutureUtils
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
// local 
#include "CaloFutureSelectNeutralClusterWithTracks.h"

// ============================================================================
/** @file 
 *
 *  implementation file for class CaloFutureSelectNeutralClusterWithTracks
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 26 Apr 2002 
 */
// ============================================================================

DECLARE_COMPONENT( CaloFutureSelectNeutralClusterWithTracks )

// ============================================================================
/** Standard constructor
 *  @see  GaudiTool
 *  @see   AlgTool
 *  @see  IAlgTool 
 *  @param type tool type (?)
 *  @param name name of the concrete instance 
 *  @param parent pointer to the parent 
 */
// ============================================================================
CaloFutureSelectNeutralClusterWithTracks::CaloFutureSelectNeutralClusterWithTracks
( const std::string& type   , 
  const std::string& name   ,
  const IInterface*  parent )
  : GaudiTool ( type , name , parent )
{
  declareInterface<ICaloFutureClusterSelector> (this) ;
  m_tableLocation = LHCb::CaloFutureAlgUtils::CaloFutureIdLocation( "ClusterMatch", context() );
}

// ============================================================================
/** standard initialization method 
 *  @see GaudiTool
 *  @see  AlgTool
 *  @see IAlgTool
 *  @return status code
 */
// ============================================================================
StatusCode 
CaloFutureSelectNeutralClusterWithTracks::initialize ()
{  
  // initialize the base class 
  StatusCode sc = GaudiTool::initialize () ;
  if( sc.isFailure() )
  {return Error("Could not initialize the base class GaudiTool!",sc); }
  m_tables = tool<ICaloFutureRelationsGetter>("CaloFutureRelationsGetter","CaloFutureRelationsGetter",this);  
  return StatusCode::SUCCESS ;
}

// ============================================================================
/** @brief "select"  method 
 *
 *  Cluster is considered to be "selected" 
 *  if there are no reconstructed tracks with 
 *  chi2 value for 2D-matching under the threshold value 
 *
 *  @see ICaloFutureClusterSelector
 *  @param  cluster pointer to calo cluster object to be selected 
 *  @return true if cluster is selected
 */
// ============================================================================
bool CaloFutureSelectNeutralClusterWithTracks::select     
( const LHCb::CaloCluster* cluster ) const 
{ return (*this) ( cluster ); }
// ============================================================================

// ============================================================================
/** @brief "select"  method (functor interface)
 *
 *  Cluster is considered to be "selected" 
 *  if there are no reconstructed tracks with 
 *  chi2 value for 2D-matching under the threshold value 
 *
 *  @see ICaloFutureClusterSelector
 *  @param  cluster pointer to calo cluster object to be selected 
 *  @return true if cluster is selected
 */
// ============================================================================
bool CaloFutureSelectNeutralClusterWithTracks::operator() 
  ( const LHCb::CaloCluster* cluster   ) const 
{
  // check the cluster 
  if ( 0 == cluster ) { Warning ( "CaloCluster* points to NULL!" ).ignore() ; return false ; }
  
  // locate the table 
  LHCb::CaloFuture2Track::IClusTrTable* table = m_tables->getClusTrTable( m_tableLocation );
  if( NULL == table )return true;

  // get all relations with WEIGHT = 'chi2' under the threshold value 
  const LHCb::CaloFuture2Track::IClusTrTable::Range range = table -> relations ( cluster , m_chi2cut , false ) ;

  bool isSelected = range.empty();
  if (isSelected ) { 
    ++m_counter;
  }
  return isSelected;
}
