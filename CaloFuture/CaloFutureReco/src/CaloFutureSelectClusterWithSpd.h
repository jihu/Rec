// ============================================================================
#ifndef CALOFUTURESELECTCLUSTERWITHSPD_H
#define CALOFUTURESELECTCLUSTERWITHSPD_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiAlg
// ============================================================================
#include "GaudiAlg/GaudiTool.h"
// ============================================================================
// CaloFutureInterfaces
// ============================================================================
#include "CaloFutureInterfaces/ICaloFutureClusterSelector.h"
#include "CaloFutureInterfaces/ICaloFutureHypo2CaloFuture.h"
#include "CaloFutureInterfaces/IFutureCounterLevel.h"
// ============================================================================
/** @class CaloFutureSelectClusterWithSpd
 *  Helper base class for "cluster selectioin with Spd" tools
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date 2009-07-18
 */
// ============================================================================
class CaloFutureSelectClusterWithSpd :
  public virtual ICaloFutureClusterSelector ,
  public          GaudiTool
{
public:
  // ==========================================================================
  /// initialize the tool
  StatusCode initialize() override;
  // ==========================================================================
public:
  // ==========================================================================
  /// number of hits in SPD
  int n_hits ( const LHCb::CaloCluster& cluster ) const ;
  /// number of hits in SPD
  int n_hit  ( const LHCb::CaloCluster& cluster ) const
  { return n_hits ( cluster ) ; }
  // ==========================================================================
  /// standard constructor
  CaloFutureSelectClusterWithSpd
  ( const std::string& type   ,
    const std::string& name   ,
    const IInterface*  parent ) ;
  // ==========================================================================
protected:
  // ==========================================================================
  /// get number of hits
  int cut () const { return m_cut ; }
  /// get the tool
  ICaloFutureHypo2CaloFuture* calo2calo () const { return m_toSpd ; }
  /// get the calorimeter
  const std::string& det() const { return m_det   ; }
  IFutureCounterLevel* counterStat = nullptr;
  // ==========================================================================
private:
  // ==========================================================================
  /// number of hits in spd
  Gaudi::Property<int> m_cut {this, "MinMultiplicity", 0, "number of hits in spd "};
  /// Calo -> Calo tool
  ICaloFutureHypo2CaloFuture* m_toSpd = nullptr;
  /// Calorimeter
  Gaudi::Property<std::string> m_det {this, "Detector", "Ecal"};
  // ==========================================================================
};
#endif // CALOFUTURESELECTCLUSTERWITHSPD_H
// ============================================================================
