#include "GaudiKernel/SystemOfUnits.h"
#include "CaloFutureSelectClusterWithPrs.h"

DECLARE_COMPONENT( CaloFutureSelectClusterWithPrs )

// ============================================================================
CaloFutureSelectClusterWithPrs::CaloFutureSelectClusterWithPrs
( const std::string& type   , 
  const std::string& name   ,
  const IInterface*  parent )
  : GaudiTool ( type , name , parent )
{
  declareInterface<ICaloFutureClusterSelector> ( this ) ;
}
// ============================================================================

StatusCode CaloFutureSelectClusterWithPrs::initialize (){  
  // initialize the base class 
  StatusCode sc = GaudiTool::initialize () ;
  //
  m_toPrs = tool<ICaloFutureHypo2CaloFuture>("CaloFutureHypo2CaloFuture", "CaloFutureHypo2Prs");
  m_toPrs->setCalos(m_det,"Prs");
  counterStat = tool<IFutureCounterLevel>("FutureCounterLevel");
  return sc;
}

// ============================================================================
/** @brief "select"  method 
 *
 *  Cluster is considered to be "selected" if there are Spd/Prs hit in front 
 *
 */
// ============================================================================
bool CaloFutureSelectClusterWithPrs::select( const LHCb::CaloCluster* cluster ) const{ 
return (*this) ( cluster ); 
}
// ============================================================================
bool CaloFutureSelectClusterWithPrs::operator()( const LHCb::CaloCluster* cluster   ) const{
  // check the cluster 
  if ( 0 == cluster ) { Warning ( "CaloCluster* points to NULL!" ).ignore() ; return false ; }
  double ePrs = m_toPrs->energy( *cluster, "Prs");
  int mPrs = m_toPrs->multiplicity();
  if ( UNLIKELY(msgLevel( MSG::DEBUG) ))debug() << "Found " << mPrs << "Prs hits " 
                                      << " for a total energy of " << ePrs <<  endmsg;

  bool sel = (ePrs>m_cut) && (mPrs>m_mult) ;
  if(counterStat->isVerbose())counter("selected clusters") += (int) sel;
  return sel;
}
