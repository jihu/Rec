// ============================================================================
// Include files 
// ============================================================================
// local
// ============================================================================
#include "CaloFutureSelectorOR.h"
// ============================================================================
/** @file 
 * 
 *  Implementation file for class : CaloFutureSelectorOR
 * 
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 27 Apr 2002 
 */
// ============================================================================
DECLARE_COMPONENT( CaloFutureSelectorOR )
// ============================================================================
/*  Standard constructor
 *  @see GaudiTool
 *  @see  AlgTool 
 *  @see IAlgTool 
 *  @param type   tool type (?)
 *  @param name   tool name 
 *  @param parent tool parent   
 */
// ============================================================================
CaloFutureSelectorOR::CaloFutureSelectorOR
( const std::string& type,
  const std::string& name,
  const IInterface* parent )
  : GaudiTool ( type, name , parent ) 
{
  declareInterface<ICaloFutureClusterSelector> (this);
}
// ============================================================================
/*  standard initialization of the tool
 *  @see IAlgTool 
 *  @see AlgTool 
 *  @see GaudiTool 
 *  @return status code 
 */
// ============================================================================
StatusCode CaloFutureSelectorOR::initialize () 
{
  // initialize the base class
  StatusCode sc = GaudiTool::initialize() ;
  if( sc.isFailure() ) 
    { return Error("Could not initialize the base class GaudiTool",sc);}
  // locate selectors 
  std::transform( m_selectorsTypeNames.begin(), m_selectorsTypeNames.end(),
                  std::back_inserter(m_selectors),
                  [&](const std::string& name)
                  { return  tool<ICaloFutureClusterSelector>( name, this ); } );
  ///
  return StatusCode::SUCCESS ;
}
// ============================================================================
/*  standard finalization  of the tool
 *  @see IAlgTool 
 *  @see AlgTool 
 *  @see GaudiTool 
 *  @return status code 
 */
// ============================================================================
StatusCode CaloFutureSelectorOR::finalize   () 
{
  // clear containers 
  m_selectors          .clear() ;
  m_selectorsTypeNames .clear() ;
  // finalize the base class 
  return GaudiTool::finalize () ;
}
// ============================================================================
/*  "select"/"preselect" method 
 *  @see ICaloFutureClusterSelector
 *  @param  cluster pointer to calo cluster object to be selected 
 *  @return true if cluster is selected
 */
// ============================================================================
bool CaloFutureSelectorOR::select     
( const LHCb::CaloCluster* cluster ) const { return (*this) ( cluster ) ; }
// ============================================================================
/** "select"/"preselect" method (functor interface)
 *  @see ICaloFutureClusterSelector
 *  @param  cluster pointer to calo cluster object to be selected 
 *  @return true if cluster is selected
 */
// ============================================================================
bool CaloFutureSelectorOR::operator() ( const LHCb::CaloCluster* cluster ) const
{
  return std::any_of( m_selectors.begin(), m_selectors.end(),
                      [&](typename Selectors::const_reference s)
                      { return (*s)(cluster); } );
}
// ============================================================================
// The END 
// ============================================================================
