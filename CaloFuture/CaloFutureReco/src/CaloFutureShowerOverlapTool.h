#ifndef CALOFUTURESHOWEROVERLAPTOOL_H
#define CALOFUTURESHOWEROVERLAPTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "CaloDet/DeCalorimeter.h"
#include "Event/CaloCluster.h"
#include "CaloFutureCorrectionBase.h"
#include "CaloFutureInterfaces/ICaloFutureHypoTool.h"
#include "CaloFutureInterfaces/ICaloFutureShowerOverlapTool.h"            // Interface
#include "CaloFutureInterfaces/IFutureCounterLevel.h"


/** @class CaloFutureShowerOverlapTool CaloFutureShowerOverlapTool.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2014-06-02
 */
class CaloFutureShowerOverlapTool : public GaudiTool, virtual public ICaloFutureShowerOverlapTool {
public:
  /// Standard constructor
  CaloFutureShowerOverlapTool( const std::string& type,
                         const std::string& name,
                         const IInterface* parent);

  StatusCode initialize() override;

  StatusCode setProfile(std::string) override;
  void process(const LHCb::CaloCluster* c1, const LHCb::CaloCluster* c2,
               int spd=0, int niter=10,bool propagateInitialWeights=false) override;

protected:
  void storeInitialWeights(const LHCb::CaloCluster* cl1,const LHCb::CaloCluster* cl2);
  double getInitialWeight(const LHCb::CaloCellID id);
  double fraction(LHCb::CaloCluster* c, LHCb::CaloDigit* d,int flag);
  void subtract(LHCb::CaloCluster* c1,LHCb::CaloCluster* c2,bool propagateInitialWeights);
  double showerFraction(double d3d,unsigned int area,int spd);
  void evaluate(LHCb::CaloCluster* c,bool hypoCorrection = true);
private:
  int m_a1 = 0;
  int m_a2 = 0;
  int m_s1 = 0;
  int m_s2 = 0;
  Gaudi::Property<std::string> m_detLoc {this, "Detector", DeCalorimeterLocation::Ecal};
  Gaudi::Property<std::string> m_pcond {this, "Profile", "Conditions/Reco/Calo/PhotonShowerProfile"};
  std::string m_type;
  const DeCalorimeter* m_det = nullptr;
  ICaloFutureHypoTool*      m_stool = nullptr;
  ICaloFutureHypoTool*      m_ltool = nullptr;
  CaloFutureCorrectionBase* m_shape = nullptr;
  std::map<const LHCb::CaloCellID,double> m_weights;
  Gaudi::Property<bool> m_verbose {this, "Verbose", false};
  Gaudi::Property<unsigned int> m_minSize {this, "ClusterMinSize", 2};
  IFutureCounterLevel* counterStat = nullptr;
};
#endif // CALOFUTURESHOWEROVERLAPTOOL_H
