// ============================================================================
// Include files
// CaloFutureInterfaces 
#include "CaloFutureInterfaces/ICaloFutureLikelihood.h"
// local
#include "CaloFutureSelector.h"

// ============================================================================
/** @file CaloFutureSelector.cpp 
 *
 *  Implementation file for class : CaloFutureSelector
 * 
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru 
 *  @date 31/03/2002 
 */
// ============================================================================

DECLARE_COMPONENT( CaloFutureSelector )

// ============================================================================
/** Standard constructor
 *  @param    type   tool type (?)
 *  @param    name   tool name 
 *  @param    parent tool parent 
 */
// ============================================================================
CaloFutureSelector::CaloFutureSelector( const std::string&  type   ,
                            const std::string&  name   ,
                            const IInterface*   parent )
  : GaudiTool ( type, name , parent ) 
{  
  // interfaces  
  declareInterface<ICaloFutureClusterSelector> (this);
}

// ============================================================================
/** standard initialization of the tool 
 *  @see IAlgTool 
 *  @see AlgTool 
 *  @see GaudiTool 
 *  @return status code 
 */
// ============================================================================
StatusCode CaloFutureSelector::initialize() 
{
  // initialialize the base class 
  StatusCode sc = GaudiTool::initialize() ;
  if( sc.isFailure() ) 
  { return Error("Could not initialize the base class GaudiTool!",sc);}
  /// locate the tool 
  m_likelihood = m_lhName.empty() ?
    tool<ICaloFutureLikelihood>( m_lhType            , this ) :
    tool<ICaloFutureLikelihood>( m_lhType , m_lhName , this ) ;
  if( 0 == m_likelihood ) { return StatusCode::FAILURE ; }
  //
  return StatusCode::SUCCESS ;
}

// ============================================================================
/** standard finalization  of the tool 
 *  @see IAlgTool 
 *  @see AlgTool 
 *  @see GaudiTool 
 *  @return status code 
 */
// ============================================================================
StatusCode CaloFutureSelector::finalize() 
{
  ///finalize the base class 
  return GaudiTool::finalize() ;
}

// ============================================================================
/** "select"/"preselect" method (functor interface)
 *  @see ICaloFutureClusterSelector 
 *  @param  cluster pointer to calo cluster object to be selected 
 *  @return true if cluster is selected
 */
// ============================================================================
bool CaloFutureSelector::operator() ( const LHCb::CaloCluster* cluster ) const
{
  if( 0 == cluster ) {  return false ; }
  return  m_cut <= (*m_likelihood) (cluster) ;
}

// ============================================================================
/** "select"/"preselect" method 
 *  @see ICaloFutureClusterSelector 
 *  @param  cluster pointer to calo cluster object to be selected 
 *  @return true if cluster is selected
 */
// ============================================================================
bool CaloFutureSelector::select     ( const LHCb::CaloCluster* cluster ) const 
{ return (*this) ( cluster ); }

// ============================================================================
// The End 
// ============================================================================
