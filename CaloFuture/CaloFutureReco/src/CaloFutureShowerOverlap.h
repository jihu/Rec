#ifndef CALOFUTURESHOWEROVERLAP_H 
#define CALOFUTURESHOWEROVERLAP_H 1

// Include files 
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CellID.h"
#include "FutureSubClusterSelectorTool.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/ICaloFutureShowerOverlapTool.h"
#include "Event/CaloCluster.h"

/** @class CaloFutureShowerOverlap CaloFutureShowerOverlap.h
 *  
 *
 *  @author Olivier Deschamps
 *  @date   2014-06-02
 */
class CaloFutureShowerOverlap : public GaudiAlgorithm 
{

public: 

  /// Standard constructor
  CaloFutureShowerOverlap( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:

  Gaudi::Property<int>   m_dMin   {this, "DistanceThreshold", 4};
  Gaudi::Property<float> m_etMin  {this, "MinEtThreshold", 50., "( ET1 > x && ET2 > x)"};
  Gaudi::Property<float> m_etMin2 {this, "MaxEtThreshold", 150., "( ET2 > y || ET2 > y)"};
  Gaudi::Property<int>   m_iter   {this, "Iterations", 5};
  Gaudi::Property<std::string> m_input {this, "InputData", LHCb::CaloClusterLocation::Ecal};
  Gaudi::Property<std::string> m_det   {this, "Detector" , DeCalorimeterLocation::Ecal};
  
  // following properties are inherited by the selector tool when defined :
  Gaudi::Property<std::vector<std::string>> m_taggerP {this, "PositionTags"};
  Gaudi::Property<std::vector<std::string>> m_taggerE {this, "EnergyTags"};
  
  const DeCalorimeter*    m_detector = nullptr ;
  ICaloFutureShowerOverlapTool* m_oTool    = nullptr ;
  FutureSubClusterSelectorTool* m_tagger   = nullptr ;

};

#endif // CALOFUTURESHOWEROVERLAP_H
