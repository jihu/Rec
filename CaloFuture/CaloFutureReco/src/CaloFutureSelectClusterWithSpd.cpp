// ============================================================================
// Include files 
// ============================================================================
// Local
// ============================================================================
#include "CaloFutureSelectClusterWithSpd.h"
// ============================================================================
/** @file
 *  Implementation file for class CaloFutureSelectClusterWithSpd
 *  @date 2009-07-18 
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 */
// ============================================================================
// standard constructor 
// ============================================================================
CaloFutureSelectClusterWithSpd::CaloFutureSelectClusterWithSpd
( const std::string& type   , 
  const std::string& name   ,
  const IInterface*  parent )
  : GaudiTool ( type , name , parent )
{
  declareInterface<ICaloFutureClusterSelector> ( this ) ;
}
// ============================================================================
// initialization 
// ============================================================================
StatusCode CaloFutureSelectClusterWithSpd::initialize ()
{
  // initialize the base class 
  StatusCode sc = GaudiTool::initialize () ;  
  m_toSpd = tool<ICaloFutureHypo2CaloFuture> ( "CaloFutureHypo2CaloFuture", "CaloFutureHypo2Spd" , this );
  m_toSpd->setCalos( m_det,"Spd");
  counterStat = tool<IFutureCounterLevel>("FutureCounterLevel");
  return sc ;
}
// ============================================================================
// the main method 
// ============================================================================
int CaloFutureSelectClusterWithSpd::n_hits( const LHCb::CaloCluster& cluster ) const
{ return m_toSpd->multiplicity ( cluster, "Spd" ) ; }

