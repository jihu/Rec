#ifndef CALOFUTURERECO_SUBCLUSTERSELECTORSwissCross_H
#define CALOFUTURERECO_SUBCLUSTERSELECTORSwissCross_H 1
// Include files
// CaloFutureTools
#include "FutureSubClusterSelectorBase.h"
// CaloFutureUtils
#include "CaloFutureUtils/CellSwissCross.h"


class FutureSubClusterSelectorSwissCross  : public FutureSubClusterSelectorBase{
public:
  StatusCode initialize() override;

  StatusCode tag        ( LHCb::CaloCluster* cluster)const override;
  StatusCode untag      ( LHCb::CaloCluster* cluster ) const override;

  FutureSubClusterSelectorSwissCross( const std::string& type   ,
                         const std::string& name   ,
                         const IInterface*  parent );

private:

  CellSwissCross        m_matrix;

};

#endif
