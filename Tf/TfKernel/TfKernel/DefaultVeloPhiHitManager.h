#ifndef INCLUDE_TF_DEFAULTVELOPHIHITMANAGER_H
#define INCLUDE_TF_DEFAULTVELOPHIHITMANAGER_H 1

#include "TfKernel/DefaultVeloHitManager.h"

namespace Tf {

  static const InterfaceID IID_DefaultVeloPhiHitManager( "Tf::DefaultVeloPhiHitManager", 1, 0 );

  /** @class DefaultVeloPhiHitManager DefaultVeloPhiHitManager.h
   *
   *  Default Hit manager for Velo Phi hits
   *  
   * @author Kurt Rinnert <kurt.rinnert@cern.ch>
   * @date   2007-08-07
   */
  struct DefaultVeloPhiHitManager : DefaultVeloHitManager<DeVeloPhiType,VeloPhiHit,2> {

    /// Retrieve interface ID
    static const InterfaceID& interfaceID() { return IID_DefaultVeloPhiHitManager; }

    /// Standard Constructor
    DefaultVeloPhiHitManager(const std::string& type,
                             const std::string& name,
                             const IInterface* parent);

  };

}
#endif // INCLUDE_TF_DEFAULTVELOPHIHITMANAGER_H

