#ifndef INDEXEDHITCONTAINER1D_H
#define INDEXEDHITCONTAINER1D_H 1

#include <assert.h>
#include <vector>
#include <algorithm>
#include <numeric>
#include <array>

#include "MultiIndexedHitContainer.h"

template<size_t nSub, typename Hit>
using IndexedHitContainer1D = MultiIndexedHitContainer< Hit, nSub >;

#endif
