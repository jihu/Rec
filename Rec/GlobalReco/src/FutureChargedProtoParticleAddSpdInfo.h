
//-----------------------------------------------------------------------------
/** @file FutureChargedProtoParticleAddSpdInfo.h
 *
 * Header file for algorithm FutureChargedProtoParticleAddSpdInfo
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 29/03/2006
 */
//-----------------------------------------------------------------------------

#ifndef GLOBALRECO_FutureChargedProtoParticleAddSpdInfo_H
#define GLOBALRECO_FutureChargedProtoParticleAddSpdInfo_H 1

// from Gaudi
#include "FutureChargedProtoParticleCALOFUTUREBaseAlg.h"

/** @class FutureChargedProtoParticleAddSpdInfo FutureChargedProtoParticleAddSpdInfo.h
 *
 *  Updates the CALO SPD information stored in the ProtoParticles
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date 28/08/2009
 */

class FutureChargedProtoParticleAddSpdInfo final : public FutureChargedProtoParticleCALOFUTUREBaseAlg
{

public:

  /// Standard constructor
  FutureChargedProtoParticleAddSpdInfo( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override;       ///< Algorithm execution

private:

  /// Load the Calo Spd tables
  bool getSpdData();

  /// Add Calo Spd information to the given ProtoParticle
  bool addSpd( LHCb::ProtoParticle * proto ) const;

private:

  std::string m_protoPath; ///< Location of the ProtoParticles in the TES

  std::string m_inSpdPath ;
  std::string m_spdEPath ;

  const LHCb::CaloFuture2Track::ITrAccTable*  m_InSpdTable = nullptr;
  const LHCb::CaloFuture2Track::ITrEvalTable*  m_SpdETable = nullptr;

};

#endif // GLOBALRECO_FutureChargedProtoParticleAddSpdInfo_H
