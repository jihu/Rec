//-----------------------------------------------------------------------------
/** @file FutureChargedProtoParticleCALOFUTUREBaseAlg.cpp
 *
 * Implementation file for algorithm FutureChargedProtoParticleCALOFUTUREBaseAlg
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 28/08/2009
 */
//-----------------------------------------------------------------------------

// local
#include "FutureChargedProtoParticleCALOFUTUREBaseAlg.h"

//-----------------------------------------------------------------------------

//=============================================================================
// Initialization
//=============================================================================
StatusCode FutureChargedProtoParticleCALOFUTUREBaseAlg::initialize()
{
  const StatusCode sc = GaudiAlgorithm::initialize();
  if ( sc.isFailure() ) return sc;

  counterStat = tool<IFutureCounterLevel>("FutureCounterLevel");
  m_estimator = tool<ICaloFutureHypoEstimator>("CaloFutureHypoEstimator","CaloFutureHypoEstimator",this);

  return sc;
}
