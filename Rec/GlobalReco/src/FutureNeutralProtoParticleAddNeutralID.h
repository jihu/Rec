#ifndef FUTURENEUTRALPROTOPARTICLEADDNEUTRALID_H
#define FUTURENEUTRALPROTOPARTICLEADDNEUTRALID_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "CaloFutureInterfaces/IFutureGammaPi0SeparationTool.h"
#include "CaloFutureInterfaces/IFutureNeutralIDTool.h"
#include "Event/ProtoParticle.h"

/** @class FutureNeutralProtoParticleAddNeutralID FutureNeutralProtoParticleAddNeutralID.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2014-05-27
 */

class FutureNeutralProtoParticleAddNeutralID final : public GaudiAlgorithm
{

 public:

  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode execute() override;    ///< Algorithm execution

 private:

  Gaudi::Property<std::string> m_input  { this,   "Input"          , LHCb::ProtoParticleLocation::Neutrals };
  Gaudi::Property<bool>   m_isNotE      { this,  "UpdateIsNotE"    , true    };
  Gaudi::Property<bool>   m_isNotH      { this,  "UpdateIsNotH"    , true    };
  Gaudi::Property<bool>   m_isPhoton    { this,  "UpdateIsPhoton"  , true    };
  Gaudi::Property<double> m_isNotE_Pt   { this,  "MinPtIsNotE"     , 75.0    };
  Gaudi::Property<double> m_isNotH_Pt   { this,  "MinPtIsNotH"     , 75.0    };
  Gaudi::Property<double> m_isPhoton_Pt { this,  "MinPtIsPhoton"   ,  2000.0 };
  ToolHandle<IFutureGammaPi0SeparationTool> m_gammaPi0  { this, "FutureGammaPi0SeparationTool" , "FutureGammaPi0SeparationTool" };
  ToolHandle<IFutureNeutralIDTool>          m_neutralID { this, "FutureNeutralIDTool" , "FutureNeutralIDTool" };

};


#endif // FUTURENEUTRALPROTOPARTICLEADDNEUTRALID_H
