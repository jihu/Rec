
//-----------------------------------------------------------------------------
/** @file ChargedProtoParticleAddBremInfo.h
 *
 * Header file for algorithm ChargedProtoParticleAddBremInfo
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 29/03/2006
 */
//-----------------------------------------------------------------------------

#ifndef GLOBALRECO_FutureChargedProtoParticleAddBremInfo_H
#define GLOBALRECO_FutureChargedProtoParticleAddBremInfo_H 1

// from Gaudi
#include "FutureChargedProtoParticleCALOFUTUREBaseAlg.h"

/** @class FutureChargedProtoParticleAddBremInfo FutureChargedProtoParticleAddBremInfo.h
 *
 *  Updates the CALO 'BREM' information stored in the ProtoParticles
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date 28/08/2009
 */

class FutureChargedProtoParticleAddBremInfo final : public FutureChargedProtoParticleCALOFUTUREBaseAlg
{

public:

  /// Standard constructor
  FutureChargedProtoParticleAddBremInfo( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode execute() override; ///< Algorithm execution

private:

  /// Load the Calo Brem tables
  bool getBremData();

  /// Add Calo Brem information to the given ProtoParticle
  bool addBrem( LHCb::ProtoParticle * proto ) const;

private:

  std::string m_protoPath; ///< Location of the ProtoParticles in the TES

  std::string m_inBremPath ;
  std::string m_bremMatchPath ;
  std::string m_bremChi2Path ;
  std::string m_bremPIDePath ;

  const LHCb::CaloFuture2Track::ITrAccTable*  m_InBremTable = nullptr;
  const LHCb::CaloFuture2Track::IHypoTrTable2D* m_bremTrTable = nullptr;
  const LHCb::CaloFuture2Track::ITrEvalTable*  m_BremChi2Table = nullptr;
  const LHCb::CaloFuture2Track::ITrEvalTable* m_dlleBremTable = nullptr;

};

#endif // GLOBALRECO_FutureChargedProtoParticleAddBremInfo_H
