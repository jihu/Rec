
//-----------------------------------------------------------------------------
/** @file FutureChargedProtoParticleAddSpdInfo.cpp
 *
 * Implementation file for algorithm FutureChargedProtoParticleAddSpdInfo
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 28/08/2009
 */
//-----------------------------------------------------------------------------

// local
#include "FutureChargedProtoParticleAddSpdInfo.h"

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FutureChargedProtoParticleAddSpdInfo )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
FutureChargedProtoParticleAddSpdInfo::
FutureChargedProtoParticleAddSpdInfo( const std::string& name,
                                ISvcLocator* pSvcLocator)
  : FutureChargedProtoParticleCALOFUTUREBaseAlg ( name , pSvcLocator )
{
  // default locations from context()

  using namespace LHCb::CaloFuture2Track;
  using namespace LHCb::CaloFutureIdLocation;
  using namespace LHCb::CaloFutureAlgUtils;

  m_protoPath   = LHCb::ProtoParticleLocation::Charged ;
  m_inSpdPath   = PathFromContext( context() , InSpd  );
  m_spdEPath    = PathFromContext( context() , SpdE   );

  declareProperty("ProtoParticleLocation"      , m_protoPath       );
  declareProperty("InputInSpdLocation"         , m_inSpdPath       );
  declareProperty("InputSpdELocation"          , m_spdEPath        );
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode FutureChargedProtoParticleAddSpdInfo::execute()
{
  // Load the Brem data
  const bool sc = getSpdData();
  if ( !sc )
  {
    return Warning( "No CALO SPD data -> ProtoParticles will not be changed.",
                    StatusCode::SUCCESS );
  }

  // ProtoParticle container
  auto * protos = getIfExists<LHCb::ProtoParticles>(m_protoPath);
  if ( !protos )
  {
    if ( msgLevel(MSG::DEBUG) ) debug() << "No existing ProtoParticle container at "
                                        <<  m_protoPath<<" thus do nothing."<<endmsg;
    return StatusCode::SUCCESS;
  }

  // Loop over proto particles and update SPD info
  for ( auto * proto : *protos ) { addSpd(proto); }

  if ( counterStat->isQuiet() )
    counter("SpdPIDs("+context()+") ==> " + m_protoPath )+= protos->size();

  return StatusCode::SUCCESS;
}

//=============================================================================

//=============================================================================
// Loads the Calo Spd data
//=============================================================================
bool FutureChargedProtoParticleAddSpdInfo::getSpdData()
{
  const bool sc1 = loadCaloTable(m_InSpdTable , m_inSpdPath);
  const bool sc2 = loadCaloTable(m_SpdETable  , m_spdEPath );
  const bool sc  = sc1 && sc2;
  if ( sc && msgLevel(MSG::DEBUG) ) debug() << "SPD PID SUCCESSFULLY LOADED" << endmsg;
  return sc;
}

//=============================================================================
// Add Calo Spd info to the protoparticle
//=============================================================================
bool FutureChargedProtoParticleAddSpdInfo::addSpd( LHCb::ProtoParticle * proto ) const
{
  // clear SPD info
  proto->removeCaloSpdInfo();

  bool hasSpdPID = false;

  const auto aRange = m_InSpdTable -> relations ( proto->track() ) ;
  if ( !aRange.empty() )
  {
    hasSpdPID = aRange.front().to();
    if ( hasSpdPID )
    {
      if ( msgLevel(MSG::VERBOSE) )verbose() << " -> The track is in Spd acceptance"  << endmsg;
      proto->addInfo(LHCb::ProtoParticle::additionalInfo::InAccSpd , true );

      // Get the PrsE (intermediate) estimator
      const auto vRange = m_SpdETable -> relations ( proto->track() ) ;
      if ( !vRange.empty() ) { proto->addInfo(LHCb::ProtoParticle::additionalInfo::CaloSpdE,  vRange.front().to() ); }

      if ( msgLevel(MSG::VERBOSE) )
        verbose() << " -> Spd PID : "
                  << " SpdE       =" <<  proto->info(LHCb::ProtoParticle::additionalInfo::CaloSpdE, -999.)
                  << endmsg;

    }
    else
    {
      if ( msgLevel(MSG::VERBOSE) ) verbose() << " -> The track is NOT in Spd acceptance"  << endmsg;
    }
  }
  else
  {
    if ( msgLevel(MSG::VERBOSE) ) verbose() << " -> No entry for that track in the Spd acceptance table"  << endmsg;
  }

  return hasSpdPID;
}
