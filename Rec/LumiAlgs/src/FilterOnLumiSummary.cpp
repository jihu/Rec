// Include files

// event model
#include "Event/HltLumiSummary.h"
#include "Event/LumiCounters.h"
#include "Event/LumiMethods.h"

// local
#include "FilterOnLumiSummary.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FilterOnLumiSummary
//
// 2010-01-29 : Jaap Panman
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FilterOnLumiSummary )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
FilterOnLumiSummary::FilterOnLumiSummary( const std::string& nam,
                                          ISvcLocator* pSvcLocator)
: FilterPredicate ( nam , pSvcLocator,
                    { KeyValue{ "InputDataContainer", LHCb::HltLumiSummaryLocation::Default } } )
{
  m_CounterName.declareUpdateHandler( [=](Property&) {
      m_Counter = LHCb::LumiCounters::counterKeyToType(m_CounterName);
      if ( m_Counter == LHCb::LumiCounters::counterKey::Unknown ) {
        throw GaudiException( "LumiCounter not found with name: " + m_CounterName,
                              name(),
                              StatusCode::SUCCESS );
      }
  } );
  m_CounterName.useUpdateHandler();

  m_ValueName.declareUpdateHandler( [=](Property&) {
      m_Value = LHCb::LumiMethods::methodKeyToType(m_ValueName);
      if ( m_Value == LHCb::LumiMethods::methodKey::Unknown ) {
        throw GaudiException( "LumiMethod not found with name: " + m_ValueName,
                              name(),
                              StatusCode::SUCCESS );
      }
  } );
  m_ValueName.useUpdateHandler();
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode FilterOnLumiSummary::initialize()
{
  StatusCode sc = FilterPredicate::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  if ( msgLevel(MSG::DEBUG) ) {
    debug() << "==> Initialize" << endmsg;
    debug() << "CounterName:       " << m_CounterName.value() << " "
            << "ValueName:         " << m_ValueName.value()   << endmsg;
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
bool FilterOnLumiSummary::operator()(const LHCb::HltLumiSummary& summary) const
{
  // look at the specified value
  return summary.info(m_Counter,-1) == m_Value;
}
