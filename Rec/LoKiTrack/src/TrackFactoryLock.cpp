// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Report.h"
#include "LoKi/TrackFactoryLock.h"
#include "LoKi/TrackEngineActor.h"
// ============================================================================
/** @file
 *  Implementation file for class LoKi::Hybrid::TrackFactoryLock
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @date 2007-06-10
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================
// constructor
// ============================================================================
LoKi::Hybrid::TrackFactoryLock::TrackFactoryLock
( const LoKi::ITrackFunctorAntiFactory* tool    ,
  const LoKi::Context&                  context ) 
  : m_tool ( tool )
{
  LoKi::Hybrid::TrackEngineActor& actor =
    LoKi::Hybrid::TrackEngineActor::instance() ;
  // connect the tool to the actor
  StatusCode sc = actor.connect ( m_tool.getObject () , context ) ;
  if ( sc.isFailure () )
  { LoKi::Report::Error
      ( "LoKi::Hybrid::TrackFactoryLock: error from connectTool", sc ) .ignore() ; }
}
// ============================================================================
// destructor
// ============================================================================
LoKi::Hybrid::TrackFactoryLock::~TrackFactoryLock()
{
  LoKi::Hybrid::TrackEngineActor& actor =
    LoKi::Hybrid::TrackEngineActor::instance() ;
  // connect the tool to the actor
  StatusCode sc = actor.disconnect ( m_tool.getObject () ) ;
  if ( sc.isFailure () )
  { LoKi::Report::Error
      ( "LoKi::Hybrid::TrackFactoryLock: error from releaseTool", sc ) .ignore() ; }
}
// ============================================================================
// The END
// ============================================================================
