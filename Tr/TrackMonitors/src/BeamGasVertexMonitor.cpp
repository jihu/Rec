#include <functional>
#include <Event/ODIN.h>
#include <Event/Track.h>
#include <Event/HltDecReports.h>
#include <Event/HltSelReports.h>
#include <Event/MCHeader.h>
#include <Event/GenHeader.h>
#include "BeamGasVertexMonitor.h"

//-----------------------------------------------------------------------------
// Implementation file for class : BeamGasVertexMonitor
//
// 2015-05-08 Rosen Matev
//-----------------------------------------------------------------------------
namespace {

using Tuple = Tuples::Tuple;


bool fillOdin(const Tuple& tuple, const LHCb::ODIN* odin) {
  bool test = true;
  test &= tuple->column("run", odin->runNumber());
  test &= tuple->column("gps", odin->gpsTime());
  test &= tuple->column("evt", odin->eventNumber());
  test &= tuple->column("orbit", odin->orbitNumber());
  test &= tuple->column("step", odin->calibrationStep());
  test &= tuple->column("bcid", odin->bunchId());
  test &= tuple->column("bx", odin->bunchCrossingType());
  return test;
}

bool fillVertex(const Tuple& tuple, const std::string& prefix, const BeamGasVertex& vertex, bool all) {
  bool test = true;
  test &= tuple->column(prefix + "nTr", vertex.ntr);
  if (all) {
    test &= tuple->column(prefix + "nTrBw", vertex.ntrbw);
    test &= tuple->column(prefix + "chi2", vertex.chi2);
    test &= tuple->column(prefix + "ndof", vertex.ndof);
  }
  test &= tuple->column(prefix + "x", vertex.x);
  test &= tuple->column(prefix + "y", vertex.y);
  test &= tuple->column(prefix + "z", vertex.z);
  return test;
}


BeamGasCandidates getRecoCandidates(const LHCb::RecVertices& splitVertices) {
  auto Assert = [](bool b, auto& label) {
      if (!b) throw GaudiException("BeamGasVertexMonitor::getRecoCandidates", label, StatusCode::FAILURE);
  };

  BeamGasCandidates candidates;
  Assert(splitVertices.size() % 3 == 0, "Split vertices container length must be a multiple of 3.");
  for (auto it = splitVertices.begin(); it != splitVertices.end(); ) {
    LHCb::RecVertex* pv = *(it++);
    LHCb::RecVertex* sv1 = *(it++);
    LHCb::RecVertex* sv2 = *(it++);
    Assert(sv1->info(1000003, -1) == pv->position().z(), "Split vertex 1 does not match PV");
    Assert(sv2->info(1000003, -1) == pv->position().z(), "Split vertex 2 does not match PV");
    candidates.emplace_back(*pv, *sv1, *sv2);
  }
  return candidates;
}

BeamGasCandidates getMCCandidates(const SmartRefVector<LHCb::MCVertex>& mcVertices) {
  BeamGasCandidates bgc; bgc.reserve(mcVertices.size());
  std::transform( mcVertices.begin(), mcVertices.end(),
                  std::back_inserter(bgc), [](const auto& i) -> BeamGasCandidate { return { *i }; } );
  return bgc;
}

template <typename Iterator, typename Projection, typename Threshold>
std::pair<Iterator,Threshold> min_below_threshold( Iterator begin, Iterator end, Projection proj, Threshold mn ) {
    auto r = make_pair(end,std::move(mn));
    for ( auto i = begin; i != end; ++i ) {
        auto val = std::invoke( proj, *i );
        if ( val < r.second ) {
            r.second = std::move(val);
            r.first = i;
        }
    }
    return r;
}

}

DECLARE_COMPONENT( BeamGasVertexMonitor )

StatusCode BeamGasVertexMonitor::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiTupleAlg

  if (m_svLocations.size() != m_svPrefixes.size()) {
    error() << "Number of prefixes does not match number of split vertex locations!" << endmsg;
    return StatusCode::FAILURE;
  }

  m_velo = getDet<DeVelo>(DeVeloLocation::Default);

  return StatusCode::SUCCESS;
}

StatusCode BeamGasVertexMonitor::execute() {
  LHCb::ODIN* odin = get<LHCb::ODIN>(LHCb::ODINLocation::Default);

  BeamGasCandidatesSet set;
  bool withHlt = false;
  bool withMC = false;

  for (unsigned int i = 0; i < m_svLocations.size(); ++i) {
    const auto* splitVertices = get<LHCb::RecVertices>(m_svLocations[i]);
    set.emplace_back("rec" + m_svPrefixes[i], getRecoCandidates(*splitVertices));
  }

  if (!m_selNames.empty()) {
    const auto* selReports = getIfExists<LHCb::HltSelReports>(m_reportLocation);
    if (selReports) {
      set.emplace_back("hlt", getHltCandidates(*selReports));
      withHlt = true;
    }
  }

  const auto* mcHeader = getIfExists<LHCb::MCHeader>(LHCb::MCHeaderLocation::Default);
  const LHCb::GenHeader* genHeader = nullptr;
  if (mcHeader) {
    genHeader = get<LHCb::GenHeader>(LHCb::GenHeaderLocation::Default);
    set.emplace_back("mc", getMCCandidates(mcHeader->primaryVertices()));
    withMC = true;
  }

  if (set.empty()) return StatusCode::SUCCESS;

  Tuple tuple = nTuple("ntuple");

  const BeamGasCandidate defcand = BeamGasCandidate();

  unsigned int totCandidates = set[0].second.size();
  unsigned int nCandidate = 0;
  const auto& name = set[0].first;
  for (auto& cand : set[0].second) {
    bool test = true;
    test &= fillOdin(tuple, odin);
    test &= tuple->column("totCandidates", totCandidates);
    test &= tuple->column("nCandidate", nCandidate);

    // The main primary/split vertices
    if (name != "mc") {
      test &= fillVertex(tuple, name + "_", cand.pv, true);
      test &= fillVertex(tuple, name + "_1_", cand.sv1, false);
      test &= fillVertex(tuple, name + "_2_", cand.sv2, false);
    } else {
      test &= fillVertex(tuple, name + "_", cand.pv, false);
    }

    // Matched primary/split vertices
    for (auto it = ++set.begin(); it != set.end(); ++it) {
      const auto& mname = it->first;
      const auto& mcandidates = it->second;

      auto mpair = min_below_threshold( mcandidates.begin(), mcandidates.end(),
                                [z = cand.pv.z](const BeamGasCandidate& c) {
                                    return std::abs(c.pv.z-z);
                                },
                                m_maxMatchDeltaZ.value() );
      const BeamGasCandidate* matched = ( mpair.first!=mcandidates.end() ? &*mpair.first : &defcand );

      // For **rec** PVs (other than the main), don't write the PV and
      // the matching distance (which is always zero).
      if (mname.compare(0, 3, "rec") != 0) {
        test &= fillVertex(tuple, mname + "_", matched->pv, mname != "mc");
        test &= tuple->column(mname + "_match_dist", mpair.second);
      }

      if (mname != "mc") {
        test &= fillVertex(tuple, mname + "_1_", matched->sv1, false);
        test &= fillVertex(tuple, mname + "_2_", matched->sv2, false);
      }

      if (withHlt && !matched->selections.empty())
        cand.selections = matched->selections;
    }

    // HLT selections
    if (withHlt) {
        test = std::accumulate( begin(m_selNames), end(m_selNames) ,
                                test,
                                [&](bool t, const auto& selName) {
                    t &= tuple->column(selName, cand.selections.count(selName) > 0);
                    return t;
        } );
    }

    if (withMC) {
      tuple->column("mc_evtype", genHeader->evType());
      //tuple->column("mc_ncollisions", genHeader->numOfCollisions());

      std::vector<double> processTypes;
      for (const auto& collision : genHeader->collisions()) {
        processTypes.push_back(collision->processType());
      }
      tuple->farray("mc_processtypes", processTypes, "mc_ncollisions", 10);
    }

    test &= tuple->write();

    if (!test) {
      error() << "Failed filling/writing tuple" << endmsg;
      return StatusCode::FAILURE;
    }

    nCandidate++;
  }

  return StatusCode::SUCCESS;
}


BeamGasCandidates BeamGasVertexMonitor::getHltCandidates(const LHCb::HltSelReports& selReports) {
  BeamGasCandidates candidates;
  for (const auto& name : m_selNames) {
    const auto& sr = selReports.selReport(name);
    if (sr) {
      const SmartRefVector<LHCb::HltObjectSummary>& sub = sr->substructure();
      Assert(sub.size() % 3 == 0, "HLT vertex selection length must be a multiple of 3.");
      for (unsigned int i = 0; i < sub.size(); i += 3) {
        BeamGasVertex pv(*(sub[i]), m_velo);

        auto it = std::find_if(candidates.begin(), candidates.end(),
                          [&pv](const BeamGasCandidate& cand) {
                            return cand.pv.z == pv.z;
                          });
        if (it == candidates.end()) {
          candidates.emplace_back(pv,
            BeamGasVertex(*(sub[i+1]), m_velo),
            BeamGasVertex(*(sub[i+2]), m_velo));
          it = candidates.end() - 1;
        }
        it->selections.insert(name);
      }
    }
  }
  return candidates;
}


BeamGasVertex::BeamGasVertex(const LHCb::RecVertex& recvtx)
  : x(recvtx.position().x()),
    y(recvtx.position().y()),
    z(recvtx.position().z()),
    chi2(recvtx.chi2()),
    ndof(recvtx.nDoF()),
    ntr(recvtx.tracks().size())
{
  const auto& trks = recvtx.tracks();
  this->ntrbw = std::count_if( begin(trks), end(trks),
                               [](const LHCb::Track* t) {
                                   return t->checkFlag(LHCb::Track::Flags::Backward);
                               } );
}

BeamGasVertex::BeamGasVertex(const LHCb::MCVertex& mcvtx)
  : x(mcvtx.position().x()),
    y(mcvtx.position().y()),
    z(mcvtx.position().z()),
    ntr(mcvtx.products().size())
{
}

BeamGasVertex::BeamGasVertex(const LHCb::HltObjectSummary& recvtx_hos, const DeVelo* velo) {
  if (recvtx_hos.summarizedObjectCLID() != 10030)
    throw std::runtime_error("Unexpected CLID of summarized object (expect 10030).");

  const auto& substr = recvtx_hos.substructureFlattened();
  int nTr = substr.size();
  int nTrBw = 0;
  for (const auto& track_hos : substr) {
    bool bwd = false;
    if (track_hos->summarizedObjectCLID() != 10010)
      throw std::runtime_error("Unexpected CLID of summarized object (expect 10010).");
    auto ni = track_hos->numericalInfoFlattened();
    // 2016-11-02 RM: I don't understand 10#Track.flags. It seems to be lossy and
    //                maybe buggy. Let's revert to a manual check for backwardness.
    // auto flags = ni.find("10#Track.flags");
    // if (flags != ni.end()) {
    //   bwd = (unsigned int)((*flags).second) & (LHCb::Track::Flags::Backward << 18);  // LHCb::Track::flagBits (= 18) is protected
    // } else {
      bwd = isBackwardTrack(*track_hos, velo);
    // }
    nTrBw += bwd;
  }
  const auto& ni = recvtx_hos.numericalInfoFlattened();

  this->x = ni.at("0#RecVertex.position.x");
  this->y = ni.at("1#RecVertex.position.y");
  this->z = ni.at("2#RecVertex.position.z");
  this->chi2 = ni.at("3#RecVertex.chi2");
  this->ndof = 2*nTr - 3;
  this->ntr = nTr;
  this->ntrbw = nTrBw;
}

bool BeamGasVertex::isBackwardTrack(const LHCb::HltObjectSummary& track_hos, const DeVelo* velo) {
  const auto& ids =  track_hos.lhcbIDsFlattened();
  auto zmax = std::accumulate( begin(ids), end(ids), -1e9,
                               [velo](double zm, const LHCb::LHCbID id) {
    return id.isVelo() ? std::max( zm, velo->sensor(id.veloID())->z() )
                       : zm;
  });
  if (zmax == -1e9)
    throw std::runtime_error("No VELO LHCbIDs (hits) for given track!");
  double z0 = track_hos.numericalInfoFlattened().at("0#Track.firstState.z");
  return z0 > zmax;
}
