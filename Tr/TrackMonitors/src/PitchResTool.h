#ifndef INCLUDE_PITCHRESTOOL_H
#define INCLUDE_PITCHRESTOOL_H 1

#include "GaudiAlg/GaudiTool.h"

#include "TrackInterfaces/IPitchResTool.h"
#include "Event/Track.h"

#include <string>
#include <vector>
#include <utility>

// forward declaratrions
struct ITrackExtrapolator;

/** @class PitchResTool PitchResTool.h
 * tool to calculate pitch residuals for all suitable pairs of OT hits
 * on a track
 *
 * @author Manuel Tobias Schiller <schiller@physi.uni-heidelberg.de>
 * @date   2008-06-05
 */
class PitchResTool : public extends<GaudiTool,IPitchResTool> {

public:

  /// Standard Constructor
  using extends::extends;

  /// return a vector of pairs of (layer, pitch residual) from the OT hits on the track
  std::vector<std::pair<LHCb::OTChannelID, double> > calcPitchResiduals(const LHCb::Track* track) const override;

private:
  PublicToolHandle<ITrackExtrapolator> m_extrapolator{ this,"TrackExtrapolatorName", "TrackMasterExtrapolator" };

};
#endif // INCLUDE_PITCHRESTOOL_H

