################################################################################
# Package: TrackTools
################################################################################
gaudi_subdir(TrackTools v6r2)

gaudi_depends_on_subdirs(Calo/CaloUtils
                         Det/MuonDet
                         Det/OTDet
                         Det/STDet
                         Det/VPDet
                         Det/VeloDet
                         Event/DigiEvent
                         Event/TrackEvent
                         GaudiAlg
                         Kernel/LHCbKernel
                         Kernel/PartProp
                         Kernel/Relations
                         Muon/MuonInterfaces
                         OT/OTDAQ
                         ST/STKernel
                         Tf/PatKernel
                         Tf/TsaKernel
                         Tr/TrackFitEvent
                         Tr/TrackInterfaces
                         Tr/TrackKernel
                         Tr/TrackVectorFit)

find_package(GSL)
find_package(ROOT)

find_package(Boost)
find_package(CLHEP)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS})

gaudi_add_module(TrackTools
                 src/*.cpp
                 INCLUDE_DIRS GSL ROOT Event/DigiEvent OT/OTDAQ Tf/PatKernel Tr/TrackInterfaces
                 LINK_LIBRARIES GSL ROOT CaloUtils MuonDetLib OTDetLib STDetLib VPDetLib VeloDetLib TrackEvent GaudiAlgLib LHCbKernel PartPropLib RelationsLib MuonInterfacesLib STKernelLib TsaKernel TrackFitEvent TrackKernel)
