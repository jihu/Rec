################################################################################
# Package: ParameterizedKalman
################################################################################
gaudi_subdir(ParameterizedKalman v0r0)

gaudi_depends_on_subdirs(Det/FTDet
                         Det/VPDet
                         Event/LinkerEvent
                         Event/MCEvent
                         Associators/AssociatorsBase
                         Kernel/Relations
                         Event/RecEvent
                         Event/TrackEvent
                         GaudiAlg
                         GaudiKernel
                         Kernel/MCInterfaces
                         Kernel/LHCbMath
                         Tr/TrackKernel
                         Tr/TrackFitEvent
                         Tr/TrackInterfaces)

find_package(Boost)
find_package(ROOT COMPONENTS RIO Tree)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

find_package(GSL)

gaudi_add_module(ParameterizedKalman
                 src/*.cpp
                 INCLUDE_DIRS GSL ROOT Tr/TrackInterfaces Kernel/MCInterfaces Associators/AssociatorsBase
                 LINK_LIBRARIES GSL ROOT TrackEvent GaudiAlgLib GaudiKernel LHCbMathLib TrackFitEvent TrackKernel LinkerEvent MCEvent VPDetLib FTDetLib RelationsLib)
