
/** @class TrackListRefiner TrackListRefiner.h
 *
 *  Make a subselection of a track list
 *
 *  @author Wouter Hulsbergen
 *  @date   05/01/2010
 */

#ifdef _WIN32
#pragma warning ( disable : 4355 ) // This used in initializer list, needed for ToolHandles
#endif

#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/ToolHandle.h"
#include "TrackInterfaces/ITrackSelector.h"
#include "GaudiKernel/SharedObjectsContainer.h"
#include <string>
#include "Event/Track.h"


class TrackListRefiner: public Gaudi::Functional::Transformer<LHCb::Track::Selection(const LHCb::Tracks&)> {
public:

  TrackListRefiner(const std::string& name, ISvcLocator* pSvcLocator);

  StatusCode initialize() override;
  StatusCode finalize() override;
  LHCb::Track::Selection operator()(const LHCb::Tracks& tracksin) const override;

private:
  ToolHandle<ITrackSelector> m_selector{ "", this };
};


DECLARE_COMPONENT( TrackListRefiner )

TrackListRefiner::TrackListRefiner(const std::string& name,
                       ISvcLocator* pSvcLocator):
  Transformer(name, pSvcLocator,
              KeyValue{ "inputLocation",  {} },
              KeyValue{ "outputLocation", {} } )
{
  declareProperty( "Selector", m_selector );
}

StatusCode TrackListRefiner::initialize()
{
  StatusCode sc = Transformer::initialize();
  if (sc.isFailure()) return Error("Failed to initialize");

  // retrieve the selector if it is set
  if ( !m_selector.empty() ) {
    sc = m_selector.retrieve() ;
    if(sc.isFailure())
      error() << "Failed to retrieve selector." << endmsg ;
  }
  return sc ;
}

StatusCode TrackListRefiner::finalize()
{
  if ( !m_selector.empty() ) m_selector.release().ignore() ;
  return Transformer::finalize() ;
}

LHCb::Track::Selection TrackListRefiner::operator()(const LHCb::Tracks& tracksin) const
{
  LHCb::Track::Selection tracksout;
  counter("#seeds")+=tracksin.size();
  //TODO: can we use std::transform -- i.e. is there an 'inserter' for LHCb::Track::Selection?
  for (const auto& trk : tracksin) {
    if ( m_selector.empty() || m_selector->accept( *trk ) ) tracksout.insert( trk ) ;
  }
  counter("#passed") += tracksout.size();
  return tracksout;
}
