#ifndef _UpstreamGhostClassification_H
#define _UpstreamGhostClassification_H

#include "TrackGhostClassificationBase.h"
#include "Event/Track.h"



class UpstreamGhostClassification: public TrackGhostClassificationBase {

public:

  /// constructer
  UpstreamGhostClassification(const std::string& type,
                               const std::string& name,
                               const IInterface* parent);

  /** destructer */
  virtual ~UpstreamGhostClassification();


 private:

 StatusCode specific(LHCbIDs::const_iterator& start,
               LHCbIDs::const_iterator& stop, LHCb::GhostTrackInfo& tinfo) const override;


};



#endif
