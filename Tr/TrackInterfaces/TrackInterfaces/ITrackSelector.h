#ifndef _ITrackSelector_H
#define _ITrackSelector_H

#include "GaudiKernel/IAlgTool.h"
#include "Event/Track.h"


/** @class ITrackSelector
 *
 *  interface for selecting tracks....
 *
 *  @author M.Needham
 *  @date   31/05/2004
 */


struct ITrackSelector: extend_interfaces<IAlgTool> {

  DeclareInterfaceID( ITrackSelector, 1, 0 );

  /// the method
  virtual bool accept(const LHCb::Track& aTrack) const = 0;

};

#endif
