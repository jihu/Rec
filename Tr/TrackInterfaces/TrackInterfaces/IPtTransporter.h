#ifndef TRACKINTERFACES_IPTTRANSPORTER_H
#define TRACKINTERFACES_IPTTRANSPORTER_H 1

// Include files

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

namespace LHCb {
  class State;
}


/** @class IPtTransporter IPtTransporter.h TrackInterfaces/IPtTransporter.h
 *
 *  calculate pt at origin from a given state at T
 *
 *  @author Johannes Albrecht
 *  @date   2008-05-08
 */
struct IPtTransporter : extend_interfaces<IAlgTool> {

  // Return the interface ID
  DeclareInterfaceID(IPtTransporter, 2, 0 );

  virtual double ptAtOrigin(double zref, double xref, double yref,
                            double tx, double ty, double p) const =0;

  virtual double ptAtOrigin(const LHCb::State& stateAtT) const =0;

};
#endif // TRACKINTERFACES_IPTTRANSPORTER_H
