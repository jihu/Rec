#ifndef _ITrackFunctor_h
#define _ITrackFunctor_h

#include "GaudiKernel/IAlgTool.h"
#include "Event/Track.h"


/** @class ITrackFunctor
 *
 *  interface to compute some float given a Track
 */


struct ITrackFunctor: extend_interfaces<IAlgTool> {

  DeclareInterfaceID( ITrackFunctor, 1, 0 );

  virtual float operator()(const LHCb::Track& aTrack) const = 0;

};

#endif
