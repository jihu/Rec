#ifndef _ITrackManipulator_H
#define _ITrackManipulator_H

#include "GaudiKernel/IAlgTool.h"
#include "Event/Track.h"


/** @class ITrackManipulator
 *
 *  interface for setting the track reference parameters from the measurement info
 *
 *  @author M.Needham
 *  @date   16/06/2006
 */


struct ITrackManipulator: extend_interfaces<IAlgTool> {

  DeclareInterfaceID( ITrackManipulator, 1, 0 );

  /** Add the reference information */
  virtual StatusCode execute(LHCb::Track& aTrack) const = 0;

};

#endif
