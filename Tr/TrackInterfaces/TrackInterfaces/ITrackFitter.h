#ifndef TRACKINTERFACES_ITRACKFITTER_H
#define TRACKINTERFACES_ITRACKFITTER_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/SystemOfUnits.h"
// from LHCbKernel
#include "Kernel/TrackDefaultParticles.h"

// vector
#include <vector>

// TrackVectorFit store
#include "Kernel/VectorSOAStore.h"
#include "Event/Track.h"

// Forward declarations

/** @class ITrackFitter ITrackFitter.h TrackInterfaces/ITrackFitter.h
 *
 *  Interface for a track fitting tool.
 *
 *  @author Jose A. Hernando, Eduardo Rodrigues
 *  @date   2005-05-25
 *
 *  @author Rutger van der Eijk  07-04-1999
 *  @author Mattiew Needham
 */
class ITrackFitter : public extend_interfaces<IAlgTool>
{

public:

  DeclareInterfaceID( ITrackFitter, 4, 0 );

  // Fit a track
  virtual StatusCode operator() (
    LHCb::Track& track,
    const LHCb::Tr::PID& pid = LHCb::Tr::PID::Pion()
  ) const = 0;

  // Fit a batch of tracks
  // Note: Returns SUCCESS if all tracks are Fitted
  virtual StatusCode operator() (
    std::vector<std::reference_wrapper<LHCb::Track>>& tracks,
    const LHCb::Tr::PID& pid = LHCb::Tr::PID::Pion()
  ) const = 0;
};

#endif // TRACKINTERFACES_ITRACKFITTER_H
