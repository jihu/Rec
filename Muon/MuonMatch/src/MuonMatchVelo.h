#ifndef MUONMATCH_VELO_H
#define MUONMATCH_VELO_H 1

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// from MuonMatch
#include "MuonMatchBase.h"
#include "MuonMatch/Candidate.h"

/** @class MuonMatchVelo MuonMatchVelo.h
 *  Look for Muon hits which match a velo track.
 *
 *  @author Roel Aaij
 *  @date   2010-12-02
 *
 *  @author Miguel Ramos Pernas
 *  @date   2017-10-05
 */

class MuonMatchVelo : public MuonMatchBase {
  
public:

  /// Inherit constructor
  using MuonMatchBase::MuonMatchBase;

  void addHits( Candidate& seed ) const override;

  Candidates findSeeds( const LHCb::Track& seed, const unsigned int seedStation ) const override;

  void fitCandidate( Candidate& seed ) const override;

  void match( const LHCb::Track& seed, LHCb::Track::Vector& tracks ) const override;

private:

  using DoubleProp = Gaudi::Property<double>;
  using UIntProp   = Gaudi::Property<unsigned int>;
  using BoolProp   = Gaudi::Property<bool>;

  // Properties
  DoubleProp m_za {this, "MagnetParA", 1.069*Gaudi::Units::m};
  DoubleProp m_zb {this, "MagnetParB", 5.203*Gaudi::Units::m};
  
  DoubleProp m_xWindow {this, "XWindow", 200*Gaudi::Units::mm};
  DoubleProp m_yWindow {this, "YWindow", 400*Gaudi::Units::mm};

  DoubleProp m_maxChi2DoFX {this, "MaxChi2DoFX", 10};

  UIntProp m_maxMissed {this, "MaxMissedHits", 2};

  BoolProp m_setQOverP {this, "SetQOverP", false};
 
};

#endif // MUONMATCH_VELO_H
