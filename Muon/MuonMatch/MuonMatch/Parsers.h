#ifndef MUONMATCH_PARSERS_H
#define MUONMATCH_PARSERS_H 1

// STL
#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <unordered_map>
#include <vector>

// from Gaudi
#include "GaudiKernel/GrammarsV2.h"
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StatusCode.h"
#include "GaudiKernel/ToStream.h"

/** Define parsers for the MuonMatch package
 *
 * @author Miguel Ramos Pernas
 * @date   2017-10-05
 */

namespace {
  typedef std::vector<double> doubles;
  typedef std::pair<double, double> dpair;
}

// Parsers for std::unordered_map types we need
using CorrectMap = std::unordered_map<std::string, doubles>;
using WindowMap  = std::unordered_map<unsigned int, dpair>;

#ifdef REGISTER_MAPGRAMMAR
#error REGISTER_MAPGRAMMAR has already been defined
#else

#define REGISTER_MAPGRAMMAR(ResultType)				\
  template <class Iterator, class Skipper>			\
  struct Grammar_<Iterator, ResultType, Skipper >		\
  {								\
    typedef MapGrammar<Iterator, ResultType, Skipper> Grammar;	\
  }								\

namespace Gaudi {
  namespace Parsers {
    
    // Register the parse information for the two new types
    REGISTER_MAPGRAMMAR(CorrectMap);
    REGISTER_MAPGRAMMAR(WindowMap);
    
  } // Parsers
} // Gaudi

#undef REGISTER_MAPGRAMMAR
#endif // REGISTER_MAPGRAMMAR


// We also need to be able to print an object of our type as a string that both
// Python and our parser can understand. Conversion to a "std::map" is needed
// to make a TCK, because it ensures the output is sorted and "fromString"
// followed by "toString" gives the same result.
#ifdef DEF_MAPGRAMMAR_STREAM
#error DEF_MAPGRAMMAR_STREAM has already been defined
#else
#define DEF_MAPGRAMMAR_STREAM(MapType, MapKey, MapValue)		\
  inline ostream& operator<<(ostream& s, const MapType& m)		\
  {									\
  bool first = true;							\
  s << '{';								\
  const std::map<MapKey, MapValue> m1(m.cbegin(), m.cend());		\
  for(const auto& i: m1)						\
    {									\
      if (first) first = false;						\
      else s << ", ";							\
      Gaudi::Utils::toStream(i.first, s) << ": ";			\
      Gaudi::Utils::toStream(i.second, s);				\
    }									\
  s << '}';								\
  return s;								\
  }									\

namespace std {

  DEF_MAPGRAMMAR_STREAM(CorrectMap, std::string, doubles)
  DEF_MAPGRAMMAR_STREAM(WindowMap, unsigned int, dpair)

}

#undef DEF_MAPGRAMMAR_STREAM
#endif // DEF_MAPGRAMMAR_STREAM

#endif // MUONMATCH_PARSERS_H
