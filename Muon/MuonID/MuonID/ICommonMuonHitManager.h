#ifndef ICOMMONMUONHITMANAGER_H_
#define ICOMMONMUONHITMANAGER_H_

#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "MuonID/CommonMuonHit.h"
#include "MuonID/CommonMuonStation.h"


struct ICommonMuonHitManager : extend_interfaces<IAlgTool, IIncidentListener> {
  DeclareInterfaceID(ICommonMuonHitManager, 2, 0);

  virtual CommonMuonHitRange hits(float xmin, unsigned int station,
                                  unsigned int region) = 0;
  virtual CommonMuonHitRange hits(float xmin, float xmax,
                                  unsigned int station, unsigned int region) = 0;

  virtual unsigned int nRegions(unsigned int station) const = 0;

  virtual const CommonMuonStation& station(unsigned int id) const = 0;
};

#endif  // ICOMMONMUONHITMANAGER_H_
