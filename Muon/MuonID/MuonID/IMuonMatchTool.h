#ifndef MUONID_IMUONMATCHTOOL_H
#define MUONID_IMUONMATCHTOOL_H 1
#include <vector>
#include <tuple>

#include "GaudiKernel/IAlgTool.h"
#include "MuonID/CommonMuonHit.h"
#include "Event/Track.h"

namespace LHCb {
  class MuonTileID;
}

/** @class IMuonMatchTool IMuonMatchTool.h MuonMatch/IMuonMatchTool.h
 *
 *  tool to match tracks to the muon detector, starting from pre-selected matching tables
 *
 *  @author Giacomo Graziani
 *  @date   2015-11-10
 */

/// TrackMuMatch contains MuonCommonHit, sigma_match, track extrap X, track extrap Y
typedef std::tuple<const CommonMuonHit*,float,float,float> TrackMuMatch;

struct IMuonMatchTool : extend_interfaces< IAlgTool> {

  DeclareInterfaceID( IMuonMatchTool, 3, 0);

  enum MuonMatchType {NoMatch=0, Uncrossed=1, Good=2};

  virtual StatusCode run(const LHCb::Track* track,
                         std::vector<TrackMuMatch>* bestMatches,
                         std::vector<TrackMuMatch>* spareMatches=nullptr)=0;
  virtual CommonConstMuonHits getListofCommonMuonHits(int station=-1, bool onlybest=true) const =0;
  virtual std::vector<LHCb::MuonTileID> getListofMuonTiles(int station=-1, bool onlybest=true) const =0;
  virtual std::pair<float,int>  getChisquare() const=0;
  virtual float getMaxChi2Contrib() const=0;
  virtual MuonMatchType getMatch(int station) const=0;
  virtual float getMatchSigma(unsigned int station) const=0;

};
#endif // MUONID_IMUONMATCHTOOL_H
