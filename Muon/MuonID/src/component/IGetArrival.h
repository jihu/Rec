#ifndef IGETARRIVAL_H 
#define IGETARRIVAL_H 1

// Include files
// from STL
#include <vector>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "Event/Track.h"


/** @class IGetArrival ICLTool.h
 *  
 *
 *  @author Jose Angel Hernando Morata
 *  @author Xabier Cid Vidal
 *  @date   2008-07-02
 */
struct IGetArrival : extend_interfaces<IAlgTool> {

  // Return the interface ID
  DeclareInterfaceID( IGetArrival, 1, 0 );
  
  virtual StatusCode getArrivalFromTrack(const LHCb::Track& mutrack, double& parr)=0;
  virtual StatusCode clArrival(const LHCb::Track& muTrack, double& clarr)=0;
  virtual StatusCode clArrival(const double p,const std::vector<int>& type_st, double& clarr)=0;

};
#endif // IGETARRIVAL_H
