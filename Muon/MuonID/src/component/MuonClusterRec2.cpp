// Include files

#include <numeric>

// local
#include "MuonClusterRec2.h"
#include "MuonInterfaces/MuonLogPad.h"
#include "MuonInterfaces/MuonHit.h"

using namespace LHCb;
using namespace std;

//-----------------------------------------------------------------------------
// Implementation file for class : MuonClusterRec2
//
// clustering tool for the muon detector
// 2017-05-04 : Florian Lemaitre, Marco Santimaria, Manuel Schiller
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( MuonClusterRec2 )

MuonClusterRec2::ClusteringContext MuonClusterRec2::setupClustering(LHCb::span<const MuonLogPad> pads) const
{
  ClusteringContext retVal;
  auto& [ cluster_ids, neighbours ] = retVal;

  cluster_ids.reserve(pads.size());

  for (const auto& pad : pads) {
    if( pad.truepad() ) cluster_ids.push_back(pad.tile());
  }

  std::sort(cluster_ids.begin(), cluster_ids.end());


  neighbours.reserve(cluster_ids.size());

  auto ourNeighbour = [] (LHCb::MuonTileID ref, int dx, int dy) {
    enum {
      MaskX = (1 << MuonBase::BitsX) - 1, MaskY = (1 << MuonBase::BitsY) - 1
    };
    if (int(ref.nX() + dx) < 0 || (int(ref.nX() + dx) & ~MaskX) ||
        int(ref.nY() + dy) < 0 || (int(ref.nY() + dy) & ~MaskY))
      return MuonTileID();
    LHCb::MuonTileID retVal(ref);
    retVal.setX(ref.nX() + dx);
    retVal.setY(ref.nY() + dy);
    return retVal;
  };

  for (auto& cluster_id: cluster_ids) {
    std::array<LHCb::MuonTileID, 5> id_mask = {
      cluster_id, // CENTER
      ourNeighbour(cluster_id, 0, -1), // DOWN
      ourNeighbour(cluster_id, 0, 1), // UP
      ourNeighbour(cluster_id, -1 ,0), // LEFT
      ourNeighbour(cluster_id, 1, 0), // RIGHT
    };
    Neighbourhood found;
    auto jt = found.begin();
    auto findit = [&jt, &cluster_ids] (Tiles::iterator begin, Tiles::iterator end, LHCb::MuonTileID needle) {
      if (MuonTileID() == needle) return;
      auto it = std::lower_bound(begin, end, needle);
      if (cluster_ids.end() == it || needle != *it) return;
      *jt++ = &*it;
    };
    *jt++ = &cluster_id;
    auto kt = cluster_ids.begin() + (&cluster_id - &cluster_ids.front());
    if (cluster_ids.begin() != kt && *(kt - 1) == id_mask[1]) *jt++ = &*(kt - 1);
    if (cluster_ids.end() != kt && *(kt + 1) == id_mask[2]) *jt++ = &*(kt + 1);
    findit(cluster_ids.begin(), kt, id_mask[3]);
    findit(kt, cluster_ids.end(), id_mask[4]);
    if (1 == (jt - found.begin())) continue;
    neighbours.emplace_back(std::move(found),jt-found.begin());
    //neighbours_end.push_back(neighbours.back().begin() + (jt - found.begin()));
    //neighbours_end.push_back(jt - found.begin());
  }
  return retVal;
}

void MuonClusterRec2::doClustering(MuonClusterRec2::ClusteringContext& ctx) const
{
  auto& [ cluster_ids, neighbours ] = ctx;

  bool changed;
  do {
    changed = false;

    for(auto& i : neighbours){
      auto end = std::next(i.first.begin(),i.second);
      auto& mask = i.first;
      auto iter = std::min_element(mask.begin(), end,
                                   [] (const MuonTileID* id1, const MuonTileID* id2)
	                               {return *id1 < *id2;} );
      MuonTileID min = **iter;
      for (auto it = mask.begin(); end != it; ++it) {
        auto pid = *it;
        if(*pid!=min) {
            *pid = min;
            changed = true;
        }
      }
    }
  } while (changed);

  std::sort(cluster_ids.begin(), cluster_ids.end());

}

ClusterVars MuonClusterRec2::clusters(LHCb::span<const MuonLogPad> pads) const {


  auto clusctx = setupClustering(pads);
  doClustering(clusctx);

  ClusterVars Cvars;

  const auto& cluster_ids = std::get<0>(clusctx);
  for (auto it = cluster_ids.begin(); cluster_ids.end() != it; ) {
    int station = (*it).station();
    auto jt = it++;
    it = std::find_if( it, cluster_ids.end(), [&](const auto& i) { return i!=*jt; } );

    if(station!=0){
      Cvars.avg_cs[station-1] += it - jt; // setting index 0 to M2
      ++Cvars.ncl[station-1];
    }
  }

  for(unsigned i = 0; i < Cvars.ncl.size(); i++){
    if (Cvars.ncl[i]>0) Cvars.avg_cs[i] /= Cvars.ncl[i];
  }

  //  printf("Called makeMuonClusters() with NRAWHITS: %lu\n", pads.size());
  //  printf("nclusters: %i with avg_csize: %f\n", ncl, avg_cs);

  return Cvars;


}
