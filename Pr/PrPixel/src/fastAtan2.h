#ifndef FastAtan2_H
#define FastAtan2_H 1

#include <cmath>
constexpr auto ONEQTR_PI = M_PI/4.0;
constexpr auto THRQTR_PI = 3.0*M_PI/4.0;
constexpr float RADDEG = 180.0 / M_PI;
constexpr float PI_FLOAT = M_PI;
constexpr float PIBY2_FLOAT = M_PI/2;

inline float atan2_approximation1(const float y, const float x) noexcept{
  float abs_y = fabs(y) + 1e-10f;
  // used to prevent 0/0 conditio
  if ( x < 0.0f ){
    float r = (x + abs_y) / (abs_y - x);
    float angle = THRQTR_PI + (0.1963f * r * r - 0.9817f) * r; ;
    if( y<0.0f) return -angle*RADDEG;
    return angle*RADDEG;
  }else{
    float r = (x-abs_y)/( x + abs_y);
    float angle = ONEQTR_PI + (0.1963f * r * r - 0.9817f) * r; ;
    if( y<0.0f) return -angle*RADDEG;
    return angle * RADDEG;
  }
}

#endif // FASTATAN2_H
