#ifndef PRUTMAGNETTOOL_H
#define PRUTMAGNETTOOL_H 1

// Include files
#include <limits>
#include "GaudiAlg/GaudiTool.h"

// Forward declarations
class IMagneticFieldSvc;
#include "PrTableForFunction.h"
#include "STDet/DeSTDetector.h"

/** @class PrUTMagnetTool PrUTMagnetTool.h newtool/PrUTMagnetTool.h
 *
 *  Magnet tool for Pr
 *
 *  @author Mariusz Witek
 *  @date   2006-09-25
 *  @update for A-Team framework 2007-08-20 SHM
 *
 */

class PrUTMagnetTool final : public GaudiTool {
public:

  /// Standard constructor
  using GaudiTool::GaudiTool;

  StatusCode initialize() override;

  float bdlIntegral(float ySlopeVelo,float zOrigin,float zVelo) const {
    return  m_noField  ? s_bdlIntegral_NoB
                       : m_lutBdl.getInterpolatedValue({ ySlopeVelo, zOrigin,zVelo });
  }
  float zBdlMiddle(float ySlopeVelo,float zOrigin,float zVelo) const {
    return m_noField ? s_zBdlMiddle_NoB
                     : m_lutZHalfBdl.getInterpolatedValue({ ySlopeVelo, zOrigin,zVelo });
  }
  float dist2mom(float ySlope) const {
    return m_noField ? s_averageDist2mom_NoB
                     : m_lutDxToMom.getValue({ ySlope });
  }

  //=========================================================================
  // z middle of UT
  //=========================================================================
  float zMidUT() const { return m_zCenterUT; }
  //=========================================================================
  // z middle of B field betweenz=0 and zMidUT
  //=========================================================================
  float zMidField() const { return m_noField ? s_zMidField_NoB : m_zMidField; }
  //=========================================================================
  // averageDist2mom
  //=========================================================================
  float averageDist2mom() const { return m_noField ? s_averageDist2mom_NoB : m_dist2mom; }

  const auto& DxLayTable() const { return m_lutDxLay; }
  const auto& BdlTable() const { return m_lutBdl; }

private:
  void prepareBdlTables();
  void prepareDeflectionTables();
  StatusCode updateField() ;

  struct bdl_t { float BdlTrack, zHalfBdlTrack; };
  bdl_t f_bdl(float slopeY, float zOrigin , float zStart, float zStop) const;

  IMagneticFieldSvc* m_magFieldSvc = nullptr;   /// pointer to mag field service
  DeSTDetector*      m_STDet = nullptr;         ///< ST Detector element
  std::string        m_detLocation;

  std::array<float,4> m_zLayers = { 0 };

  float m_zCenterUT = std::numeric_limits<float>::signaling_NaN();
  float m_zMidField = std::numeric_limits<float>::signaling_NaN();
  float m_dist2mom = std::numeric_limits<float>::signaling_NaN();

  PrTableForFunction<30,10,10> m_lutBdl { PrTable::Range{-0.3,+0.3},
                                          PrTable::Range{-250.*Gaudi::Units::mm, 250.*Gaudi::Units::mm},
                                          PrTable::Range{ 0.*Gaudi::Units::mm, 800.*Gaudi::Units::mm } };
  PrTableForFunction<30,10,10> m_lutZHalfBdl { PrTable::Range{-0.3,+0.3},
                                               PrTable::Range{-250.*Gaudi::Units::mm, 250.*Gaudi::Units::mm},
                                               PrTable::Range{ 0.*Gaudi::Units::mm, 800.*Gaudi::Units::mm } };
  PrTableForFunction<3,30>  m_lutDxLay { PrTable::Range{ 0., 3.},  // index of UT layer 1 to 4
                                         PrTable::Range{ 0.0, 0.3} }; // slope y
  PrTableForFunction<30> m_lutDxToMom { PrTable::Range{ 0.0, 0.3 } }; // slope y;



  bool m_noField = false;
  // set parameters for no field run
  constexpr static float s_bdlIntegral_NoB = 0.1;
  constexpr static float s_zBdlMiddle_NoB = 1900.;
  constexpr static float s_zMidField_NoB = 1900.;
  constexpr static float s_averageDist2mom_NoB = 0.00004;

};


#endif // PRUTMAGNETTOOL_H
