#ifndef PRVELOUT_H
#define PRVELOUT_H 1

//#define SMALL_OUTPUT

// Include files
//
#include <numeric>

// from Gaudi
#include "GaudiAlg/ISequencerTimerTool.h"
#include "GaudiAlg/Transformer.h"

// from TrackInterfaces
#include "Event/Track.h"
#include "Event/RecVertex.h"

#include "PrKernel/UTHitHandler.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/ObjectContainerBase.h"
#include "GaudiKernel/Range.h"
#include "PrKernel/UTHitInfo.h"
#include "PrKernel/UTHit.h"
#include "TfKernel/IndexedHitContainer.h"
#include "TfKernel/MultiIndexedHitContainer.h"
#include "PrUTMagnetTool.h"

#include "PrKernel/PrVeloUTTrack.h"
#include "Event/RecVertex.h"
#include "vdt/sqrt.h"
/** @class PrVeloUT PrVeloUT.h
   *
   *  PrVeloUT algorithm. This is just a wrapper,
   *  the actual pattern recognition is done in the 'PrVeloUTTool'.
   *
   *  - InputTracksName: Input location for Velo tracks
   *  - OutputTracksName: Output location for VeloTT tracks
   *  - TimingMeasurement: Do a timing measurement?
   *
   *  @author Mariusz Witek
   *  @date   2007-05-08
   *  @update for A-Team framework 2007-08-20 SHM
   *
   *  2017-03-01: Christoph Hasse (adapt to future framework)
   */


struct MiniState final{
  float x,y,z,tx,ty;
};

struct TrackHelper final {
  TrackHelper(const MiniState& miniState, const float zKink, const float sigmaVeloSlope, const float maxPseudoChi2):
    state(miniState),
    bestParams{{ 0.0, maxPseudoChi2, 0.0, 0.0 }}{
    xMidField = state.x + state.tx*(zKink-state.z);
    const float a = sigmaVeloSlope*(zKink - state.z);
    wb=1./(a*a);
    invKinkVeloDist = 1/(zKink-state.z);
  }

  MiniState state;
  std::array<const UT::Mut::Hit*, 4> bestHits = { nullptr, nullptr, nullptr, nullptr};
  std::array<float, 4> bestParams;
  float wb, invKinkVeloDist, xMidField;

};

struct LayerInfo final {
  float z;
  unsigned int nColsPerSide;
  unsigned int nRowsPerSide;
  float invHalfSectorYSize;
  float invHalfSectorXSize;
  float dxDy;
};

#ifdef SMALL_OUTPUT
class PrVeloUT : public Gaudi::Functional::Transformer<PrVeloUTTracks(const std::vector<LHCb::Event::v1::Track>&, const UT::HitHandler& )>{
  using Track = LHCb::Event::v1::Track;
  using OutputContainer = PrVeloUTTracks ;
#else
  class PrVeloUT : public Gaudi::Functional::Transformer<std::vector<LHCb::Event::v1::Track>(const std::vector<LHCb::Event::v1::Track>&, const  std::vector<LHCb::RecVertex>&, const UT::HitHandler&)>{
  using Track = LHCb::Event::v1::Track;
  using OutputContainer = std::vector<Track>;
#endif

public:


  /// Standard constructor
  PrVeloUT( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization

  OutputContainer operator()(const std::vector<Track>& inputTracks,
#ifndef SMALL_OUTPUT
                             const std::vector<LHCb::RecVertex>& vertices,
#endif
                             const UT::HitHandler& uthits) const override;

private:

  Gaudi::Property<float> m_minMomentum    {this, "minMomentum",      1.5*Gaudi::Units::GeV};
  Gaudi::Property<float> m_minPT          {this, "minPT",            0.3*Gaudi::Units::GeV};
  Gaudi::Property<float> m_maxPseudoChi2  {this, "maxPseudoChi2",    1280.};
  Gaudi::Property<float> m_yTol           {this, "YTolerance",       0.5  * Gaudi::Units::mm}; // 0.8
  Gaudi::Property<float> m_yTolSlope      {this, "YTolSlope",        0.08}; // 0.2
  Gaudi::Property<float> m_hitTol1        {this, "HitTol1",          6.0 * Gaudi::Units::mm};
  Gaudi::Property<float> m_hitTol2        {this, "HitTol2",          0.8 * Gaudi::Units::mm}; // 0.8
  Gaudi::Property<float> m_deltaTx1       {this, "DeltaTx1",         0.035};
  Gaudi::Property<float> m_deltaTx2       {this, "DeltaTx2",         0.018}; // 0.02
  Gaudi::Property<float> m_maxXSlope      {this, "MaxXSlope",        0.350};
  Gaudi::Property<float> m_maxYSlope      {this, "MaxYSlope",        0.300};
  Gaudi::Property<float> m_centralHoleSize{this, "centralHoleSize",  33. * Gaudi::Units::mm};
  Gaudi::Property<float> m_intraLayerDist {this, "IntraLayerDist",   15.0 * Gaudi::Units::mm};
  Gaudi::Property<float> m_overlapTol     {this, "OverlapTol",       0.7 * Gaudi::Units::mm};
  Gaudi::Property<float> m_passHoleSize   {this, "PassHoleSize",     40. * Gaudi::Units::mm};
  Gaudi::Property<int>   m_minHighThres   {this, "MinHighThreshold", 1};
  Gaudi::Property<bool>  m_printVariables {this, "PrintVariables",   false};
  Gaudi::Property<bool>  m_passTracks     {this, "PassTracks",       false};
  Gaudi::Property<bool>  m_doTiming       {this, "TimingMeasurement",false};

  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_seedsCounter{ this, "#seeds" };
  mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_tracksCounter{ this, "#tracks" };

  StatusCode recomputeGeometry();

  template <typename Container>
  bool getState(const Track& iTr, MiniState& trState, Container& outputTracks) const;

  template <typename FudgeTable>
  bool getHits(LHCb::span<UT::Mut::Hits,4> hitsInLayers,
               const UT::HitHandler& hh,
               const FudgeTable& fudgeFactors, MiniState& trState ) const;

  bool formClusters(LHCb::span<const UT::Mut::Hits,4> hitsInLayers, TrackHelper& helper) const;

  template <typename Container, typename BdlTable>
  void prepareOutputTrack(const Track& veloTrack,
                          const TrackHelper& helper,
                          LHCb::span<const UT::Mut::Hits,4> hitsInLayers,
                          Container& outputTracks,
                          const BdlTable& bdlTable) const;

    constexpr static const auto mapQuarterSectorToSectorCentralRegion = std::array{
      6,6,9,9,10,10,13,13,
        7,7,8,8,11,11,12,12,
        25,25,26,28,31,33,34,34,
        24,24,27,29,30,32,35,35,
        46,46,49,51,52,54,57,57,
        47,47,48,50,53,55,56,56,
        69,69,70,70,73,73,74,74,
        68,68,71,71,72,72,75,75
        };

    constexpr static const auto  mapSectorToSector = std::array{
      1,2,3,4,5,0,0,0,0,14,15,16,17,18,
        19,20,21,22,23,0,0,0,0,36,37,38,39,40,
        41,42,43,44,45,0,0,0,0,58,59,60,61,62,
        63,64,65,66,67,0,0,0,0,76,77,78,79,80
        };

    /**
     * fills container of (region, sector) pairs with all sectors concerned by
     * a hit at given layer and coordinates and with given x tolerance
     */
    template <typename Container>
    void findSectors(unsigned int layer, float x, float y, float xTol, float yTol,
                     Container& sectors) const {
      const auto& info = m_layers[layer];
      auto localX = x - info.dxDy * y;
      // deal with sector overlaps and geometry imprecision
      xTol += 1; //mm
      auto localXmin = localX - xTol;
      auto localXmax = localX + xTol;
      int subcolmin = std::round(localXmin * info.invHalfSectorXSize-0.5)+2*info.nColsPerSide;
      int subcolmax = std::round(localXmax * info.invHalfSectorXSize-0.5)+2*info.nColsPerSide;
      if (subcolmax < 0 || subcolmin >= (int)(4*info.nColsPerSide)) {
        // out of acceptance, return empty result
        return;
      }
      // on the acceptance limit
      if (subcolmax >= (int)(4*info.nColsPerSide)) subcolmax = (int)(4*info.nColsPerSide) - 1;
      if (subcolmin < 0) subcolmin = 0;
      // deal with sector shifts in tilted layers and overlaps in regular ones
      yTol += (layer == 1 || layer == 2) ? 8 : 1; //  mm
      auto localYmin = y-yTol;
      auto localYmax = y+yTol;
      int subrowmin = std::round(localYmin * info.invHalfSectorYSize-0.5)+2*info.nRowsPerSide;
      int subrowmax = std::round(localYmax * info.invHalfSectorYSize-0.5)+2*info.nRowsPerSide;
      if (subrowmax < 0 || subrowmin >= (int)(4*info.nRowsPerSide)) {
        // out of acceptance, return empty result
        return;
      }
      // on the acceptance limit
      if (subrowmax >= (int)(4*info.nRowsPerSide)) subrowmax = (int)(4*info.nRowsPerSide) - 1;
      if (subrowmin <0) subrowmin = 0;
      for (int subcol=subcolmin; subcol <= subcolmax; subcol++) {
        int region = subcol < (int)(2*info.nColsPerSide-4) ? 1 : subcol >= (int)(2*info.nColsPerSide+4) ? 3 : 2;
        for (int subrow=subrowmin; subrow <= subrowmax; subrow++) {
          if (region == 1) {
            sectors.emplace_back(1, (subcol/2)*info.nRowsPerSide*2+subrow/2+1);
          } else if (region == 2) {
            int subcolInReg = subcol - 2*info.nColsPerSide + 4;
            if (subrow < (int)(2*info.nRowsPerSide-4) || subrow >= (int)(2*info.nRowsPerSide+4)) {
              // no in central Region
              sectors.emplace_back(2, mapSectorToSector[(subcolInReg/2)*14+(subrow/2)]);
            } else {
              // central region
              sectors.emplace_back(2, mapQuarterSectorToSectorCentralRegion[subcolInReg*8+subrow-2*info.nRowsPerSide+4]);
            }
          } else {
            sectors.emplace_back(3, (subcol/2-info.nColsPerSide-2)*info.nRowsPerSide*2+subrow/2+1);
          }
        }
      }
    }

  // ==============================================================================
  // -- Method that finds the hits in a given layer within a certain range
  // ==============================================================================
    template< typename RANGE >
    void findHits( RANGE range,
                   float zInit,
                   const MiniState& myState, const float xTolNormFact,
                   const float invNormFact, UT::Mut::Hits& hits) const {

    const auto yApprox       = myState.y + myState.ty * (zInit - myState.z);
    const auto xOnTrackProto = myState.x + myState.tx * (zInit - myState.z);
    const auto yyProto       = myState.y - myState.ty * myState.z;

    auto first = std::find_if_not( range.begin(), range.end(),
                                   [ y = m_yTol + m_yTolSlope * std::abs(xTolNormFact), yApprox ]
                                   ( const auto& h )
                                   { return h.isNotYCompatible( yApprox, y ); } );
    for ( auto last = range.end() ; first!=last; ++first ) {
      const auto& hit  = *first;

      const auto xx = hit.xAt(yApprox);
      const auto dx = xx - xOnTrackProto;

      if( dx < -xTolNormFact ) continue;
      if( dx >  xTolNormFact ) break;

      // -- Now refine the tolerance in Y
      if(  hit.isNotYCompatible( yApprox, m_yTol + m_yTolSlope * std::abs(dx*invNormFact)) ) continue;

      const auto zz = hit.zAtYEq0();
      const auto yy = yyProto +  myState.ty*zz;
      const auto xx2 = hit.xAt(yy);
      hits.emplace_back(&hit, xx2, zz);

    }
  }

  // ===========================================================================================
  // -- 2 helper functions for fit
  // -- Pseudo chi2 fit, templated for 3 or 4 hits
  // ===========================================================================================
  void addHit( LHCb::span<float,3> mat, LHCb::span<float,2> rhs, const UT::Mut::Hit& hit)const{
    const float ui = hit.x;
    const float ci = hit.HitPtr->cosT();
    const float dz = 0.001*(hit.z - m_zMidUT);
    const float wi = hit.HitPtr->weight();
    mat[0] += wi * ci;
    mat[1] += wi * ci * dz;
    mat[2] += wi * ci * dz * dz;
    rhs[0] += wi * ui;
    rhs[1] += wi * ui * dz;
  }




  template <std::size_t N>
  void simpleFit( std::array<const UT::Mut::Hit*,N> hits, TrackHelper& helper) const {
    static_assert( N==3||N==4 );

    // -- Veto hit combinations with no high threshold hit
    // -- = likely spillover
    const int nHighThres = std::count_if( hits.begin(),  hits.end(),
                                          []( const UT::Mut::Hit* hit ){ return hit && hit->HitPtr->highThreshold(); });

    if( nHighThres < m_minHighThres ) return;

    // -- Scale the z-component, to not run into numerical problems
    // -- with floats
    const float zDiff = 0.001*(m_zKink-m_zMidUT);
    auto mat = std::array{ helper.wb, helper.wb*zDiff, helper.wb*zDiff*zDiff };
    auto rhs = std::array{ helper.wb* helper.xMidField, helper.wb*helper.xMidField*zDiff };
    std::for_each( hits.begin(), hits.end(), [&](const auto* h) { this->addHit(mat,rhs,*h); } );

    ROOT::Math::CholeskyDecomp<float, 2> decomp(mat.data());
    if( UNLIKELY(!decomp)) return;

    decomp.Solve(rhs);

    const float xSlopeTTFit = 0.001*rhs[1];
    const float xTTFit = rhs[0];

    // new VELO slope x
    const float xb = xTTFit+xSlopeTTFit*(m_zKink-m_zMidUT);
    const float xSlopeVeloFit = (xb-helper.state.x)*helper.invKinkVeloDist;
    const float chi2VeloSlope = (helper.state.tx - xSlopeVeloFit)*m_invSigmaVeloSlope;

    const float chi2TT = std::accumulate( hits.begin(), hits.end(), chi2VeloSlope*chi2VeloSlope,
                                          [&](float chi2, const auto* hit) {
                                              const float du    = (xTTFit + xSlopeTTFit*(hit->z-m_zMidUT)) - hit->x;
                                              return chi2 + hit->HitPtr->weight()*(du*du);
                                          } ) / ( N + 1 - 2 );

    if( chi2TT < helper.bestParams[1] ){

      // calculate q/p
      const float sinInX  = xSlopeVeloFit*vdt::fast_isqrt(1.+xSlopeVeloFit*xSlopeVeloFit);
      const float sinOutX = xSlopeTTFit*vdt::fast_isqrt(1.+xSlopeTTFit*xSlopeTTFit);
      const float qp = (sinInX-sinOutX);

      helper.bestParams = { qp, chi2TT, xTTFit, xSlopeTTFit };

      std::copy( hits.begin(), hits.end(), helper.bestHits.begin() );
      if( N == 3 ) { helper.bestHits[3] = nullptr ; }
    }

  }
  // --
  DeSTDetector *m_utDet = nullptr;

  Gaudi::Property<float> m_minIP{this, "minIP", 0.1*Gaudi::Units::millimeter};
  Gaudi::Property<bool> m_doIPCut{this, "doIPCut", false};

  /// Multipupose tool for Bdl and deflection
  ToolHandle<PrUTMagnetTool> m_PrUTMagnetTool { this, "PrUTMagnetTool", "PrUTMagnetTool" };
  /// timing tool
  mutable ToolHandle<ISequencerTimerTool> m_timerTool { this, "SequencerTimerTool", "SequencerTimerTool" }; //FIXME
  ///< Counter for timing tool
  int   m_veloUTTime{0};

  float m_zMidUT;
  float m_distToMomentum;
  float m_zKink{1780.0};
  float m_sigmaVeloSlope{0.10*Gaudi::Units::mrad};
  float m_invSigmaVeloSlope{10.0/Gaudi::Units::mrad};

  /// information about the different layers
  std::array<LayerInfo,4> m_layers;

  using SectorsInRegionZ = std::array<float,98>;
  using SectorsInLayerZ = std::array<SectorsInRegionZ,3>;
  using SectorsInStationZ = std::array<SectorsInLayerZ,2>;
  std::array<SectorsInStationZ,2> m_sectorsZ;

};

#endif // PRVELOUT_H
