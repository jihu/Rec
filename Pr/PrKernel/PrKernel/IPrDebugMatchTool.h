#ifndef IPRDEBUGMATCHTOOL_H
#define IPRDEBUGMATCHTOOL_H 1

// Include files

// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "Event/Track.h"

//forward declarations                                                                                     

/** @class PrDebugMatchTool 
 *
 *  @author Sevda Esem
 *  @date   2017-02-21
 */
struct IPrDebugMatchTool : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IPrDebugMatchTool, 2, 0 );


  virtual int matchMCPart(const LHCb::Track& velo, const LHCb::Track& seed) const =0;


  virtual void fillTuple(const LHCb::Track& velo, const LHCb::Track& seed, const  std::vector<float>& vars) const =0;

};
#endif // IPRDEBUGMATCHTOOL_H
