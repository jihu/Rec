#ifndef LHCB_CONVERTERS_TRACK_V1_FROMVECTORLHCBTRACK_H 
#define LHCB_CONVERTERS_TRACK_V1_FROMVECTORLHCBTRACK_H 


// Include files 
// from Gaudi
#include "GaudiAlg/ScalarTransformer.h"
#include "Event/Track.h"

/** @class fromVectorLHCbTrack fromVectorLHCbTrack.h
 *  
 *  Small helper to convert std::vector<LHCb::Track> to LHCb::Tracks
 *
 *  @author Renato Quagliani, Michel De Cian
 *  @date   2018-03-09
 */

namespace LHCb{
  namespace Converters{
    namespace Track{
      namespace v1{
        
        struct fromVectorLHCbTrack : public Gaudi::Functional::ScalarTransformer<fromVectorLHCbTrack, LHCb::Tracks(const std::vector<LHCb::Track>&)>{
          
          fromVectorLHCbTrack( const std::string& name, ISvcLocator* pSvcLocator ):    
            ScalarTransformer(name, pSvcLocator,
                              KeyValue{"InputTracksName", ""} ,
                              KeyValue{"OutputTracksName", ""}){}
          
          
          using ScalarTransformer::operator();
          
          /// The main function, converts the track
          LHCb::Track* operator()(const LHCb::Track& track) const {
            if (track.hasKey()){
              return new LHCb::Track(track, track.key());
            }else{
              return new LHCb::Track(track);
            }
          }
          
        };
        DECLARE_COMPONENT(fromVectorLHCbTrack)
      }
    }
  }
}
#endif // LHCB_CONVERTERS_TRACK_V1_FROMVECTORLHCBTRACK_H  
