#ifndef LHCB_CONVERTERS_TRACK_V1_FROMLHCBTRACKVECTOR_H
#define LHCB_CONVERTERS_TRACK_V1_FROMLHCBTRACKVECTOR_H


// Include files
// from Gaudi
#include "GaudiAlg/Transformer.h"
#include "Event/Track.h"

/** @class fromLHCbTrackVector fromLHCbTrackVector.h
 *
 *  Small helper to convert LHCb::Tracks to std::vector<LHCb::Track>
 *
 */

namespace LHCb{
  namespace Converters{
    namespace Track{
      namespace v1{
        class fromLHCbTrackVector : public Gaudi::Functional::Transformer<std::vector<LHCb::Track>(const LHCb::Tracks&)>{
	public:
          fromLHCbTrackVector( const std::string& name, ISvcLocator* pSvcLocator ):
            Transformer(name, pSvcLocator,
			KeyValue{"InputTracksName", ""} ,
			KeyValue{"OutputTracksName", ""}){}
          // The main function, converts the track
	  std::vector<LHCb::Track> operator()(const LHCb::Tracks& inputTracks) const override
          {
	    std::vector<LHCb::Track> outputTracks;
	    outputTracks.reserve(inputTracks.size());
	    for (const auto& track : inputTracks){
	      if (track->hasKey())
		{
		  outputTracks.emplace_back(*track,track->key());
		}else
		{
		  outputTracks.emplace_back(*track);
		}
	    }
	    return outputTracks;
          }
        };
        DECLARE_COMPONENT(fromLHCbTrackVector)
      }
    }
  }
}
#endif // LHCB_CONVERTERS_TRACK_V1_FROMLHCBTRACKVECTOR_H
