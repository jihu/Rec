#ifndef IPRADDUTHITSTOOL_H
#define IPRADDUTHITSTOOL_H 1

// Include files
// -------------
#include <vector>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "PrKernel/UTHit.h"
#include "Event/Track.h"

/** @class IPrAddUTHitsTool IPrAddUTHitsTool.h TrackInterfaces/IPrAddUTHitsTool.h
 *
 *  @author:  Michel De Cian
 *  @date:    13-11-2013
 */

// forward declaration
namespace LHCb
{
  class State;
}

class IPrAddUTHitsTool : public extend_interfaces<IAlgTool>
{
  using Track = LHCb::Event::v1::Track;
  
 public:
  DeclareInterfaceID( IPrAddUTHitsTool, 2, 0 );

  /// Add UT clusters to matched tracks
  virtual StatusCode addUTHits( Track& track ) const = 0;
  virtual UT::Mut::Hits returnUTHits( LHCb::State& state, double& finalChi2, double p ) const = 0;
};
#endif // TRACKINTERFACES_IPRADDUTHITSTOOL_H
