#include "Event/Track.h"
#include "GaudiAlg/FilterPredicate.h"
#include <vector>

template<typename Container>
class PrCheckEmptyFilter : public Gaudi::Functional::FilterPredicate<bool(
                               const Container &)> {

public:
  // Standard Constructor
  PrCheckEmptyFilter(const std::string &name, ISvcLocator *pSvcLocator)
    : Gaudi::Functional::FilterPredicate<bool(const Container &)>(
          name, pSvcLocator,
          {"inputLocation", ""}) {}

  bool operator()(const Container &inputs) const override {
    if (this->msgLevel(MSG::DEBUG))
      this->debug() << this->inputLocation() << " is " << (inputs.empty()? "empty" : "not empty") << endmsg;
    return !inputs.empty();
  }
};

DECLARE_COMPONENT_WITH_ID(PrCheckEmptyFilter<LHCb::Track::Selection>, "PrCheckEmptyTracks")
