#ifndef IPRFORWARDTOOL_H
#define IPRFORWARDTOOL_H 1

// Include files
// from STL
#include "GaudiKernel/IAlgTool.h"
#include "Event/Track.h"
#include "PrKernel/PrFTHitHandler.h"
#include "PrKernel/IPrDebugTool.h"
#include <string>
/** @class IPrForwardTool IPrForwardTool.h
 *  Interface for the various implementations of the Forward tool
 *
 *  @author Olivier Callot
 *  @date   2012-07-13
 */


struct IPrForwardTool : public extend_interfaces<IAlgTool> {
  using Track = LHCb::Event::v1::Track;
  
  DeclareInterfaceID( IPrForwardTool, 3, 0);
 
  virtual std::vector<Track> extendTracks( LHCb::span<const Track> velo , const PrFTHitHandler<PrHit>& hitHandler) const = 0;
  

  // -- Debugging stuff
  virtual void setDebugParams( IPrDebugTool* tool, const int key, const int veloKey ) = 0;
  
  virtual bool matchKey( const PrHit& hit ) const = 0;
  
  virtual void printHit ( const PrHit& hit, const std::string title = "" ) const = 0;
  

};
#endif // IPRFORWARDTOOL_H
