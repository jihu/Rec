################################################################################
# Package: RichFutureRecTrackAlgorithms
################################################################################
gaudi_subdir(RichFutureRecTrackAlgorithms v1r0)

gaudi_depends_on_subdirs(Rich/RichFutureRecBase
                         Event/TrackEvent
                         Rich/RichUtils
                         Rich/RichFutureUtils
                         Tr/TrackKernel
                         Tr/TrackInterfaces)

find_package(ROOT)
find_package(Boost)
find_package(GSL)
find_package(VDT)
find_package(Vc)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS}
                           ${Vc_INCLUDE_DIR})

gaudi_add_module(RichFutureRecTrackAlgorithms
                 src/*.cpp
                 INCLUDE_DIRS Boost GSL VDT Rich/RichFutureRecBase Rich/RichUtils Rich/RichFutureUtils Tr/TrackInterfaces Tr/TrackKernel
                 LINK_LIBRARIES Boost GSL VDT RichFutureRecBase RichUtils RichFutureUtils )

# Fixes for GCC7.
if( BINARY_TAG_COMP_NAME STREQUAL "gcc" AND BINARY_TAG_COMP_VERSION VERSION_GREATER "6.99")
  set_property(TARGET RichFutureRecTrackAlgorithms APPEND PROPERTY COMPILE_FLAGS " -faligned-new ")
endif()
