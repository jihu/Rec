
//---------------------------------------------------------------------------------
/** @file RichBaseTrSegMaker.cpp
 *
 * Implementation file for class : Rich::Future::Rec::BaseTrSegMaker
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 14/01/2002
 */
//---------------------------------------------------------------------------------

// local
#include "RichBaseTrSegMaker.h"

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//-------------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
BaseTrSegMaker::BaseTrSegMaker( const std::string &name, ISvcLocator *pSvcLocator )
  : AlgBase( name, pSvcLocator )
{
  // initialise
  m_deBeam.fill( nullptr );
}

//=============================================================================
// Initialisation.
//=============================================================================
StatusCode
BaseTrSegMaker::initialize()
{
  // Sets up various tools and services
  const StatusCode sc = AlgBase::initialize();
  if ( sc.isFailure() ) return sc;

  if ( usedRads( Rich::Aerogel ) )
  { return Error( "Aerogel not supported in the 'RichFuture' framework" ); }
  if ( !usedRads( Rich::Rich1Gas ) )
  { Warning( "Track segments for Rich1Gas are disabled", StatusCode::SUCCESS ).ignore(); }
  if ( !usedRads( Rich::Rich2Gas ) )
  { Warning( "Track segments for Rich2Gas are disabled", StatusCode::SUCCESS ).ignore(); }

  _ri_debug << "Beam pipe intersection test (Aero/R1Gas/R2Gas) = " << m_checkBeamP << endmsg;
  for ( const auto rad : { Rich::Rich1Gas, Rich::Rich2Gas } )
  {
    if ( m_checkBeamP[rad] )
    {
      const auto rich = ( Rich::Rich2Gas == rad ? Rich::Rich2 : Rich::Rich1 );
      m_deBeam[rich]  = getDet< DeRichBeamPipe >(
        Rich::Rich1 == rich ? DeRichLocations::Rich1BeamPipe : DeRichLocations::Rich2BeamPipe );
    }
  }

  if ( m_checkBeamP[Rich::Rich1Gas] ) { deBeam( Rich::Rich1Gas ); }
  if ( m_checkBeamP[Rich::Rich2Gas] ) { deBeam( Rich::Rich2Gas ); }

  return sc;
}
