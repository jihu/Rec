
#pragma once

// STL
#include <array>
#include <cassert>
#include <tuple>
#include <vector>

// Gaudi
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/PhysicalConstants.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Kernel
#include "Kernel/RichDetectorType.h"

// Event Model
#include "RichFutureRecEvent/RichRecMassHypoRings.h"
#include "RichFutureRecEvent/RichRecPhotonYields.h"

// Utils
#include "RichUtils/FastMaths.h"
#include "RichUtils/RichMap.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/StlArray.h"
#include "RichUtils/ZipRange.h"

// RichDet
#include "RichDet/DeRich1.h"
#include "RichDet/DeRich2.h"
#include "RichDet/DeRichPD.h"
#include "RichDet/DeRichSphMirror.h"

namespace Rich::Future::Rec
{

  // Use the functional framework
  using namespace Gaudi::Functional;

  namespace
  {
    /// Output data type
    using OutData = std::tuple< PhotonYields::Vector, PhotonSpectra::Vector >;
  } // namespace

  /** @class DetectablePhotonYields RichDetectablePhotonYields.h
   *
   *  Computes the emitted photon yield data from Track Segments.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class DetectablePhotonYields final
    : public MultiTransformer< OutData( const LHCb::RichTrackSegment::Vector &,
                                        const PhotonSpectra::Vector &,
                                        const MassHypoRingsVector & ),
                               Traits::BaseClass_t< AlgBase > >
  {

  public:

    /// Standard constructor
    DetectablePhotonYields( const std::string &name, ISvcLocator *pSvcLocator );

    /// Initialization after creation
    StatusCode initialize() override;

  public:

    /// Algorithm execution via transform
    OutData operator()( const LHCb::RichTrackSegment::Vector &segments,
                        const PhotonSpectra::Vector &         emittedSpectra,
                        const MassHypoRingsVector &           massRings ) const override;

  private: // definitions

    /// Utility class to count entry for a given entity.
    template < typename TYPE >
    class Count final : public std::vector< std::pair< TYPE *, std::size_t > >
    {

    public: // definitions

      /// The Data type
      using Data = std::pair< TYPE *, std::size_t >;

    public:

      /// Count the given object
      inline void count( TYPE *p ) noexcept
      {
        // Is this the same object as last time ?
        if ( p == m_last ) { ++( m_data->second ); }
        // need to do a search
        else
        {
          // try and find the entry for this object
          auto it = std::find_if(
            this->begin(), this->end(), [&p]( const auto &i ) { return i.first == p; } );
          // if not preset add a new count of one
          if ( it == this->end() )
          {
            this->emplace_back( p, 1 );
            m_data = &( this->back() );
          }
          // increment existing count
          else
          {
            ++( it->second );
            m_data = &*it;
          }
          // update 'last' pointer cache
          m_last = p;
        }
      }

      /// Reset
      inline void reset() noexcept
      {
        // clear the vector
        this->clear();
        // reset the cache pointers
        m_last = nullptr;
        m_data = nullptr;
      }

    private:

      /// The last object added to the count
      TYPE *m_last = nullptr;

      /// Pointer to the data count for the last object added
      Data *m_data = nullptr;
    };

    /// Type for count by photon detector
    using PDCount = Count< const DeRichPD >;

    /// Type for count by mirror segment
    using MirrorCount = Count< const DeRichSphMirror >;

  private: // data

    /// Pointers to RICHes
    DetectorArray< const DeRich * > m_riches = { {} };

    /// Cached value storing product of quartz window eff. and digitisation pedestal loss
    float m_qEffPedLoss { 0 };

    /// Thickness of windows in each RICH
    DetectorArray< float > m_qWinZSize = { {} };
  };

} // namespace Rich::Future::Rec
