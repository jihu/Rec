
#pragma once

// STL
#include <tuple>

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Base class
#include "RichFutureKernel/RichAlgBase.h"

// Event model
#include "Event/Track.h"

namespace Rich::Future::Rec
{

  // Use the functional framework
  using namespace Gaudi::Functional;

  namespace
  {
    /// Output data type
    using OutData = std::tuple< LHCb::Track::Selection,   // Long
                                LHCb::Track::Selection,   // Downstream
                                LHCb::Track::Selection >; // Upstream
  }                                                       // namespace

  /** @class TrackFilter RichTrackFilter.h
   *
   *  (Temporary) algorithm that takes an input track location and splits
   *  it in shared containers by type.
   *
   *  Stop gap solution until a more general LoKiFunctor implementation is available.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class TrackFilter final
    : public MultiTransformer< OutData( const LHCb::Tracks & ), Traits::BaseClass_t< AlgBase > >
  {

  public:

    /// Standard constructor
    TrackFilter( const std::string &name, ISvcLocator *pSvcLocator );

  public:

    /// Algorithm execution via transform
    OutData operator()( const LHCb::Tracks &tracks ) const override;
  };

} // namespace Rich::Future::Rec
