
#pragma once

// STD
#include <algorithm>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiUtils/Aida2ROOT.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Event model
#include "Event/MCRichDigitSummary.h"

// Relations
#include "RichFutureMCUtils/RichRecMCHelper.h"

// FPE exception protection
#include "Kernel/FPEGuard.h"

namespace Rich::Future::Rec::MC::Moni
{

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class SIMDPhotonCherenkovAngles RichSIMDPhotonCherenkovAngles.h
   *
   *  Monitors the reconstructed cherenkov angles.
   *
   *  @author Chris Jones
   *  @date   2016-12-12
   */

  class SIMDPhotonCherenkovAngles final
    : public Consumer< void( const Summary::Track::Vector &,
                             const LHCb::Track::Selection &,
                             const SIMDPixelSummaries &,
                             const Rich::PDPixelCluster::Vector &,
                             const Relations::PhotonToParents::Vector &,
                             const LHCb::RichTrackSegment::Vector &,
                             const CherenkovAngles::Vector &,
                             const SIMDCherenkovPhoton::Vector &,
                             const Rich::Future::MC::Relations::TkToMCPRels &,
                             const LHCb::MCRichDigitSummarys & ),
                       Traits::BaseClass_t< HistoAlgBase > >
  {

  public:

    /// Standard constructor
    SIMDPhotonCherenkovAngles( const std::string &name, ISvcLocator *pSvcLocator );

  public:

    /// Functional operator
    void operator()( const Summary::Track::Vector &                  sumTracks,
                     const LHCb::Track::Selection &                  tracks,
                     const SIMDPixelSummaries &                      pixels,
                     const Rich::PDPixelCluster::Vector &            clusters,
                     const Relations::PhotonToParents::Vector &      photToSegPix,
                     const LHCb::RichTrackSegment::Vector &          segments,
                     const CherenkovAngles::Vector &                 expTkCKThetas,
                     const SIMDCherenkovPhoton::Vector &             photons,
                     const Rich::Future::MC::Relations::TkToMCPRels &tkrels,
                     const LHCb::MCRichDigitSummarys &               digitSums ) const override;

  protected:

    /// Pre-Book all histograms
    StatusCode prebookHistograms() override;

  private:

    /// Which radiators to monitor
    Gaudi::Property< RadiatorArray< bool > > m_rads { this, "Radiators", { false, true, true } };

    /// minimum beta value for tracks
    Gaudi::Property< RadiatorArray< float > > m_minBeta { this,
                                                          "MinBeta",
                                                          { 0.9999f, 0.9999f, 0.9999f } };

    /// maximum beta value for tracks
    Gaudi::Property< RadiatorArray< float > > m_maxBeta { this,
                                                          "MaxBeta",
                                                          { 999.99f, 999.99f, 999.99f } };

    /// Min theta limit for histos for each rad
    Gaudi::Property< RadiatorArray< float > > m_ckThetaMin { this,
                                                             "ChThetaRecHistoLimitMin",
                                                             { 0.150f, 0.030f, 0.010f } };

    /// Max theta limit for histos for each rad
    Gaudi::Property< RadiatorArray< float > > m_ckThetaMax { this,
                                                             "ChThetaRecHistoLimitMax",
                                                             { 0.325f, 0.060f, 0.036f } };

    /// Histogram ranges for CK resolution plots
    Gaudi::Property< RadiatorArray< float > > m_ckResRange { this,
                                                             "CKResHistoRange",
                                                             { 0.025f, 0.005f, 0.0025f } };

    /// The minimum track momentum (MeV/c)
    Gaudi::Property< float > m_minP { this,
                                      "MinP",
                                      float( 2 * Gaudi::Units::GeV ),
                                      "Minimum track momentum" };

    /// The maximum track momentum (MeV/c)
    Gaudi::Property< float > m_maxP { this,
                                      "MaxP",
                                      float( 100 * Gaudi::Units::GeV ),
                                      "Maximum track momentum" };

    /// Option to skip electrons
    Gaudi::Property< bool > m_skipElectrons { this,
                                              "SkipElectrons",
                                              false,
                                              "Skip eletrons from monitoring" };
  };

} // namespace Rich::Future::Rec::MC::Moni
