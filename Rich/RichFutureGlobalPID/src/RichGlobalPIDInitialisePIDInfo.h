
#pragma once

// STL
#include <tuple>

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Rec Event Model
#include "RichFutureRecEvent/RichRecTrackPIDInfo.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

namespace Rich::Future::Rec::GlobalPID
{

  // Use the functional framework
  using namespace Gaudi::Functional;

  namespace
  {
    /// Type for output data
    using OutData = std::tuple< TrackPIDHypos, TrackDLLs::Vector >;
  } // namespace

  /** @class InitialisePIDInfo RichGlobalPIDInitialisePIDInfo.h
   *
   *  Initialises the PID information for a given set of tracks
   *
   *  @author Chris Jones
   *  @date   2016-10-25
   */

  class InitialisePIDInfo final
    : public MultiTransformer< OutData( const Summary::Track::Vector & ),
                               Traits::BaseClass_t< AlgBase > >
  {

  public:

    /// Standard constructor
    InitialisePIDInfo( const std::string &name, ISvcLocator *pSvcLocator );

  public:

    /// Functional operator
    OutData operator()( const Summary::Track::Vector &gTracks ) const override;
  };

} // namespace Rich::Future::Rec::GlobalPID
