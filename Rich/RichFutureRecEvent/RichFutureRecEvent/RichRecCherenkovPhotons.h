
#pragma once

// STL
#include <string>

#include "RichFutureUtils/RichGeomPhoton.h"
#include "RichFutureUtils/RichSIMDGeomPhoton.h"

namespace Rich::Future::Rec
{

  /** @class CherenkovPhoton RichFutureRecEvent/RichRecCherenkovPhotons.h
   *
   *  A scalar reconstructed photon object.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  using CherenkovPhoton = Rich::Future::RecoPhoton;

  /// TES locations
  namespace CherenkovPhotonLocation
  {
    /// Default Location in TES for the scalar photons
    static const std::string Default = "Rec/RichFuture/CherenkovPhotons/Default";
  } // namespace CherenkovPhotonLocation

  /** @class SIMDCherenkovPhoton RichFutureRecEvent/RichRecCherenkovPhotons.h
   *
   *  A SIMD vectorised reconstructed photon object.
   *
   *  @author Chris Jones
   *  @date   2017-10-13
   */
  using SIMDCherenkovPhoton = Rich::SIMD::Future::RecoPhoton;

  /// TES locations
  namespace SIMDCherenkovPhotonLocation
  {
    /// Default Location in TES for the SIMD photons
    static const std::string Default = "Rec/RichFuture/SIMDCherenkovPhotons/Default";
  } // namespace SIMDCherenkovPhotonLocation

} // namespace Rich::Future::Rec
