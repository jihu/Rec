
#pragma once

// STL
#include <vector>

// Utils
#include "RichUtils/RichSIMDTypes.h"

namespace Rich::Future::Rec
{

  /// Type for pixel background values (Scalar)
  using PixelBackgrounds = std::vector< double >;

  /// TES locations
  namespace PixelBackgroundsLocation
  {
    /// Default TES location for pixel backgrounds
    static const std::string Default = "Rec/RichFuture/PixelBackgrounds/Default";
  } // namespace PixelBackgroundsLocation

  /// Type for pixel background values (SIMD)
  using SIMDPixelBackgrounds = SIMD::STDVector< Rich::SIMD::FP< Rich::SIMD::DefaultScalarFP > >;

  /// TES locations
  namespace SIMDPixelBackgroundsLocation
  {
    /// Default TES location for pixel backgrounds
    static const std::string Default = "Rec/RichFuture/SIMDPixelBackgrounds/Default";
  } // namespace SIMDPixelBackgroundsLocation

} // namespace Rich::Future::Rec
