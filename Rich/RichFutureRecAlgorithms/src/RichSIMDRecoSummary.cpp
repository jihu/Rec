
// local
#include "RichSIMDRecoSummary.h"

using namespace Rich::Future::Rec;

//-----------------------------------------------------------------------------
// Implementation file for class : RichGlobalPIDRecoSummary
//
// 2016-10-25 : Chris Jones
//-----------------------------------------------------------------------------

SIMDRecoSummary::SIMDRecoSummary( const std::string &name, ISvcLocator *pSvcLocator )
  : MultiTransformer(
      name,
      pSvcLocator,
      { KeyValue { "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
        KeyValue { "TrackToSegmentsLocation", Relations::TrackToSegmentsLocation::Selected },
        KeyValue { "PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default },
        KeyValue { "DetectablePhotonYieldLocation", PhotonYieldsLocation::Detectable },
        KeyValue { "SignalPhotonYieldLocation", PhotonYieldsLocation::Signal },
        KeyValue { "PhotonSignalsLocation", SIMDPhotonSignalsLocation::Default },
        KeyValue { "RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default } },
      { KeyValue { "SummaryTracksLocation", Summary::TESLocations::Tracks },
        KeyValue { "SummaryPixelsLocation", Summary::TESLocations::Pixels } } )
{
  // debug output
  // setProperty( "OutputLevel", MSG::VERBOSE );
}

//-----------------------------------------------------------------------------

OutData
SIMDRecoSummary::operator()( const LHCb::RichTrackSegment::Vector &    segments,
                             const Relations::TrackToSegments::Vector &tkToSegs,
                             const Relations::PhotonToParents::Vector &photToSegPix,
                             const PhotonYields::Vector &              detYields,
                             const PhotonYields::Vector &              sigYields,
                             const SIMDPhotonSignals::Vector &         expPhotSigs,
                             const SIMDPixelSummaries &                pixels ) const
{
  // the data to return
  OutData oData;

  // get short cuts to the two containers in the tuple
  auto &gTracks = std::get< Summary::Track::Vector >( oData );
  auto &gPixels = std::get< Summary::Pixel::Vector >( oData );

  // flag if we had some tracks and some clusters to process
  const bool eventOK = !segments.empty() && !pixels.empty();

  // reserve size in the track container
  gTracks.reserve( tkToSegs.size() );

  // fill pixel vector with one entry per cluster
  gPixels = Summary::Pixel::Vector( pixels.size() );

  // start iterator in the photon->(pixel,segment) relations
  auto photRels = photToSegPix.begin();

  // Loop over track to segment relations
  Summary::Track::Vector::size_type tkIndex( 0 );
  for ( const auto &tkrel : tkToSegs )
  {
    // create a new GPID track
    // must always do this, so there is an exact 1 to 1 with the input tracks
    gTracks.emplace_back( tkrel.tkKey, tkIndex++ );
    auto &gtk = gTracks.back();

    // if event is OK continue
    if ( eventOK )
    {

      // Do we have any segments for this track
      if ( !tkrel.segmentIndices.empty() )
      {

        // Loop over (real) mass hypos
        for ( const auto hypo : activeParticlesNoBT() )
        {

          // threshold flag for this hypo, checking all segments
          const auto above = std::any_of(
            tkrel.segmentIndices.begin(),
            tkrel.segmentIndices.end(),
            [&detYields, &hypo]( const auto iSeg ) { return ( detYields[iSeg] )[hypo] > 0; } );
          gtk.thresholds().setData( hypo, above );
          // if above, set overall track active flag to true
          if ( above ) { gtk.setActive( true ); }

          // if below, abort checking other (heavier hypos)
          // if ( !above ) break;

          // Total observable photons
          const auto signal =
            std::accumulate( tkrel.segmentIndices.begin(),
                             tkrel.segmentIndices.end(),
                             0.0f,
                             [&sigYields, &hypo]( const auto sum, const auto iSeg ) {
                               return sum + ( sigYields[iSeg] )[hypo];
                             } );
          gtk.totalSignals().setData( hypo, signal );

        } // hypo loop

        // set the radiator flags
        std::for_each( tkrel.segmentIndices.begin(),
                       tkrel.segmentIndices.end(),
                       [&gtk, &segments]( const auto iSeg ) {
                         gtk.radiatorActive()[segments[iSeg].radiator()] = true;
                       } );

        // if active, find photons
        if ( gtk.active() )
        {
          // Loop over the segments
          for ( const auto fSeg : tkrel.segmentIndices )
          {

            // Iterate over the photon relations until we find the first photon for this
            // segment for this track.
            // Relies on the fact data objects are all produced in order,
            // so all the segments for a given track, or photons for a given segment
            // are consecutive.
            const auto photRelsFirst =
              std::find_if( photRels, photToSegPix.end(), [&fSeg]( const auto &p ) {
                return p.segmentIndex() == fSeg;
              } );
            // .. and then find the last one
            const auto photRelsLast =
              std::find_if( photRelsFirst, photToSegPix.end(), [&fSeg]( const auto &p ) {
                return p.segmentIndex() != fSeg;
              } );

            // Number of photons for this segment
            const auto nSegPhots = photRelsLast - photRelsFirst;

            // extend reserved size for the new photons
            gtk.photonIndices().reserve( gtk.photonIndices().size() + nSegPhots );

            // loop over the found photons and fill track photon vector
            for ( auto iS = photRelsFirst; iS != photRelsLast; ++iS )
            {
              // photon index
              const auto iPhot = iS->photonIndex();
              // Get the pixel signals for this photon
              const auto &sigs = expPhotSigs[iPhot];
              // Check photon has a decent signal prob
              if ( std::any_of(
                     activeParticlesNoBT().begin(),
                     activeParticlesNoBT().end(),
                     [&sigs]( const auto id ) { return any_of( sigs[id] > SIMDFP::Zero() ); } ) )
              {
                // Save this photon in the track photon list
                gtk.photonIndices().push_back( iPhot );
                // The pixel photon list
                auto &pixL = gPixels[iS->pixelIndex()].photonIndices();
                // If first time for this pixel, reserve a reasonable average size
                if ( pixL.empty() ) { pixL.reserve( 16 / SIMDFP::Size ); }
                // Add to the list
                pixL.push_back( iPhot );
              }
            }

            // set the start segment iterator for the next segment to the last one
            // if not the end of the container ( as this means search above failed )
            if ( photRelsLast != photToSegPix.end() ) { photRels = photRelsLast; }

            // Set the active RICH flags
            gtk.richActive()[segments[fSeg].rich()] = true;

          } // segment loop

        } // is active

      } // has segments

    } // event OK

  } // loop over tracks

  return oData;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDRecoSummary )

//=============================================================================
