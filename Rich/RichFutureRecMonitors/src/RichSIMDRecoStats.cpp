
// local
#include "RichSIMDRecoStats.h"

using namespace Rich::Future::Rec::Moni;

//-----------------------------------------------------------------------------
// Implementation file for class : SIMDRecoStats
//
// 2016-11-07 : Chris Jones
//-----------------------------------------------------------------------------

SIMDRecoStats::SIMDRecoStats( const std::string &name, ISvcLocator *pSvcLocator )
  : Consumer(
      name,
      pSvcLocator,
      { KeyValue { "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
        KeyValue { "TrackToSegmentsLocation", Relations::TrackToSegmentsLocation::Selected },
        KeyValue { "CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default } } )
{
  // debugging
  //setProperty( "OutputLevel", MSG::VERBOSE );
}

//-----------------------------------------------------------------------------

void
SIMDRecoStats::operator()( const LHCb::RichTrackSegment::Vector &    segments,
                           const Relations::TrackToSegments::Vector &tkToSegs,
                           const SIMDCherenkovPhoton::Vector &       photons ) const
{

  DetectorArray< unsigned long long > photCount { {} };
  DetectorArray< unsigned long long > segCount { {} };
  unsigned long long                  tkCount { 0 };

  // Loop over the track data
  for ( const auto &rels : tkToSegs )
  {
    // Was this track selected. i.e. does it have at least one segment
    const bool selected = !rels.segmentIndices.empty();
    // track selection efficiency
    m_richEff += selected;
    // is selected ?
    if ( selected ) 
    {
      // count selected tracks
      ++tkCount;
      // loop over segments for this track
      for ( const auto &iSeg : rels.segmentIndices )
      {
        // get the segment
        const auto &seg = segments[iSeg];
        // count segments
        ++segCount[seg.rich()];
      }
    }
  }

  // Loop over photons
  for ( const auto &phot : photons )
  {
    // count scalar photons per rich
    photCount[phot.rich()] += phot.validityMask().count();
  }

  // update counts
  m_nTracks += tkCount;
  for ( auto rich : { Rich::Rich1, Rich::Rich2 } )
  {
    m_nSegs[rich] += segCount[rich];
    m_nPhots[rich] += photCount[rich];
  }
  
  // _ri_debug << "Photons " << photCount << endmsg;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDRecoStats )

//=============================================================================
