
// local
#include "RichDecodingErrors.h"

using namespace Rich::Future::Rec::Moni;

//-----------------------------------------------------------------------------
// Implementation file for class : DecodingErrors
//
// 2016-12-06 : Chris Jones
//-----------------------------------------------------------------------------

DecodingErrors::DecodingErrors( const std::string &name, ISvcLocator *pSvcLocator )
  : Consumer( name,
              pSvcLocator,
              { KeyValue { "DecodedDataLocation", Rich::Future::DAQ::L1MapLocation::Default },
                KeyValue { "OdinLocation", LHCb::ODINLocation::Default } } )
{}

//-----------------------------------------------------------------------------

StatusCode
DecodingErrors::initialize()
{
  // Initialise base class
  const auto sc = Consumer::initialize();
  if ( !sc ) return sc;

  // Load the RICH detector system
  m_richSys = getDet< DeRichSystem >( DeRichLocations::RichSystem );

  return sc;
}

//-----------------------------------------------------------------------------

const DecodingErrors::BinLabels &
DecodingErrors::labels()
{
  static const BinLabels labels = {
    "L1 Ingress Truncated", "ODIN/Ingress BXID MisMatch",   "HPD Inhibit",
    "HPD DB Lookup",        "Ingress/HPD EventID MisMatch", "Extended HPD Header"
  };
  return labels;
}

//-----------------------------------------------------------------------------

StatusCode
DecodingErrors::prebookHistograms()
{
  const auto  nlabels = labels().size();
  const auto &L1s     = m_richSys->level1HardwareIDs();
  const auto  nL1s    = L1s.size();

  BinLabels l1HardIDLabels( nL1s );
  for ( const auto &l1 : L1s )
  {
    const auto copyN             = m_richSys->copyNumber( l1 );
    l1HardIDLabels[copyN.data()] = ( std::string )( l1 );
    std::ostringstream id, title;
    id << "L1s/decodingErrors-L1hardID" << l1;
    title << "L1-HardwareID " << l1 << " DAQ Decoding Error Rates (%)";
    m_l1Histos[l1] = richProfile1D( HID( id.str() ),
                                    title.str(),
                                    0.5,
                                    labels().size() + 0.5,
                                    labels().size(),
                                    "DAQ Decoding Error Types",
                                    "Error Rate (%)",
                                    labels() );
  }

  m_l1CombH = richProfile1D( HID( "decodingErrors" ),
                             "DAQ Decoding Error Rates (%)",
                             0.5,
                             nlabels + 0.5,
                             nlabels,
                             "DAQ Decoding Error Types",
                             "Error Rate (%)",
                             labels() );

  m_2dErrors = richHisto2D( HID( "decodingErrorsByBoard" ),
                            "DAQ Decoding Errors by UKL1 Hardware ID",
                            -0.5,
                            nL1s - 0.5,
                            nL1s,
                            0.5,
                            nlabels + 0.5,
                            nlabels,
                            "UKL1 Hardware ID",
                            "",
                            "# of errors",
                            l1HardIDLabels,
                            labels() );

  return StatusCode::SUCCESS;
}

//-----------------------------------------------------------------------------

AIDA::IProfile1D *
DecodingErrors::getHisto( const int l1ID ) const
{
  AIDA::IProfile1D *histo( nullptr );
  if ( l1ID < 0 ) { histo = m_l1CombH; }
  else
  {
    auto iH = m_l1Histos.find( Rich::DAQ::Level1HardwareID( l1ID ) );
    if ( iH != m_l1Histos.end() ) { histo = iH->second; }
    else
    {
      std::ostringstream mess;
      mess << "Failed to find histogram for L1 Board " << l1ID;
      Warning( mess.str() ).ignore();
    }
  }
  return histo;
}

//-----------------------------------------------------------------------------

void
DecodingErrors::operator()( const Rich::Future::DAQ::L1Map &data, const LHCb::ODIN &odin ) const
{
  // Loop over L1 boards
  for ( const auto &L1 : data )
  {
    // All boards combined
    makePlots( L1.second, odin, -1 );
    // Each L1 board on its own
    makePlots( L1.second, odin, (int)L1.first.data() );
  }
}

//-----------------------------------------------------------------------------

void
DecodingErrors::makePlots( const Rich::Future::DAQ::IngressMap &inMap,
                           const LHCb::ODIN &                   odin,
                           const int                            l1ID ) const
{
  // The histos to fill
  auto *h1D = getHisto( l1ID );
  if ( !h1D ) return;
  auto *h2D = ( l1ID == -1 ? nullptr : m_2dErrors );

  // Get L1 Copy Number
  Rich::DAQ::Level1CopyNumber copyN;
  if ( l1ID != -1 )
  {
    const Rich::DAQ::Level1HardwareID hID( l1ID );
    copyN = m_richSys->copyNumber( hID );
  }

  // loop over ingresses for this L1 board
  for ( const auto &In : inMap )
  {
    const auto &ingressInfo   = In.second;
    const auto &ingressHeader = ingressInfo.ingressHeader();

    // Check if all HPDs are suppressed
    fillPlots( copyN, 1, ingressHeader.hpdsSuppressed(), h1D, h2D );

    // Check BX ID between Rich and ODIN
    const bool bxIDOK = Rich::DAQ::BXID( odin.bunchId() ) == ingressHeader.bxID();
    fillPlots( copyN, 2, !bxIDOK, h1D, h2D );

    // Loop over HPDs in this ingress
    for ( const auto &HPD : In.second.pdData() )
    {
      const bool inhibit = HPD.second.header().inhibit();
      // inhibited HPDs
      fillPlots( copyN, 3, inhibit, h1D, h2D );
      if ( !inhibit )
      {
        // Invalid HPD (BD lookup error)
        fillPlots( copyN, 4, !HPD.second.pdID().isValid(), h1D, h2D );
        // Event IDs
        const bool evtIDOK = ( ingressHeader.eventID() == HPD.second.header().eventID() );
        fillPlots( copyN, 5, !evtIDOK, h1D, h2D );
        // HPD header in extended mode
        fillPlots( copyN, 6, HPD.second.header().extendedFormat(), h1D, h2D );
      }
    } // loop over HPDs

  } // ingresses
}

//-----------------------------------------------------------------------------

void
DecodingErrors::fillPlots( const Rich::DAQ::Level1CopyNumber &copyN,
                           const int                          errorCode,
                           const bool                         error,
                           AIDA::IProfile1D *                 h1D,
                           AIDA::IHistogram2D *               h2D ) const
{
  if ( h1D ) { h1D->fill( errorCode, error ? 100.0 : 0.0 ); }
  if ( h2D && error ) { h2D->fill( copyN.data(), errorCode ); }
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( DecodingErrors )

//-----------------------------------------------------------------------------
