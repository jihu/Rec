#ifndef DICT_RICHRECUTILSDICT_H
#define DICT_RICHRECUTILSDICT_H 1

#include "RichRecUtils/RichCKResolutionFitter.h"
#include "RichRecUtils/RichRadCorrLocalPositions.h"

// instantiate some templated classes, to get them into the dictionary
namespace
{
  struct _Instantiations
  {
    Rich::Rec::CKResolutionFitter obj_1;
  };
} // namespace

#endif // DICT_RICHRECUTILSDICT_H
