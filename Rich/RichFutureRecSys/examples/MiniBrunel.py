
from Configurables import MiniBrunel
from Configurables import HLTControlFlowMgr, HLTEventLoopMgr, HiveDataBrokerSvc
from Gaudi.Configuration import *
import os.path

mbrunel = MiniBrunel()

#threads = 8
#mbrunel.EnableHive = True

threads = 1
mbrunel.EnableHive = False

mbrunel.EvtMax                = 50000
mbrunel.Scheduler             = "HLTControlFlowMgr"
mbrunel.RunChecks             = False
mbrunel.GECCut                = 11000
mbrunel.ThreadPoolSize        = threads
mbrunel.EventSlots            = threads*2

HLTControlFlowMgr().PrintFreq   = mbrunel.EvtMax/10 
#HLTControlFlowMgr().OutputLevel = DEBUG
#HLTEventLoopMgr().OutputLevel   = DEBUG
#HiveDataBrokerSvc().OutputLevel = DEBUG

mbrunel.RunRich          = False
mbrunel.CallgrindProfile = False
mbrunel.HLT1Only         = True
mbrunel.HLT1Fitter       = False
mbrunel.VeloOnly         = False
mbrunel.DisableTiming    = False
mbrunel.RunFastForwardFitter = False

# Fallback default is on EOS
datafile = 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/swtest/MiniBrunel/MC_Upg_Down_201710_43k.mdf'
# Possible local copies
datafiles = [ '/usera/jonesc/NFS/data/MC/Upgrade/MiniBrunelTests/MC_Upg_Down_201710_43k.mdf',
              '/group/rich/jonesc/LHCb/Data/MC/Upgrade/MiniBrunelTests/MC_Upg_Down_201710_43k.mdf' ]
for f in datafiles :
    if os.path.isfile(f) :
        datafile = f
print "Datafile", datafile

mbrunel.IgnoreChecksum   = True
mbrunel.HLT1ParamKalman  = True
mbrunel.TimelineFile = "test.csv"
mbrunel.HLT1BestPerf    = True
from PRConfig.TestFileDB import test_file_db
filedb = test_file_db['MiniBrunel_2018_MinBias_FTv2_MDF']
filedb.filenames = ['mdf:'+datafile]*10
filedb.run(configurable=mbrunel)

#from GaudiConf import IOHelper
#IOHelper

def fetch():
    from Configurables import GaudiSequencer
    from Configurables import Gaudi__Hive__FetchDataFromFile as FetchDataFromFile

    seq = GaudiSequencer("RecoDecodingSeq")

    from Configurables import createODIN
    odinDecode = createODIN( "ODINDecode" )
    odinDecode.ODIN = "/Event/DAQ/TempODIN"

    fetcher = FetchDataFromFile("DataFetch")
    fetcher.DataKeys = ['DAQ/RawEvent']
    fetcher.OutputLevel = 1

    ApplicationMgr().TopAlg.insert(0,odinDecode)
    ApplicationMgr().TopAlg.insert(0,fetcher)

    #seq.Members = [fetcher] + seq.Members
    
appendPostConfigAction(fetch)
