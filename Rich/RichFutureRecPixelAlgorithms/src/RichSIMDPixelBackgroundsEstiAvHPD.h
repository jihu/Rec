
#pragma once

// STL
#include <algorithm>
#include <cstdint>
#include <ostream>
#include <utility>
#include <vector>

// Gaudi
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiAlg/Transformer.h"

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Rec Event
#include "RichFutureRecEvent/RichRecGeomEfficiencies.h"
#include "RichFutureRecEvent/RichRecPhotonYields.h"
#include "RichFutureRecEvent/RichRecPixelBackgrounds.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"
#include "RichFutureRecEvent/RichRecTrackPIDInfo.h"

// Rich Utils
#include "RichUtils/RichMap.h"
#include "RichUtils/ZipRange.h"

// RicDet
#include "RichDet/DeRich1.h"
#include "RichDet/DeRichSystem.h"

namespace Rich::Future::Rec
{

  // Use the functional framework
  using namespace Gaudi::Functional;

  /** @class PixelBackgroundsEstiAvHPD RichPixelBackgroundsEstiAvHPD.h
   *
   *  Computes an estimate of the background contribution to each pixel
   *  given the set of track mass hypotheses.
   *
   *  Provisonally this is a SIMD version, acting on the SIMD pixels,
   *  but the algorithm (for now) is really still scalar. It is far
   *  from being a major CPU user in the overall RICH sequence...
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class SIMDPixelBackgroundsEstiAvHPD final
    : public Transformer< SIMDPixelBackgrounds( const Relations::TrackToSegments::Vector &,
                                                const TrackPIDHypos &,
                                                const LHCb::RichTrackSegment::Vector &,
                                                const GeomEffsPerPDVector &,
                                                const PhotonYields::Vector &,
                                                const SIMDPixelSummaries & ),
                          Traits::BaseClass_t< AlgBase > >
  {

  public:

    /// Standard constructor
    SIMDPixelBackgroundsEstiAvHPD( const std::string &name, ISvcLocator *pSvcLocator );

    /// Initialization after creation
    StatusCode initialize() override;

  public:

    /// Algorithm execution via transform
    SIMDPixelBackgrounds operator()( const Relations::TrackToSegments::Vector &tkToSegs,
                                     const TrackPIDHypos &                     tkHypos,
                                     const LHCb::RichTrackSegment::Vector &    segments,
                                     const GeomEffsPerPDVector &               geomEffsPerPD,
                                     const PhotonYields::Vector &              detYieldsV,
                                     const SIMDPixelSummaries &pixels ) const override;

  private: // definitions

    /// Scalar type for SIMD data
    using FP = SIMD::DefaultScalarFP;
    /// SIMD floating point type
    using SIMDFP = SIMD::FP< Rich::SIMD::DefaultScalarFP >;
    /// Type for SIMD copy numbers
    using SIMDPDCopyNumber = SIMD::INT< Rich::DAQ::PDCopyNumber::Type >;

    /// Data values for a single PD
    struct PDData
    {
      std::uint16_t obsSignal { 0 };        ///< Number of observed hits in this PD
      FP            expSignal { 0 };        ///< Expected signal in this PD
      FP            expBackgrd { 0 };       ///< Expected background in this PD
      FP            effNumPixs { 0 };       ///< Effective number of pixels
      using Vector = std::vector< PDData >; ///< Vector type
    };

  private:

    /// Per Rich Data Structure
    using RichPDData = DetectorArray< PanelArray< PDData::Vector > >;

    /// Access the static data structure
    const RichPDData &getPDData() const;

  private: // methods

    /// Gets the per RICH index for the given PD ID
    inline decltype( auto ) pdCopyNumber( const LHCb::RichSmartID pdID ) const
    {
      // Panel index
      return m_richSys->dePDPanel( pdID )->pdNumber( pdID );
    }

    /// Gets the working data index for the given PD ID
    inline decltype( auto ) pdIndex( const LHCb::RichSmartID  pdID,
                                     const Rich::DetectorType rich ) const noexcept
    {
      // NB : This is a poor mans attempt to group the PDs together, using
      //      their copy numbers. This will be correlated to grouping spatially,
      //      but is not perfect. To be addressed once a better PD numbering
      //      scheme is available from RichDet.
      return ( pdCopyNumber( pdID ).data() / m_pdGroupSize[rich] );
    }

    /// Get the max Index value for each panel
    inline decltype( auto ) maxPdIndex( const Rich::DetectorType rich, const Rich::Side side ) const
    {
      // Need to add one to handle fact for MaPMTs numbering scheme starts at 1...
      return ( m_richSys->dePDPanel( rich, side )->maxPdNumber().data() + 1 );
    }

    /// Get the DePD object
    inline decltype( auto ) dePD( const Rich::DetectorType      rich,
                                  const Rich::Side              side,
                                  const Rich::DAQ::PDPanelIndex copyN ) const
    {
      return m_richSys->dePDPanel( rich, side )->dePD( copyN );
    }

    /// Get the DePD object
    inline decltype( auto ) dePD( const LHCb::RichSmartID       pdID,
                                  const Rich::DAQ::PDPanelIndex copyN ) const noexcept
    {
      return dePD( pdID.rich(), pdID.panel(), copyN );
    }

  private: // data

    /// Maximum number of iterations in background normalisation
    Gaudi::Property< unsigned int > m_maxBkgIterations {
      this,
      "MaxBackgroundNormIterations",
      10,
      "Maximum number of iterations in background normalisation"
    };

    /// Minimum pixel background value, for each RICH
    Gaudi::Property< DetectorArray< FP > > m_minPixBkg { this,
                                                         "MinPixelBackground",
                                                         { 0.0f, 0.0f },
                                                         "Minimum pixel background for each RICH" };

    /// Maximum pixel background value, for each RICH
    Gaudi::Property< DetectorArray< FP > > m_maxPixBkg { this,
                                                         "MaxPixelBackground",
                                                         { 9e9f, 9e9f },
                                                         "Maximum pixel background for each RICH" };

    /** Ignore the expected signal when computing the background terms.
        Effectively, will assume all observed hits are background */
    Gaudi::Property< bool > m_ignoreExpSignal {
      this,
      "IgnoreExpectedSignals",
      false,
      "Ignore expectations when calculating backgrounds"
    };

    /// Background 'weight' for each RICH
    Gaudi::Property< DetectorArray< float > > m_bkgWeight {
      this,
      "PDBckWeights",
      { 1.0f, 1.0f },
      "Weights to apply to the background terms for each RICH"
    };

    /// PD Group Size for each RICH
    Gaudi::Property< DetectorArray< unsigned int > > m_pdGroupSize {
      this,
      "PDGroupSize",
      { 1u, 1u },
      "The number of PDs to group together for the background calculation"
    };

    /// Rich System detector element
    const DeRichSystem *m_richSys = nullptr;
  };

} // namespace Rich::Future::Rec
