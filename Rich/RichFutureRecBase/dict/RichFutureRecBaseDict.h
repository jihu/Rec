#ifndef DICT_RICHRECBASEDICT_H
#define DICT_RICHRECBASEDICT_H 1

#include "RichRecUtils/RichRadCorrLocalPositions.h"
#include "RichRecUtils/RichRecPhotonKey.h"

// instantiate some templated classes, to get them into the dictionary
namespace
{
  struct _Instantiations
  {
    // nothing to do
  };
} // namespace

#endif // DICT_RICHRECBASEDICT_H
