
//-----------------------------------------------------------------------------
/** @file RichRecBase.h
 *
 *  Header file for RICH reconstruction base class : Rich::Rec::CommonBase
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   2005-08-26
 */
//-----------------------------------------------------------------------------

#pragma once

// Gaudi
#include "GaudiKernel/StatusCode.h"

// Interfaces
#include "RichInterfaces/IRichParticleProperties.h"

namespace Rich::Future::Rec
{

  //-----------------------------------------------------------------------------
  /** @class CommonBase RichRecBase.h RichRecBase/RichRecBase.h
   *
   *  Base class containing common RICH reconstruction functionality
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2005-08-26
   */
  //-----------------------------------------------------------------------------

  template < class PBASE >
  class CommonBase
  {

  public:

    /// Standard constructor
    CommonBase( PBASE *base = nullptr );

  protected:

    /// Intialise common RICH Reco
    StatusCode initialiseRichReco();

    /// Finalise common RICH Reco
    StatusCode finaliseRichReco();

  private:

    /// Const access to derived class
    inline const PBASE *base() const noexcept { return m_base; }

    /// Non-const access to derived class
    inline PBASE *base() noexcept { return m_base; }

  protected:

    /// Access the particle properties tool
    inline const IParticleProperties *richPartProps() const noexcept
    {
      return m_richPartProp.get();
    }

    /// Access the list of all active Particle ID types to consider (including below threshold)
    inline const Rich::Particles &activeParticles() const noexcept { return m_pidTypes; }

    /// Access the list of active Particle ID types to consider (excluding below threshold)
    inline const Rich::Particles &activeParticlesNoBT() const noexcept { return m_pidTypesNoBT; }

    /// The lightest active mass hypothesis
    inline Rich::ParticleIDType lightestActiveHypo() const noexcept { return m_pidTypes.front(); }

    /// The heaviest active mass hypothesis
    inline Rich::ParticleIDType heaviestActiveHypo() const noexcept { return m_pidTypes.back(); }

  private:

    /// Real particle ID types to consider (excluding below threshold)
    Rich::Particles m_pidTypesNoBT;

    /// All particle ID types to consider (including below threshold)
    Rich::Particles m_pidTypes;

    /// Pointer to derived class
    PBASE *m_base = nullptr;

    /// Pointer to RichParticleProperties interface
    ToolHandle< const IParticleProperties > m_richPartProp;
  };

} // namespace Rich::Future::Rec
