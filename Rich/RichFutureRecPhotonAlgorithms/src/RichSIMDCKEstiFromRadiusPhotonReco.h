
#pragma once

// Array properties
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiKernel/ParsersFactory.h"

// Gaudi
#include "GaudiAlg/Transformer.h"

// Base class
#include "RichBasePhotonReco.h"

// Event Model
#include "RichFutureRecEvent/RichRecMassHypoRings.h"

// Rec Utils
#include "RichRecUtils/RichRadCorrLocalPositions.h"

// LHCbMath
#include "LHCbMath/FastMaths.h"

namespace Rich::Future::Rec
{

  // Use the functional framework
  using namespace Gaudi::Functional;

  namespace
  {
    /// Shortcut to the output data type
    using OutData = std::tuple< SIMDCherenkovPhoton::Vector, Relations::PhotonToParents::Vector >;
  } // namespace

  /** @class CKEstiFromRadiusPhotonReco RichCKEstiFromRadiusPhotonReco.h
   *
   *  Reconstructs photon candidates using the Quartic solution.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */

  class SIMDCKEstiFromRadiusPhotonReco final
    : public MultiTransformer< OutData( const LHCb::RichTrackSegment::Vector &,
                                        const CherenkovAngles::Vector &,
                                        const CherenkovResolutions::Vector &,
                                        const SegmentPanelSpacePoints::Vector &,
                                        const MassHypoRingsVector &,
                                        const SegmentPhotonFlags::Vector &,
                                        const SIMDPixelSummaries &,
                                        const Relations::TrackToSegments::Vector & ),
                               Traits::BaseClass_t< BasePhotonReco > >
  {

  private:

    /// Basic precision (float)
    using FP = SIMDCherenkovPhoton::FP;
    /// SIMD version of FP
    using SIMDFP = SIMDCherenkovPhoton::SIMDFP;

  public:

    /// Standard constructor
    SIMDCKEstiFromRadiusPhotonReco( const std::string &name, ISvcLocator *pSvcLocator );

  public:

    /// Functional operator
    OutData operator()( const LHCb::RichTrackSegment::Vector &    segments,
                        const CherenkovAngles::Vector &           ckAngles,
                        const CherenkovResolutions::Vector &      ckResolutions,
                        const SegmentPanelSpacePoints::Vector &   trHitPntsLoc,
                        const MassHypoRingsVector &               massRings,
                        const SegmentPhotonFlags::Vector &        segPhotFlags,
                        const SIMDPixelSummaries &                pixels,
                        const Relations::TrackToSegments::Vector &tkToSegRels ) const override;

  private:

    /// Flag to turn on interpolation between two nearest rings, by radiator
    Gaudi::Property< RadiatorArray< bool > > m_useRingInterp { this,
                                                               "UseRingInterpolation",
                                                               { true, true, true } };

    /// Flag to turn on the rejection of 'ambiguous' photons
    Gaudi::Property< bool > m_rejAmbigPhots { this, "RejectAmbiguousPhotons", false };
  };

} // namespace Rich::Future::Rec
