// Include files
#include "CaloChi22ID.h"
#include "ToVector.h"

// ============================================================================
using TABLEI = LHCb::RelationWeighted2D<LHCb::CaloCluster, LHCb::Track, float>;
using TABLEO = LHCb::Relation1D<LHCb::Track, float>;

struct ClusChi22ID final: public CaloChi22ID<TABLEI,TABLEO> {
  static_assert(
      std::is_base_of<LHCb::Calo2Track::IClusTrTable2D, TABLEI>::value,
      "TABLEI must inherit from IClusTrTable2D");

  ClusChi22ID(const std::string& name, ISvcLocator* pSvc)
      : CaloChi22ID<TABLEI,TABLEO>(name, pSvc) {
    using LHCb::CaloAlgUtils::CaloIdLocation;
    Gaudi::Functional::updateHandleLocation(*this,  "Input", CaloIdLocation("ClusterMatch", context()));
    Gaudi::Functional::updateHandleLocation(*this, "Output", CaloIdLocation("ClusChi2", context()));
    // @todo it must be in agrement with "Threshold" for PhotonMatchAlg
    _setProperty("CutOff", "1000");  //
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Long, LHCb::Track::Types::Ttrack,
                                     LHCb::Track::Types::Downstream));
  };
};
// ============================================================================

DECLARE_COMPONENT( ClusChi22ID )
