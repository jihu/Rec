// Include files
#include "CaloChi22ID.h"
#include "ToVector.h"

// ============================================================================
/** @class EcalChi22ID EcalChi22ID.cpp
 *  The preconfigured instance of class CaloChi22ID
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================
using TABLEI = LHCb::RelationWeighted2D<LHCb::CaloHypo, LHCb::Track, float>;
using TABLEO = LHCb::Relation1D<LHCb::Track, float>;

struct EcalChi22ID final : public CaloChi22ID<TABLEI,TABLEO> {
  static_assert(
      std::is_base_of<LHCb::Calo2Track::IHypoTrTable2D, TABLEI>::value,
      "TABLEI must inherit from IHypoTrTable2D");
  EcalChi22ID(const std::string& name, ISvcLocator* pSvc)
    : CaloChi22ID<TABLEI,TABLEO>(name, pSvc) {
    using LHCb::CaloAlgUtils::CaloIdLocation;
    Gaudi::Functional::updateHandleLocation(*this,  "Input", CaloIdLocation("ElectronMatch", context()));
    Gaudi::Functional::updateHandleLocation(*this, "Output", CaloIdLocation("EcalChi2", context()));
    // @todo it must be in agrement with "Threshold" for ElectonMatchAlg
    _setProperty("CutOff", "10000");
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                   LHCb::Track::Types::Long, LHCb::Track::Types::Ttrack,
                                   LHCb::Track::Types::Downstream));
  };
};

// ============================================================================
DECLARE_COMPONENT( EcalChi22ID )
