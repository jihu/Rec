#ifndef CALOPIDS_CALOCHI22ID_H
#define CALOPIDS_CALOCHI22ID_H 1

// Include files
#include "CaloTrackAlg.h"
#include "CaloUtils/Calo2Track.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "Event/Track.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/Counters.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted2D.h"

// ============================================================================
/** @class CaloChi22ID CaloChi22ID.h
 *
 *
 *  @author Ivan BELYAEV
 *  @date   2006-06-18
 */
// ============================================================================
template <typename TABLEI, typename TABLEO>
class CaloChi22ID : public Gaudi::Functional::Transformer<TABLEO(const LHCb::Tracks&,const TABLEI&), Gaudi::Functional::Traits::BaseClass_t<CaloTrackAlg> > 
{
  public:
    using CaloTrackAlg::context;
    using CaloTrackAlg::use;
    using CaloTrackAlg::getProperty;
    using CaloTrackAlg::_setProperty;
    using base_type = Gaudi::Functional::Transformer<TABLEO(const LHCb::Tracks&,const TABLEI&),Gaudi::Functional::Traits::BaseClass_t<CaloTrackAlg> >;
    using KeyValue  = typename base_type::KeyValue;

    // standard constructor
    CaloChi22ID(const std::string& name, ISvcLocator* pSvc);

    /// algorithm execution
    TABLEO operator()(const LHCb::Tracks& tracks, const TABLEI& input) const override;

  protected:
    Gaudi::Property<float> m_large{ this, "CutOff", 10000, "large value"};

    mutable Gaudi::Accumulators::StatCounter<>  m_nLinks {this, "#links in table"};
    mutable Gaudi::Accumulators::StatCounter<>  m_nTracks {this, "#total tracks"};
};
#endif  // CALOCHI22ID_H
// ============================================================================
