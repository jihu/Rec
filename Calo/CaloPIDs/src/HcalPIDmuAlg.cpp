// Include files
#include "CaloID2DLL.h"

// ============================================================================
/** @class HcalPIDmuAlg  HcalPIDmuAlg.cpp
 *  The preconfigured instance of class CaloID2DLL
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-18
 */
// ============================================================================

class HcalPIDmuAlg final : public CaloID2DLL {
 public:
  HcalPIDmuAlg(const std::string& name, ISvcLocator* pSvc)
      : CaloID2DLL(name, pSvc) {
    using LHCb::CaloAlgUtils::CaloIdLocation;

    Gaudi::Functional::updateHandleLocation(
        *this, "Input", CaloIdLocation("HcalE", context()));
    Gaudi::Functional::updateHandleLocation(
        *this, "Output", CaloIdLocation("HcalPIDmu", context()));

    _setProperty("nVlong"  , Gaudi::Utils::toString(10 * Gaudi::Units::GeV));
    _setProperty("nVdown"  , Gaudi::Utils::toString(10 * Gaudi::Units::GeV));
    _setProperty("nVTtrack", Gaudi::Utils::toString(10 * Gaudi::Units::GeV));
    _setProperty("nMlong"  , Gaudi::Utils::toString(25 * Gaudi::Units::GeV));
    _setProperty("nMdown"  , Gaudi::Utils::toString(25 * Gaudi::Units::GeV));
    _setProperty("nMTtrack", Gaudi::Utils::toString(25 * Gaudi::Units::GeV));

    _setProperty("HistogramL"   , "DLL_Long");
    _setProperty("HistogramD"   , "DLL_Downstream");
    _setProperty("HistogramT"   , "DLL_Ttrack");
    _setProperty("ConditionName", "Conditions/ParticleID/Calo/HcalPIDmu");

    _setProperty("HistogramL_THS", "CaloPIDs/CALO/HCALPIDM/h3");
    _setProperty("HistogramD_THS", "CaloPIDs/CALO/HCALPIDM/h5");
    _setProperty("HistogramT_THS", "CaloPIDs/CALO/HCALPIDM/h6");

    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Long, LHCb::Track::Types::Ttrack,
                                     LHCb::Track::Types::Downstream));
  };
};

// ============================================================================

DECLARE_COMPONENT( HcalPIDmuAlg )

// ============================================================================
