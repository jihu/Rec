// Include files
#include "CaloTrack2IDAlg.h"

// ============================================================================
/** @class Track2EcalEAlg Track2EcalEAlg.cpp
 *  preconfigured instance of class  CaloTrack2IDAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-15
 */
struct Track2EcalEAlg final : public CaloTrack2IDAlg {
  Track2EcalEAlg(const std::string& name, ISvcLocator* pSvc)
      : CaloTrack2IDAlg(name, pSvc) {
    using LHCb::CaloAlgUtils::CaloIdLocation;
    _setProperty("Output", CaloIdLocation("EcalE", context()));
    _setProperty("Filter", CaloIdLocation("InEcal", context()));
    _setProperty("Tool", "EcalEnergyForTrack/EcalE");
  }
};

// ============================================================================

DECLARE_COMPONENT( Track2EcalEAlg )

// ============================================================================
