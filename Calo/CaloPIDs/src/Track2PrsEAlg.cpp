// Include files
#include "CaloTrack2IDAlg.h"

// ============================================================================
/** @class Track2PrsEAlg Track2PrsEAlg.cpp
 *  preconfigured instance of class  CaloTrack2IDAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-15
 */
struct Track2PrsEAlg final : public CaloTrack2IDAlg {
  Track2PrsEAlg(const std::string& name, ISvcLocator* pSvc)
      : CaloTrack2IDAlg(name, pSvc) {
    using LHCb::CaloAlgUtils::CaloIdLocation;
    _setProperty("Output", CaloIdLocation("PrsE", context()));
    _setProperty("Filter", CaloIdLocation("InPrs", context()));
    _setProperty("Tool", "PrsEnergyForTrack/PrsE");
  }
};

// ============================================================================

DECLARE_COMPONENT( Track2PrsEAlg )

// ============================================================================
