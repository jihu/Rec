// Include files
#include "CaloTrackMatchAlg.h"

// ============================================================================
/** @class ElectronMatchAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-16
 */
// ============================================================================
using TABLE = LHCb::RelationWeighted2D<LHCb::CaloHypo, LHCb::Track, float>;
using CALOTYPES = LHCb::CaloHypos;

struct ElectronMatchAlg final : CaloTrackMatchAlg<TABLE,CALOTYPES> {
  static_assert(std::is_base_of<LHCb::Calo2Track::IHypoTrTable2D, TABLE>::value,
                "Table must inherit from IHypoTrTable2D");

  ElectronMatchAlg(const std::string& name, ISvcLocator* pSvc)
      : CaloTrackMatchAlg<TABLE,CALOTYPES>(name, pSvc) {
    Gaudi::Functional::updateHandleLocation(*this, "Calos",  LHCb::CaloAlgUtils::CaloHypoLocation("Electrons",context()));
    Gaudi::Functional::updateHandleLocation(*this, "Output", LHCb::CaloAlgUtils::CaloIdLocation("ElectronMatch",context()));
    Gaudi::Functional::updateHandleLocation(*this, "Filter", LHCb::CaloAlgUtils::CaloIdLocation("InEcal",context()));

    _setProperty("Tool", "CaloElectronMatch/ElectronMatch");
    _setProperty("Threshold", "10000");
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Long, LHCb::Track::Types::Downstream,
                                     LHCb::Track::Types::Ttrack));
    _setProperty("TableSize", "1000");
  }

};

// ============================================================================

DECLARE_COMPONENT( ElectronMatchAlg )

