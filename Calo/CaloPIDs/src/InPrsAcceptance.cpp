// Include files
#include "InCaloAcceptance.h"

// ============================================================================
/** @class InPrsAcceptance
 *  The precofigured instance of InCaloAcceptance Tool
 *  @author Victor EGORYCHEV Victor.Egorychev@cern.ch
 *  @author Vanya  BELYAEV    ibelyaev@phsycis.syr.edu
 *  @date   2006-05-28
 */
// ============================================================================

struct InPrsAcceptance final : InCaloAcceptance {
  /// standard constructor
  InPrsAcceptance(const std::string& type, const std::string& name,
                  const IInterface* parent)
      : InCaloAcceptance(type, name, parent) {
    _setProperty("Calorimeter", DeCalorimeterLocation::Prs);
    _setProperty("UseFiducial", "false");
    _setProperty("Tolerance", "10");  /// 10 * Gaudi::Units::mm
  };

  /// C++11 non-copyable idiom
  InPrsAcceptance() = delete;
  InPrsAcceptance(const InPrsAcceptance&) = delete;
  InPrsAcceptance& operator=(const InPrsAcceptance&) = delete;
};

// ============================================================================

DECLARE_COMPONENT( InPrsAcceptance )

// ============================================================================
