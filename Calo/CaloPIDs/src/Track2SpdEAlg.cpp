// Include files
#include "CaloTrack2IDAlg.h"

// ============================================================================
/** @class Track2SpdEAlg Track2SpdEAlg.cpp
 *  preconfigured instance of class  CaloTrack2IDAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-15
 */
struct Track2SpdEAlg final : public CaloTrack2IDAlg {
  Track2SpdEAlg(const std::string& name, ISvcLocator* pSvc)
      : CaloTrack2IDAlg(name, pSvc) {
    using LHCb::CaloAlgUtils::CaloIdLocation;
    _setProperty("Output", CaloIdLocation("SpdE", context()));
    _setProperty("Filter", CaloIdLocation("InSpd", context()));
    _setProperty("Tool", "SpdEnergyForTrack/SpdE");
  }
};

// ============================================================================

DECLARE_COMPONENT( Track2SpdEAlg )

// ============================================================================
