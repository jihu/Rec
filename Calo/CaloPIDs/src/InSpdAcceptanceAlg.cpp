// Include files
#include "InCaloAcceptanceAlg.h"

// ============================================================================
/** @class InSpdAcceptanceAlg InSpdAcceptanceAlg.cpp
 *  the preconfigured instance of InCaloAcceptanceAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-17
 */
// ============================================================================

struct InSpdAcceptanceAlg final : InCaloAcceptanceAlg {
  /// Standard constructor
  InSpdAcceptanceAlg(const std::string& name, ISvcLocator* pSvc)
      : InCaloAcceptanceAlg(name, pSvc) {
    using LHCb::CaloAlgUtils::CaloIdLocation;

    Gaudi::Functional::updateHandleLocation(*this, "Output",
                                            CaloIdLocation("InSpd", context()));

    _setProperty("Tool", "InSpdAcceptance/InSpd");
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Long, LHCb::Track::Types::Downstream,
                                     LHCb::Track::Types::Ttrack));
  }
};

// ============================================================================

DECLARE_COMPONENT( InSpdAcceptanceAlg )

// ============================================================================
