#ifndef CALOPIDS_INCALOACCEPTANCEALG_H 
#define CALOPIDS_INCALOACCEPTANCEALG_H 1

// Include files
#include "CaloTrackAlg.h"
#include "CaloUtils/Calo2Track.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "Event/Track.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/Counters.h"
#include "TrackInterfaces/IInAcceptance.h"
#include "Relations/Relation1D.h"
#include "ToVector.h"

// ============================================================================
/** @class InCaloAcceptanceAlg InCaloAcceptanceAlg.h
 *
 *  the trivial algorithm to fill "InCaloAcceptance" table 
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-17
 */
// ============================================================================

using Table =  LHCb::Relation1D<LHCb::Track,bool>;

class InCaloAcceptanceAlg
    : public Gaudi::Functional::Transformer<
          Table(const LHCb::Tracks&),
          Gaudi::Functional::Traits::BaseClass_t<CaloTrackAlg> >
{
  // check the proper convertability
  static_assert(std::is_base_of<LHCb::Calo2Track::ITrAccTable, Table>::value,
                "Table must inherit from ITrAccTable");

  public:
    /// Standard constructor
    InCaloAcceptanceAlg(const std::string& name, ISvcLocator* pSvc);
    /// algorithm execution
    Table operator()(const LHCb::Tracks&) const override;

    ToolHandle<IInAcceptance> m_tool {this, "Tool", "<NOT DEFINED>"};

    // counter
    mutable Gaudi::Accumulators::StatCounter<> m_nTracks{this, "#total tracks"};
    mutable Gaudi::Accumulators::StatCounter<> m_nAccept{this, "#tracks in acceptance"};
    mutable Gaudi::Accumulators::StatCounter<> m_nLinks {this, "#links in table"};
};

// ============================================================================
#endif  // CALOPIDS_INCALOACCEPTANCEALG_H
// ============================================================================
