// Include files
#include "CaloTrackMatchAlg.h"

// =============================================================================
/** @class BremMatchAlg
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-06-16
 */
// =============================================================================
using TABLE = LHCb::RelationWeighted2D<LHCb::CaloHypo, LHCb::Track, float>;
using CALOTYPES = LHCb::CaloHypos;

struct BremMatchAlg final : CaloTrackMatchAlg<TABLE,CALOTYPES> {
  static_assert(std::is_base_of<LHCb::Calo2Track::IHypoTrTable2D, TABLE>::value,
                "TABLE must inherit from IHypoTrTable2D");
  
  BremMatchAlg(const std::string& name, ISvcLocator* pSvc)
      : CaloTrackMatchAlg<TABLE,CALOTYPES>(name, pSvc) {
    Gaudi::Functional::updateHandleLocation(*this, "Calos",  LHCb::CaloAlgUtils::CaloHypoLocation("Photons",context()));
    Gaudi::Functional::updateHandleLocation(*this, "Output", LHCb::CaloAlgUtils::CaloIdLocation("BremMatch",context()));
    Gaudi::Functional::updateHandleLocation(*this, "Filter", LHCb::CaloAlgUtils::CaloIdLocation("InBrem",context()));

    _setProperty("Tool", "CaloBremMatch/BremMatch");
    _setProperty("Threshold", "10000");
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Velo, LHCb::Track::Types::Long,
                                     LHCb::Track::Types::Upstream));
    _setProperty("TableSize", "1000");
  }

};

// =============================================================================

DECLARE_COMPONENT( BremMatchAlg )
